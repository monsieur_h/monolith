#!/usr/bin/env bash
CWD=$(dirname $(readlink -f $0 || realpath $0))
source ${CWD}/var.sh

function remove_dir_from_binary(){
    if [ -z "$1" ]; then
        exit -1
    fi
    echo "Removing ${1} from executable..."
    zip --delete ${BUILD_FILE} "${1}/*"
    rm -Rf ${SHARED_DIR}${1}
    cp -Rn ${ASSET_DIR}${1} ${SHARED_DIR}
}

cd ${CWD}/..

./gradlew :desktop:dist

BUILD_FILE="${PROJECT_ROOT_DIR}desktop/build/libs/desktop-0.1.jar"

mkdir -p ${SHARED_DIR}

remove_dir_from_binary textures
remove_dir_from_binary config

rm ${SHARED_DIR}*.jar

cp ${BUILD_FILE} ${SHARED_DIR}

