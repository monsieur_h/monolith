#!/usr/bin/env bash
CWD=$(dirname $(readlink -f $0 || realpath $0))
source ${CWD}/var.sh

if [[ -z "$1" ]]; then
    HISTORY_SIZE=5
else
    HISTORY_SIZE=$1
fi

cd ${PROJECT_ROOT_DIR}
echo "Writing ${HISTORY_SIZE} lines of changelog to ${SHARED_DIR}changes.txt"
git log --oneline | head -n ${HISTORY_SIZE} > ${SHARED_DIR}changes.txt

cd -