#ifdef GL_ES
    #define LOWP lowp
    precision mediump float;
#else
    #define LOWP
#endif

#define PI 3.14159

varying LOWP vec4 v_color;
varying vec2 v_texCoords;

uniform sampler2D u_texture;
uniform float f_screenHeight;
uniform float f_strength;

uniform float dir_x;
uniform float dir_y;

void main() {
    float radius=0.10;
    vec2 dir = vec2(1, 0);

    vec4 sum = vec4(0.0);
    vec2 tc = v_texCoords;
    float blur = exp( cos(v_texCoords.y * PI * 2.0) ) * (f_strength * 0.75) / f_screenHeight;


    float hstep = dir_x;
    float vstep = dir_y;

    sum += texture2D(u_texture, vec2(tc.x - 4.0*blur*hstep, tc.y - 4.0*blur*vstep)) * 0.05;
    sum += texture2D(u_texture, vec2(tc.x - 3.0*blur*hstep, tc.y - 3.0*blur*vstep)) * 0.09;
    sum += texture2D(u_texture, vec2(tc.x - 2.0*blur*hstep, tc.y - 2.0*blur*vstep)) * 0.12;
    sum += texture2D(u_texture, vec2(tc.x - 1.0*blur*hstep, tc.y - 1.0*blur*vstep)) * 0.15;

    sum += texture2D(u_texture, vec2(tc.x, tc.y)) * 0.16;

    sum += texture2D(u_texture, vec2(tc.x + 1.0*blur*hstep, tc.y + 1.0*blur*vstep)) * 0.15;
    sum += texture2D(u_texture, vec2(tc.x + 2.0*blur*hstep, tc.y + 2.0*blur*vstep)) * 0.12;
    sum += texture2D(u_texture, vec2(tc.x + 3.0*blur*hstep, tc.y + 3.0*blur*vstep)) * 0.09;
    sum += texture2D(u_texture, vec2(tc.x + 4.0*blur*hstep, tc.y + 4.0*blur*vstep)) * 0.05;

    gl_FragColor = v_color * vec4(sum.rgb, 1.0);
//    gl_FragColor = vec4(blur*10.0, 0.0, 0.0, 1.0);
}
