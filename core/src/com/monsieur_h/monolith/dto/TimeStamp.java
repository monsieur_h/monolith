package com.monsieur_h.monolith.dto;

import com.badlogic.gdx.utils.I18NBundle;

public class TimeStamp {
    private final I18NBundle bundle;
    public int day;
    public float timeOfTheDay;

    public TimeStamp(int day, float timeOfTheDay) {
        this.day = day;
        this.timeOfTheDay = timeOfTheDay;
        bundle = L10nProvider.getBundle(L10nProvider.BundleType.LOG);
    }

    public static TimeStamp stamp() {
        return new TimeStamp(TimeManager.getWorldAgeInDays(), (float) TimeManager.getTimeOfTheDay());
    }

    public String toString() {
        return bundle.format("TIMESTAMP", day, timeOfTheDay);
    }

}
