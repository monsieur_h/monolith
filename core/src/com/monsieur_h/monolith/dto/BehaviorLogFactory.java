package com.monsieur_h.monolith.dto;


import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.utils.I18NBundle;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.ai.VillagerPlanChangeListener;
import com.monsieur_h.monolith.engine.ai.VillagerStateChangeListener;

public class BehaviorLogFactory implements VillagerStateChangeListener, VillagerPlanChangeListener {

    public static final int LOG_HISTORY_SIZE = 20;

    private I18NBundle bundle;
    private L10nProvider L10nProvider = new L10nProvider();

    public BehaviorLogFactory() {
        bundle = com.monsieur_h.monolith.dto.L10nProvider.getBundle(com.monsieur_h.monolith.dto.L10nProvider.BundleType.LOG);
    }

    public BehaviorLog create(VillagerAI villagerAI, BehaviorLog.LogLOD levelOfDetail) {
        BehaviorLog log = new BehaviorLog(villagerAI);
        log.timeStamp = TimeStamp.stamp();
        log.icon = villagerAI.getCurrentVillagerStateIcon();
        log.stateName = villagerAI.getCurrentState().getClass().getSimpleName();
        log.cause = villagerAI.getLastChoice();
        log.levelOfDetail = levelOfDetail;
        return log;
    }


    @Override
    public void onPlanChanged(VillagerAI owner) {
        create(owner, BehaviorLog.LogLOD.PLAN);
    }

    @Override
    public void onStateChanged(VillagerAI villagerAI, State<VillagerAI> previousState, State<VillagerAI> currentState) {
        create(villagerAI, BehaviorLog.LogLOD.STATE);
    }

    @Override
    public void onGrab(VillagerAI villagerAI) {
//        log(create(villagerAI, BehaviorLog.LogLOD.STATE));
    }

    @Override
    public void onReleased(VillagerAI villagerAI) {
//        log(create(villagerAI, BehaviorLog.LogLOD.STATE));
    }

    public void register(VillagerAI villagerAI) {
        villagerAI.registerChangeListener(this);
        villagerAI.registerPlanListener(this);
    }
}
