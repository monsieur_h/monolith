package com.monsieur_h.monolith.dto;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.TimeUtils;

public class FPSLabel extends Label {
    long startTime;

    public FPSLabel(CharSequence text, Skin skin) {
        super(text, skin);
        startTime = TimeUtils.nanoTime();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        update(delta);
    }

    public void update(float delta) {
        if (TimeUtils.nanoTime() - startTime > 1000000000) /* 1,000,000,000ns == one second */ {
            startTime = TimeUtils.nanoTime();
            final int fps = Gdx.graphics.getFramesPerSecond();
            final int hour = (int) (TimeManager.getTimeOfTheDay() * 24);
            final int worldAgeInDays = TimeManager.getWorldAgeInDays();
            setText(String.format("FPS : %d  -  Day : %d, %d hour", fps, worldAgeInDays, hour));
        }
    }
}
