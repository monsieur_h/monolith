package com.monsieur_h.monolith.dto;

public class RingBuffer<T> {
    private final int capacity;
    private int currentIndex;
    private T[] items;

    @SuppressWarnings("unchecked") //Warning suppressed because (T[]) new Object[capacity]; is an array of unchecked generics
    public RingBuffer(int capacity) {
        this.capacity = capacity;
        currentIndex = -1;
        items = (T[]) new Object[capacity];
    }

    public void add(T item) {
        incrementIndex();
        items[currentIndex] = item;
    }

    public T peek() {
        return items[currentIndex];
    }

    public T getPrevious(int index) {
        int itemIndex = (currentIndex - index) % capacity;
        return items[itemIndex];
    }

    public boolean isFull() {
        for (T item : items) {
            if (item == null) {
                return false;
            }
        }
        return true;
    }

    public void clear() {
        for (int i = 0; i < capacity - 1; i++) {
            items[i] = null;
        }
    }

    public boolean isEmpty() {
        return currentIndex == -1;
    }

    private void incrementIndex() {
        currentIndex = (currentIndex + 1) % capacity;
    }

    public T getCurrent() {
        return getPrevious(0);
    }
}
