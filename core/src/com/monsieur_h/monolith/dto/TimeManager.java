package com.monsieur_h.monolith.dto;

public class TimeManager {
    public static final int SECONDS_PER_DAY = 180;//3 minutes
    private static float timeElapsedSinceWorldStart = 0;

    public static void update(float delta) {
        timeElapsedSinceWorldStart += delta;
    }

    /**
     * @return time of the day on a 0-1 scale
     */
    public static double getTimeOfTheDay() {
        return (timeElapsedSinceWorldStart % SECONDS_PER_DAY) / SECONDS_PER_DAY;
    }

    public static int getWorldAgeInDays() {
        return (int) Math.floor(timeElapsedSinceWorldStart / SECONDS_PER_DAY);
    }


}
