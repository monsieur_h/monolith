package com.monsieur_h.monolith.dto;

import com.badlogic.gdx.utils.I18NBundle;
import com.monsieur_h.monolith.engine.ai.BiologicalStatus;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.ai.states.VillagerStateIcons;

public class BehaviorLog {
    public VillagerAI owner;
    public BiologicalStatus.NeedType cause;
    public VillagerStateIcons icon;
    public String stateName;
    public TimeStamp timeStamp;
    public LogLOD levelOfDetail;


    public BehaviorLog(VillagerAI owner) {
        this.owner = owner;
    }

    public String toString() {
        String stringRepresentation = String.format("( %s ) %s : %s", owner.getName(), levelOfDetail.toString(), cause);
        if (levelOfDetail == LogLOD.STATE) {
            stringRepresentation += String.format(" : %s ", icon.toString());
        }
        return stringRepresentation;
    }

    public String toLocalFormat() {
        //Example :
        // D01:0.5 Adam : "I'm bothered by safety, but I have a plan !"
        I18NBundle bundle = L10nProvider.getBundle(L10nProvider.BundleType.LOG);

        String fullLog;
        if (levelOfDetail == BehaviorLog.LogLOD.PLAN) {
            fullLog = bundle.format("PLAN", cause);
        } else {
            String action = bundle.format(icon.toString());
            fullLog = bundle.format("STATE", action);
        }
        return fullLog;
    }

    public enum LogLOD {
        PLAN,
        STATE
    }

}
