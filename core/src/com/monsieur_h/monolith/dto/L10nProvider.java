package com.monsieur_h.monolith.dto;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.HashMap;
import java.util.Locale;

public class L10nProvider {
    private static final String BUNDLE_PATH = "l10n/";
    private static HashMap<BundleType, I18NBundle> storage = new HashMap<BundleType, I18NBundle>();
    private static Locale currentLocale;

    public static Locale getCurrentLocale() {
        if (currentLocale == null) {
            currentLocale = Locale.getDefault();
        }
        return currentLocale;
    }

    public void setCurrentLocale(Locale locale) {
        currentLocale = locale;
        storage.clear();
    }

    public static I18NBundle getBundle(BundleType type) {
        final I18NBundle i18NBundle = storage.get(type);
        if (i18NBundle == null) {
            loadBundle(type);
        }
        return storage.get(type);
    }

    private static void loadBundle(BundleType type) {
        FileHandle fileHandle = Gdx.files.internal(BUNDLE_PATH + type.toString().toLowerCase());
        storage.put(type, I18NBundle.createBundle(fileHandle, getCurrentLocale()));
    }

    public enum BundleType {
        LOG,
        DESCRIPTION
    }
}
