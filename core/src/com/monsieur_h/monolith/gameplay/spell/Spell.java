package com.monsieur_h.monolith.gameplay.spell;

import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.engine.generation.WorldContainer;
import com.monsieur_h.monolith.gameplay.MainScene;

/**
 * Created for project monolith by ahub on 10/15/16.
 */
public interface Spell {
    void cast(Vector2 position, MainScene scene);
}
