package com.monsieur_h.monolith.gameplay.spell;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.monsieur_h.monolith.engine.entities.Tile;
import com.monsieur_h.monolith.engine.entities.TileFactory;
import com.monsieur_h.monolith.engine.generation.TileDef;
import com.monsieur_h.monolith.engine.generation.WorldContainer;
import com.monsieur_h.monolith.engine.input.GameWorldInputMultiplexer;
import com.monsieur_h.monolith.gameplay.MainScene;

import java.util.List;

/**
 * Created for project monolith by ahub on 10/18/16.
 */
public class TerraformSpell extends ClickableSpell {
    private final TileDef.TileType tileType;

    public TerraformSpell(GameWorldInputMultiplexer inputMultiplexer, TileDef.TileType tileType) {
        super(inputMultiplexer);
        this.tileType = tileType;
    }

    @Override
    public void cast(Vector2 position, MainScene scene) {
        WorldContainer worldContainer = scene.getWorldContainer();
        Group layer = scene.getLayer(MainScene.Layer.GROUND);
        position = layer.screenToLocalCoordinates(position);

        List<Tile> tilesAt = worldContainer.getTilesAt(position);
        if (tilesAt.isEmpty()) {
            return;//todo reset spell points
        }
        Tile targetTile = tilesAt.get(0);


        //Delete tile
        worldContainer.deleteTile(targetTile);

        //Create new tile
        TileFactory tileFactory = new TileFactory();
        TileDef tileDef = new TileDef(tileType);

        Tile newTile = tileFactory.createTile(tileDef, worldContainer.getWorld(), targetTile.getDef().x, targetTile.getDef().y);
        newTile.getDef().x = targetTile.getDef().x;
        newTile.getDef().y = targetTile.getDef().y;
        worldContainer.addTile(newTile);
        Image image = newTile.getImage();
        layer.addActor(image);
        animateTileCreation(image, scene);
    }

    private void animateTileCreation(Image image, MainScene scene) {
        float x = image.getX();
        float y = image.getY();
        float duration = .5f;
        Interpolation interpolation = Interpolation.pow5Out;
        image.addAction(Actions.parallel(
                Actions.moveTo(x, y - image.getHeight() / 2, 0),
                Actions.fadeOut(0),

                Actions.fadeIn(duration, interpolation),
                Actions.moveTo(x, y, duration, interpolation)
        ));
        scene.getCamera().shake(duration, 3);
    }
}
