package com.monsieur_h.monolith.gameplay.spell;

import com.monsieur_h.monolith.engine.input.GameWorldInputMultiplexer;
import com.monsieur_h.monolith.engine.input.SpellCasterInput;

/**
 * Created for project monolith by ahub on 10/15/16.
 */
abstract class ClickableSpell implements Spell {
    ClickableSpell(GameWorldInputMultiplexer multiplexer) {
        new SpellCasterInput(multiplexer).spellList.add(this);
    }
}
