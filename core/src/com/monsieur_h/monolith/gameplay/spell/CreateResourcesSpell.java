package com.monsieur_h.monolith.gameplay.spell;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.monsieur_h.monolith.engine.entities.ResourceEntity;
import com.monsieur_h.monolith.engine.entities.ResourceFactory;
import com.monsieur_h.monolith.engine.entities.Tile;
import com.monsieur_h.monolith.engine.generation.TileDef;
import com.monsieur_h.monolith.engine.generation.WorldContainer;
import com.monsieur_h.monolith.engine.input.GameWorldInputMultiplexer;
import com.monsieur_h.monolith.gameplay.MainScene;

import java.util.List;

/**
 * Created for project monolith by ahub on 10/18/16.
 */
public class CreateResourcesSpell extends ClickableSpell {
    public CreateResourcesSpell(GameWorldInputMultiplexer multiplexer) {
        super(multiplexer);
    }

    @Override
    public void cast(Vector2 position, MainScene scene) {
        WorldContainer worldContainer = scene.getWorldContainer();
        Group groundLayer = scene.getLayer(MainScene.Layer.GROUND);
        Group gameLayer = scene.getLayer(MainScene.Layer.GAME);
        position = groundLayer.screenToLocalCoordinates(position);

        List<Tile> tilesAt = worldContainer.getTilesAt(position);
        if (tilesAt.isEmpty()) {
            return;//todo reset spell points
        }
        Tile targetTile = tilesAt.get(0);


        //Create new tile
        ResourceFactory resourceFactory = new ResourceFactory();
        TileDef def = targetTile.getDef();
        float resourceCount = 4;//todo move as parameter
        for (int i = 0; i <= resourceCount; i++) {
            addEntity(scene, gameLayer, resourceFactory, def, i);

        }
    }

    private void addEntity(MainScene scene, Group gameLayer, ResourceFactory resourceFactory, TileDef def, int entityCount) {
        ResourceEntity entity = resourceFactory.createEntity(def, scene.getWorld(), def.x, def.y);
        scene.getWorldContainer().addResource(entity);
        gameLayer.addActor(entity.getImage());

        animateTileCreation(entity.getImage(), scene, entityCount);
    }

    private void animateTileCreation(Image image, MainScene scene, int delayMul) {
        float x = image.getX();
        float y = image.getY();
        float duration = .5f;
        Interpolation interpolation = Interpolation.pow5Out;
        float delay = .15f;
        image.addAction(
                Actions.sequence(
                        Actions.parallel(
                                Actions.moveTo(x, y + image.getHeight() / 2, 0),
                                Actions.fadeOut(0)
                        ),
                        Actions.delay(delayMul * delay),
                        Actions.parallel(
                                Actions.fadeIn(duration, interpolation),
                                Actions.moveTo(x, y, duration, interpolation)
                        )
                )
        );
        scene.getCamera().shake(duration / 2, 1);
    }
}
