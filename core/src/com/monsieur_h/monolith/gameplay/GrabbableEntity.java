package com.monsieur_h.monolith.gameplay;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;

public interface GrabbableEntity {
    void onGrab();

    Actor getActor();

    void onGrabReleased();

    Box2dSteeringEntity getBox2dComponent();

    void kill();
}
