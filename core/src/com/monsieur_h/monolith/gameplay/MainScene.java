package com.monsieur_h.monolith.gameplay;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.SnapshotArray;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.engine.GameCamera;
import com.monsieur_h.monolith.engine.ai.RelationshipMapper;
import com.monsieur_h.monolith.engine.ai.RelationshipMeter;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.entities.*;
import com.monsieur_h.monolith.engine.generation.TileDef;
import com.monsieur_h.monolith.engine.generation.WorldContainer;
import com.monsieur_h.monolith.engine.generation.WorldFactory;
import com.monsieur_h.monolith.engine.graphics.animations.PivotableAnimatedSpriteFactory;
import com.monsieur_h.monolith.engine.graphics.gui.MainSceneGUI;
import com.monsieur_h.monolith.engine.graphics.lighting.GroundLightingManager;
import com.monsieur_h.monolith.engine.graphics.lighting.SkyLightingManager;
import com.monsieur_h.monolith.engine.graphics.shaders.ShaderProvider;
import com.monsieur_h.monolith.engine.graphics.sky.CloudManager;
import com.monsieur_h.monolith.engine.graphics.sky.Sky;
import com.monsieur_h.monolith.engine.input.GameWorldInputMultiplexer;
import com.monsieur_h.monolith.engine.input.InputAndGestureDetector;

import java.util.Comparator;
import java.util.HashMap;

public class MainScene extends Stage {
    public final HashMap<Layer, Group> layers = new HashMap<Layer, Group>();
    private final World world;
    private final Box2DDebugRenderer renderer = new Box2DDebugRenderer();
    private final ShaderProvider shaderProvider = new ShaderProvider();
    private final GameCamera camera;
    public boolean debug = false;
    public boolean isPostProcessActive = true;
    public boolean isShaderActive = true;
    private MainSceneGUI mainSceneGUI;
    private VillagerEntityFactory villagerFactory;
    private WorldFactory worldFactory;
    private FrameBuffer frameBuffer;
    private ShaderProgram shaderProgram;
    private TextureRegion shaderCanvas;
    private CloudManager cloudManager;
    private Sky sky;
    private WorldContainer worldContainer;
    private SkyLightingManager skyLightingManager;
    private GroundLightingManager groundLightingManager;
    private GameWorldInputMultiplexer gameWorldInputProcessor;

    public MainScene(World world, Viewport viewport) {
        super(viewport);
        this.world = world;
        this.camera = (GameCamera) super.getCamera();

        initLayers();

        initFactories();
        initWorld();
        initLighting();
        initInputs();

        initShader();

        initGUI();

        initGameObjects();

    }

    private void initGUI() {
        mainSceneGUI = new MainSceneGUI(this);
        mainSceneGUI.setVillagerFactory(villagerFactory);
    }

    public GameCamera getCamera() {
        return this.camera;
    }

    private void initLighting() {
        GameCamera cam = getCamera();
        final float lightDistance = cam.maxZoom * worldContainer.getRadius();
        skyLightingManager = new SkyLightingManager(cloudManager.getWorld(), lightDistance, getCamera());
        groundLightingManager = new GroundLightingManager(getWorld(), getCamera());
    }

    private void initFactories() {
        worldFactory = new WorldFactory(getWorld());
        villagerFactory = new VillagerEntityFactory(worldFactory.getWorldContainer());
    }

    private void initWorld() {
        worldContainer = worldFactory.getWorldContainer();
        for (ResourceEntity resource : worldContainer.getResources()) {
            final Image image = resource.getImage();
            addActor(image, Layer.GAME);
            image.toBack();
        }

        for (Tile tile : worldContainer.getTiles()) {
            final Image image = tile.getImage();
            addActor(image, Layer.GROUND);
            image.toBack();
        }

        cloudManager = new CloudManager(getCamera(), worldContainer.getRadius());
        addActor(cloudManager, Layer.FOREGROUND);

        sky = new Sky();
    }

    private void initInputs() {
        gameWorldInputProcessor = new GameWorldInputMultiplexer(this);
        final InputAndGestureDetector gestureDetector = new InputAndGestureDetector(gameWorldInputProcessor);
        Gdx.input.setInputProcessor(gestureDetector);
    }

    private void initShader() {
        frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
        shaderCanvas = new TextureRegion();
        shaderProgram = shaderProvider.getShader(ShaderProvider.Shader.BLUR);
    }

    @Override
    public void draw() {
        orderZIndex();
        final GameCamera camera = getCamera();
        final Matrix4 combined = camera.combined;

        if (isPostProcessActive) {
            if (isShaderActive) {
                renderSceneToFramebuffer();
                renderFramebufferToScreen(camera);
            } else {
                renderSceneWithLighting();
            }
        } else {
            renderScene();
        }

        renderGui();

        if (debug) {
            renderBox2dDebugLayerToScreen(combined);
        }
    }

    private void renderSceneWithLighting() {
        cloudManager.remove();
        renderScene();
        renderSkyLightning();
        getBatch().begin();
        cloudManager.draw(getBatch(), 1f);
        getBatch().end();
        addActor(cloudManager, Layer.FOREGROUND);
    }

    private void orderZIndex() {//todo: only if needed avg ~15%CPU cost
        final SnapshotArray<Actor> children = layers.get(Layer.GAME).getChildren();
        children.sort(new Comparator<Actor>() {
            @Override
            public int compare(Actor o1, Actor o2) {
                return (int) (o2.getY() - o1.getY());
            }
        });
        final SnapshotArray<Actor> groundParts = layers.get(Layer.GROUND).getChildren();
        groundParts.sort(new Comparator<Actor>() {
            @Override
            public int compare(Actor o1, Actor o2) {
                return (int) (o2.getY() - o1.getY());
            }
        });
    }

    private void renderSkyLightning() {
        skyLightingManager.draw();
    }

    private void renderGui() {
        mainSceneGUI.draw();
    }


    private void renderFramebufferToScreen(GameCamera camera) {

        shaderCanvas = new TextureRegion(frameBuffer.getColorBufferTexture());
        shaderCanvas.flip(false, true);
        Vector2 blurDirection = new Vector2(1, 0);

        frameBuffer.begin();
        renderBlur(blurDirection, 1 - camera.getCurrentZoomRatio(), camera); //Render first pass to FBO
        frameBuffer.end();

        blurDirection.set(0, 1);
        renderBlur(blurDirection, 1 - camera.getCurrentZoomRatio(), camera); //Render second blur pass to the screen

        getBatch().setShader(null);
        renderSkyLightning();
        renderGroundLighting();
    }

    private void renderBlur(Vector2 direction, float strength, GameCamera camera) {
        final Batch batch = getBatch();
        batch.setColor(Color.WHITE);
        batch.setShader(shaderProgram);
        batch.begin();
        shaderProgram.setUniformf("dir_x", direction.x);
        shaderProgram.setUniformf("dir_y", direction.y);
        shaderProgram.setUniformf("f_screenHeight", Gdx.graphics.getHeight());
        shaderProgram.setUniformf("f_strength", strength);
        final float width = Gdx.graphics.getWidth() * camera.zoom;
        final float height = Gdx.graphics.getHeight() * camera.zoom;
        batch.draw(shaderCanvas, camera.position.x - width / 2, camera.position.y - height / 2, width, height);
        batch.end();
        batch.setShader(null);
    }

    private void renderSceneToFramebuffer() {
        frameBuffer.begin();
        renderScene();
        renderGroundLighting();
        frameBuffer.end();
    }

    private void renderGroundLighting() {
        groundLightingManager.draw();
    }

    private void renderScene() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        sky.draw();
        super.draw();
    }

    private void renderBox2dDebugLayerToScreen(Matrix4 combined) {
        combined.scl(MonolithGame.METERS_TO_PIXEL_RATIO);
        renderer.render(world, combined);
        renderer.render(cloudManager.getWorld(), combined);
        combined.scl(1 / MonolithGame.METERS_TO_PIXEL_RATIO);
    }

    public World getWorld() {
        return world;
    }

    private void initLayers() {
        for (Layer layer : Layer.values()) {
            layers.put(layer, new Group());
            if (layer != Layer.GUI && layer != Layer.SKY) {//Don't add those. We will draw them manually
                addActor(layers.get(layer));
            }
        }
    }

    public void addActor(Actor actor, Layer layer) {
        final Group group = layers.get(layer);
        group.addActor(actor);
    }

    public GrabbableEntity getEntityFromScreenSpace(float x, float y) {
        final Vector2 stageCoordinates = screenToStageCoordinates(new Vector2(x, y));
        final Actor actor = layers.get(Layer.GAME).hit(stageCoordinates.x, stageCoordinates.y, true);
        if (actor instanceof GrabbableEntity) {
            return (GrabbableEntity) actor;
        }
        return null;
    }

    public GrabbableEntity getEntityFromScreenSpace(float x, float y, float tolerance) {
        if (tolerance == 0) {
            return getEntityFromScreenSpace(x, y);
        }
        for (Actor actor : layers.get(Layer.GAME).getChildren()) {
            if (actor.getTouchable() != Touchable.enabled
                    || !(actor instanceof GrabbableEntity)) {
                continue;
            }

            Vector2 actorCenter = new Vector2(
                    actor.getX() + actor.getWidth() / 2f,
                    actor.getY() + actor.getHeight() / 2f
            );

            if (actorCenter.dst(x, y) < tolerance) {
                return (GrabbableEntity) actor;
            }
        }
        return null;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        world.step(delta, 8, 3);
        sky.act(delta);
        mainSceneGUI.update(delta);
        worldContainer.update();
        skyLightingManager.update();
        groundLightingManager.update();
    }

    private void initGameObjects() {
        final VillagerEntity adam = villagerFactory.createVillagerEntity(world, PivotableAnimatedSpriteFactory.SpriteSheet.BROWN_GUY, 500, 550);
        final VillagerEntity eve = villagerFactory.createVillagerEntity(world, PivotableAnimatedSpriteFactory.SpriteSheet.BROWN_GIRL, -100, 500);

        addActor(adam, Layer.GAME);
        addActor(eve, Layer.GAME);

        final VillagerAI eveVillagerAI = eve.getVillagerAI();
        final VillagerAI adamVillagerAI = adam.getVillagerAI();

        adam.setName("Adam");
        eve.setName("Eve");
        adam.getVillagerAI().getRelationshipMapper().marry(eve.getVillagerAI());
        eve.getVillagerAI().getRelationshipMapper().marry(adam.getVillagerAI());

        adamVillagerAI.setSex(VillagerAI.Sex.MALE);
        eveVillagerAI.setSex(VillagerAI.Sex.FEMALE);

        final Tile house = worldContainer.getTileByType(TileDef.TileType.HOUSE).get(0);
        adamVillagerAI.setHouse((HouseTile) house);
        eveVillagerAI.setHouse((HouseTile) house);

        //TODO: Remove this, it makes adam hate eve for testing purposes
        final Timer.Task task = new Timer.Task() {
            @Override
            public void run() {
                final RelationshipMapper adamRelations = adam.getVillagerAI().getRelationshipMapper();
                adamRelations.deteriorateRelationship(eveVillagerAI);
                adamRelations.getRelationShip(eveVillagerAI).setRelationValue(RelationshipMeter.MIN_VALUE);
            }
        };
        Timer.schedule(task, .25f);
    }

    public void setPostProcessActive(boolean postProcessActive) {
        isPostProcessActive = postProcessActive;
        skyLightingManager.setEnabled(postProcessActive);
    }

    public MainSceneGUI getGUI() {
        return mainSceneGUI;
    }

    public SkyLightingManager getSkyLightingManager() {
        return skyLightingManager;
    }

    public GroundLightingManager getGroundLightingManager() {
        return groundLightingManager;
    }

    public void resize(int width, int height) {
        mainSceneGUI.resize(width, height);
    }

    public WorldContainer getWorldContainer() {
        return worldContainer;
    }

    public GameWorldInputMultiplexer getInputMultiplexer() {
        return gameWorldInputProcessor;
    }

    public Group getLayer(Layer layerType) {
        return layers.get(layerType);
    }

    public enum Layer {
        SKY,
        GROUND,
        GAME,
        FOREGROUND,
        GUI
    }
}
