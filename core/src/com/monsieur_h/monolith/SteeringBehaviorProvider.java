package com.monsieur_h.monolith;


import com.badlogic.gdx.ai.steer.behaviors.*;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.engine.entities.PivotableSteeringGameWorldEntity;
import com.monsieur_h.monolith.engine.entities.SteeringGameWorldEntity;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;

public class SteeringBehaviorProvider {

    public static Arrive<Vector2> createGoNearBehavior(SteeringGameWorldEntity owner, SteeringGameWorldEntity target) {
        Arrive<Vector2> goNear = new Arrive<Vector2>(owner.getBox2dComponent(), target.getBox2dComponent());
        goNear.setTimeToTarget(0.01f);
        goNear.setArrivalTolerance(8f);
        goNear.setDecelerationRadius(8f);
        return goNear;
    }

    public static Arrive<Vector2> createGoNearBehavior(SteeringGameWorldEntity owner, Box2dSteeringEntity target) {
        Arrive<Vector2> goNear = new Arrive<Vector2>(owner.getBox2dComponent(), target);
        goNear.setTimeToTarget(0.1f);
        goNear.setArrivalTolerance(1f);
        goNear.setDecelerationRadius(4f);
        return goNear;
    }

    public static Arrive<Vector2> createGoToBehavior(SteeringGameWorldEntity owner, Box2dSteeringEntity target) {
        Arrive<Vector2> goTo = new Arrive<Vector2>(owner.getBox2dComponent(), target);
        goTo.setTimeToTarget(0.01f);
        goTo.setArrivalTolerance(0.01f);
        goTo.setDecelerationRadius(2f);
        return goTo;
    }

    public static Seek<Vector2> createSeekBehavior(SteeringGameWorldEntity owner, Box2dSteeringEntity target) {
        return new Seek<Vector2>(owner.getBox2dComponent(), target);
    }

    public static Pursue<Vector2> createPursueBehavior(Box2dSteeringEntity pursuer, Box2dSteeringEntity target) {
        Pursue<Vector2> pursue = new Pursue<Vector2>(pursuer, target);
        return pursue;
    }

    public static Evade<Vector2> createFleeBehavior(Box2dSteeringEntity owner, Box2dSteeringEntity attacker) {
        Evade<Vector2> evadeBehavior = new Evade<Vector2>(owner, attacker, .2f);
        return evadeBehavior;
    }

    public static Wander<Vector2> createWanderBehavior(PivotableSteeringGameWorldEntity owner) {
        Wander<Vector2> wander = new Wander<Vector2>(owner.getBox2dComponent());
//        wander.setFaceEnabled(true); // We want to use Face internally (independent facing is on)
        wander.setAlignTolerance(0.001f); // Used by Face
        wander.setDecelerationRadius(1); // Used by Face
        wander.setTimeToTarget(0.1f); // Used by Face
        wander.setWanderOffset(3); //
        wander.setWanderOrientation(3); //
        wander.setWanderRadius(1); //
        wander.setWanderRate(50);
        return wander;
    }

}
