package com.monsieur_h.monolith;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.monsieur_h.monolith.dto.TimeManager;
import com.monsieur_h.monolith.engine.ConfigReader;
import com.monsieur_h.monolith.engine.GameCamera;
import com.monsieur_h.monolith.gameplay.MainScene;

public class MonolithGame extends ApplicationAdapter {
    public static final float MIN_PIXEL_HEIGHT = 256f;
    public static final int MAX_ZOOM_FACTOR = 10;
    public static final String APP_VERSION = "0.1 Alpha";
    public static final String APP_NAME = "monolith";
    public static float METERS_TO_PIXEL_RATIO = 50f;
    SpriteBatch batch;
    private World world;
    private MainScene scene;
    private GameCamera camera;
    private Viewport viewport;

    public static float pixelsToMeters(int pixels) {
        return (float) pixels / METERS_TO_PIXEL_RATIO;
    }

    public static int metersToPixels(float meters) {
        return (int) (meters * METERS_TO_PIXEL_RATIO);
    }

    @Override
    public void create() {
        ConfigReader.init();
        batch = new SpriteBatch();
        world = new World(Vector2.Zero, true);
        camera = new GameCamera();
        viewport = new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), camera);
        scene = new MainScene(world, viewport);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, 1);
        viewport.setScreenBounds(0, 0, width, height);
        viewport.setWorldSize(width, height);
        camera.update();
        scene.resize(width, height);
        viewport.update(width, height);
        camera.minZoom = MIN_PIXEL_HEIGHT / (float) height;
        camera.maxZoom = MAX_ZOOM_FACTOR * camera.minZoom;
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();

        float deltaTime = Gdx.graphics.getDeltaTime();
        deltaTime = MathUtils.clamp(deltaTime, 0, 1 / 30f);//Max 1/30 to simulate 30FPS when debugging
        TimeManager.update(deltaTime);
        scene.act(deltaTime);
        scene.draw();
        MessageManager.getInstance().update(deltaTime);

        batch.end();
    }
}
