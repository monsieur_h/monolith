package com.monsieur_h.monolith.engine;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

class CameraShaker {
    private Vector2 shake = new Vector2();
    private float duration = 0;
    private float strength = 0;
    private float elapsed = 0;

    private void reset() {
        shake.set(0, 0);
        duration = 0;
        strength = 0;
        elapsed = 0;
    }

    Vector3 apply(Vector3 cameraPos) {
        return cameraPos.add(shake.x, shake.y, 0);
    }

    void shake(float duration, float strength) {
        reset();
        this.duration = duration;
        this.strength = strength;
    }

    public void update(float deltaTime) {
        if (duration == 0) {
            return;
        }
        elapsed += deltaTime;
        if (elapsed >= duration) {
            reset();
        }
        shake.nor();
        shake.setToRandomDirection();
        shake.scl(strength);
    }
}
