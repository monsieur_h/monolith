package com.monsieur_h.monolith.engine.graphics.gui;

import com.badlogic.gdx.ai.fsm.State;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.ai.VillagerStateChangeListener;
import com.monsieur_h.monolith.engine.ai.states.VillagerState;
import com.monsieur_h.monolith.engine.entities.VillagerCreationListener;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;
import com.monsieur_h.monolith.gameplay.MainScene;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class VillagerUIManager implements VillagerStateChangeListener, VillagerCreationListener {
    private MainScene scene;
    private ThinkBubbleFactory bubbleFactory = new ThinkBubbleFactory();

    private GUIFactory guiFactory = new GUIFactory();
    private HashMap<VillagerAI, VillagerDetailWidget> widgets = new HashMap<VillagerAI, VillagerDetailWidget>();

    public VillagerUIManager(MainScene scene) {
        this.scene = scene;
    }

    @Override
    public void onStateChanged(VillagerAI villagerAI, State<VillagerAI> previousState, State<VillagerAI> currentState) {
        if (bubbleFactory.hasMatchingIcon((VillagerState) currentState)) {
            final ThinkBubble thinkBubble = bubbleFactory.create(villagerAI.getParentEntity());
            scene.addActor(thinkBubble, MainScene.Layer.FOREGROUND);
        }
    }

    public void askForVillagerWidget(VillagerAI villagerAI) {
        if (!widgets.containsKey(villagerAI)) {
            final VillagerDetailWidget detailWidget = createDetailWidget(villagerAI);

            widgets.put(villagerAI, detailWidget);

            scene.addActor(detailWidget, MainScene.Layer.GUI);
            detailWidget.show();
        }
        widgets.get(villagerAI).getRefCounter().addDependency();
    }

    public void update() {
        for (Iterator<Map.Entry<VillagerAI, VillagerDetailWidget>> iterator = widgets.entrySet().iterator(); iterator.hasNext(); ) {
            Map.Entry<VillagerAI, VillagerDetailWidget> entry = iterator.next();
            VillagerDetailWidget currentWidget = entry.getValue();
            if (currentWidget.getRefCounter().isDisposable()) {
                currentWidget.destroy();
                iterator.remove();
            }
        }
    }

    public void releaseVillagerWidgetDependency(VillagerAI villagerAI) {
        widgets.get(villagerAI).getRefCounter().releaseDependency();
    }

    @Override
    public void onGrab(VillagerAI villagerAI) {
        askForVillagerWidget(villagerAI);
    }

    @Override
    public void onReleased(VillagerAI villagerAI) {
        releaseVillagerWidgetDependency(villagerAI);
    }

    private VillagerDetailWidget createDetailWidget(VillagerAI villagerAI) {
        return guiFactory.createDetailWidget(villagerAI.getParentEntity());
    }

    @Override
    public void onVillagerCreated(VillagerEntity createdVillager) {
        createdVillager.getVillagerAI().registerChangeListener(this);
    }

    public void setGUIFactory(GUIFactory GUIFactory) {
        this.guiFactory = GUIFactory;
    }
}
