package com.monsieur_h.monolith.engine.graphics.gui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.monsieur_h.monolith.dto.BehaviorLog;
import com.monsieur_h.monolith.engine.ai.BiologicalNeed;
import com.monsieur_h.monolith.engine.ai.BiologicalStatus;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;

import java.util.HashMap;

public class VillagerDetailWidget extends Window {
    public static final float ANIMATION_DURATION = .25f;
    public static final float PADDING = 2.5f;
    private static float ANIMATION_OFFSET = 100f;
    private Image villagerImage;
    private VillagerEntity villager;
    private Skin skin;
    private HashMap<BiologicalStatus.NeedType, ProgressBar> needProgressBars = new HashMap<BiologicalStatus.NeedType, ProgressBar>();
    private BehaviorLog lastPlan;
    private BehaviorLog lastState;
    private Label planLabel;
    private Label stateLabel;
    private RefCounter refCounter = new RefCounter();

    public VillagerDetailWidget(VillagerEntity villager, Skin skin) {
        super(villager.getVillagerAI().getName(), skin);
        this.villager = villager;
        this.skin = skin;
        setupImage(villager);
        setupThoughts();
        setupNeedProgressBars();
        validate();
        setHeight(getPrefHeight());
        setWidth(getPrefWidth());
    }

    private void setupThoughts() {
        lastPlan = villager.getVillagerAI().getLastPlans().getCurrent();
        lastState = villager.getVillagerAI().getLastActions().getCurrent();

        String currentPlan = lastPlan.toLocalFormat();
        String currentState = lastState.toLocalFormat();
        planLabel = new Label(currentPlan, skin);
        add(planLabel).colspan(2).pad(PADDING);
        row();
        stateLabel = new Label(currentState, skin);
        add(stateLabel).colspan(2).pad(PADDING);
        row();
    }

    private void setupImage(VillagerEntity villager) {
        villagerImage = new Image(villager.getImage().getDrawable());
        add(villagerImage).colspan(BiologicalStatus.NeedType.values().length).pad(PADDING);
        row();
    }

    private void setupNeedProgressBars() {
        for (BiologicalStatus.NeedType type : BiologicalStatus.NeedType.values()) {
            final Label nameLabel = new Label(type.toString(), skin);
            final float needSatisfaction = villager.getVillagerAI().getNeedSatisfaction(type);
            final ProgressBar progressBar = new ProgressBar(BiologicalNeed.MIN_SATISFACTION, BiologicalNeed.MAX_SATISFACTION, .01f, false, skin);
            progressBar.setValue(needSatisfaction);
            add(nameLabel).right().pad(PADDING);
            add(progressBar).left().pad(PADDING);
            row();

            needProgressBars.put(type, progressBar);
        }
    }

    public void show() {
        setPosition(ANIMATION_OFFSET, ANIMATION_OFFSET);
        addAction(
                Actions.sequence(
                        Actions.fadeOut(0),
                        Actions.moveTo(0, ANIMATION_OFFSET, 0),
                        Actions.parallel(
                                Actions.fadeIn(ANIMATION_DURATION),
                                Actions.moveTo(0, 0, ANIMATION_DURATION, Interpolation.sineOut)
                        )
                )
        );
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        updateProgressBars();
        updatePlans();
    }

    public void updateProgressBars() {
        for (BiologicalStatus.NeedType type : BiologicalStatus.NeedType.values()) {
            needProgressBars.get(type).setValue(villager.getVillagerAI().getNeedSatisfaction(type));
        }
    }

    private void updatePlans() {
        final BehaviorLog currentPlan = villager.getVillagerAI().getLastPlans().getCurrent();
        final BehaviorLog currentState = villager.getVillagerAI().getLastActions().getCurrent();
        if (currentPlan != lastPlan) {
            planLabel.setText(currentPlan.toLocalFormat());
            planLabel.addAction(createLabelUpdateAction());
            lastPlan = currentPlan;
        }

        if (currentState != lastState) {
            stateLabel.setText(currentState.toLocalFormat());
            stateLabel.addAction(createLabelUpdateAction());
            lastState = currentState;
        }
    }

    private Action createLabelUpdateAction() {
        return Actions.sequence(
                Actions.parallel(
                        Actions.fadeOut(0),
                        Actions.moveBy(0, -10, 0)
                ),
                Actions.parallel(
                        Actions.fadeIn(.5f),
                        Actions.moveBy(0, 10, .5f)
                )
        );
    }

    public void destroy() {
        addAction(
                Actions.sequence(
                        Actions.parallel(
                                Actions.fadeOut(ANIMATION_DURATION / 2),
                                Actions.moveTo(0, ANIMATION_OFFSET, ANIMATION_DURATION, Interpolation.sineOut)
                        ),
                        Actions.removeActor()
                )
        );
    }

    public RefCounter getRefCounter() {
        return refCounter;
    }
}
