package com.monsieur_h.monolith.engine.graphics.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;
import com.monsieur_h.monolith.gameplay.MainScene;

class GUIFactory {

    private final Skin defaultSkin;

    GUIFactory() {
        final FileHandle skinJson = Gdx.files.internal("gui/uiskin.json");
        defaultSkin = new Skin(skinJson);
    }

    VillagerDetailWidget createDetailWidget(VillagerEntity villager) {
        return new VillagerDetailWidget(villager, defaultSkin);
    }

    SpellCastMenu createSpellCastMenu(MainScene scene) {
        return new SpellCastMenu(defaultSkin, scene);
    }

    Skin getDefaultSkin() {
        return defaultSkin;
    }
}
