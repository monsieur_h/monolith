package com.monsieur_h.monolith.engine.graphics.gui;


public class RefCounter {
    private int refCount = 0;

    public void addDependency() {
        refCount++;
    }

    public void releaseDependency() {
        refCount--;
    }

    public boolean isDisposable() {
        return refCount == 0;
    }
}
