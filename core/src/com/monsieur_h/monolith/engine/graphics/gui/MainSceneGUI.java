package com.monsieur_h.monolith.engine.graphics.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.dto.FPSLabel;
import com.monsieur_h.monolith.engine.entities.VillagerEntityFactory;
import com.monsieur_h.monolith.gameplay.MainScene;

public class MainSceneGUI {
    private final VillagerUIManager villagerUIManager;
    private MainScene scene;
    private Stage stage;
    private GUIFactory guiFactory = new GUIFactory();

    public MainSceneGUI(MainScene mainScene) {
        this.scene = mainScene;
        this.stage = new Stage(new FitViewport(scene.getWidth(), scene.getHeight()));

        villagerUIManager = new VillagerUIManager(scene);
        villagerUIManager.setGUIFactory(guiFactory);

        Group guiLayer = scene.layers.get(MainScene.Layer.GUI);
        stage.addActor(guiLayer);//So we can draw what's added to the layer by other components
        initSpellCastMenu();
        initFPSLabel();
        initWaterMark();
    }

    private void initWaterMark() {
        String waterMarkContent = MonolithGame.APP_NAME + " " + Gdx.app.getType().toString() + " " + MonolithGame.APP_VERSION;
        final Label waterMarkLabel = new Label(waterMarkContent, guiFactory.getDefaultSkin());
        waterMarkLabel.setX(stage.getWidth() - waterMarkLabel.getWidth());
        stage.addActor(waterMarkLabel);
    }

    private void initFPSLabel() {
        final FPSLabel fpsLabel = new FPSLabel("0", guiFactory.getDefaultSkin());
        fpsLabel.setY(Gdx.graphics.getHeight() - fpsLabel.getHeight());
        stage.addActor(fpsLabel);
    }

    private void initSpellCastMenu() {
        SpellCastMenu spellCastMenu = guiFactory.createSpellCastMenu(scene);
        spellCastMenu.setKeepWithinStage(false);
        float overLapHeight = spellCastMenu.getOverLapHeight();
        Vector2 foldedPos = new Vector2((stage.getWidth() / 2f) - spellCastMenu.getWidth() / 2f, stage.getHeight() - overLapHeight);
        Vector2 unFoldedPos = new Vector2((stage.getWidth() / 2f) - spellCastMenu.getWidth() / 2f, stage.getHeight() - spellCastMenu.getHeight());
        spellCastMenu.setUnFoldedPosition(unFoldedPos);
        spellCastMenu.setFoldedPosition(foldedPos);
        stage.addActor(spellCastMenu);
    }

    public void draw() {
        stage.draw();
    }

    public void update(float delta) {
        villagerUIManager.update();
        stage.act(delta);
    }

    public void setVillagerFactory(VillagerEntityFactory villagerFactory) {
        villagerFactory.register(villagerUIManager);
    }

    public VillagerUIManager getVillagerUIManager() {
        return villagerUIManager;
    }

    public Stage getStage() {
        return stage;
    }

    public void resize(int width, int height) {
        stage.getViewport().setScreenWidth(width);
        stage.getViewport().setScreenHeight(height);
    }
}
