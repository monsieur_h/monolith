package com.monsieur_h.monolith.engine.graphics.gui;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.monsieur_h.monolith.engine.ai.states.VillagerState;
import com.monsieur_h.monolith.engine.ai.states.VillagerStateIcons;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;

import java.util.HashMap;

public class ThinkBubbleFactory {
    public static final String TEXTURE_THINK_FILENAME = "textures/icons/think.png";
    public static final String TEXTURE_ICON_DIR = "textures/icons/";
    public static final float TRANSITION_DURATION = .5f;
    public static final float DISPLAY_DURATION = .5f;
    private final Texture thinkBubbleTexture;
    private HashMap<VillagerStateIcons, Texture> behaviorTexture = new HashMap<VillagerStateIcons, Texture>();

    public ThinkBubbleFactory() {
        final FileHandle textureFileHandler = Gdx.files.internal(TEXTURE_THINK_FILENAME);
        thinkBubbleTexture = new Texture(textureFileHandler, true);
        thinkBubbleTexture.setFilter(Texture.TextureFilter.MipMapLinearLinear, Texture.TextureFilter.MipMapLinearNearest);
        initStorage();

    }

    private void initStorage() {
        bind("wander.png", VillagerStateIcons.WANDERING);
        bind("eating.png", VillagerStateIcons.EATING);
        bind("walking-boot.png", VillagerStateIcons.APPROACHING);
        bind("grab.png", VillagerStateIcons.GATHERING);
        bind("trade.png", VillagerStateIcons.STORING);
        bind("surprised.png", VillagerStateIcons.STUN);
        bind("light-bulb.png", VillagerStateIcons.THINKING);
        bind("confrontation.png", VillagerStateIcons.BEATING);
        bind("dead-head.png", VillagerStateIcons.DEAD);
        bind("kneeling.png", VillagerStateIcons.PRAY);
    }

    private void bind(String textureName, VillagerStateIcons icon) {
        Texture texture = new Texture(TEXTURE_ICON_DIR + textureName);
        behaviorTexture.put(icon, texture);
    }

    public ThinkBubble create(VillagerEntity target) {
        ThinkBubble bubble = new ThinkBubble(target);
        bubble.setBackground(thinkBubbleTexture);
        bubble.setIcon(behaviorTexture.get(target.getCurrentStateType()));
        bubble.addAction(createDefaultBubbleActions());
        bubble.act(0);

        return bubble;
    }

    private Action createDefaultBubbleActions() {
        return Actions.sequence(
                createAppearAction(),
                Actions.delay(DISPLAY_DURATION),
                createDisappearAction()
        );
    }

    private Action createAppearAction() {
        return Actions.sequence(
                Actions.fadeOut(0),
                Actions.scaleTo(0f, 0.2f),
                Actions.parallel(
                        Actions.fadeIn(TRANSITION_DURATION / 2f),
                        Actions.scaleTo(1f, 1f, TRANSITION_DURATION, Interpolation.swing)
                )
        );
    }

    private Action createDisappearAction() {
        return Actions.sequence(
                Actions.parallel(
                        Actions.fadeOut(TRANSITION_DURATION * 2),
                        Actions.scaleTo(0f, .2f, TRANSITION_DURATION, Interpolation.swing)
                ),
                Actions.removeActor()
        );
    }

    public boolean hasMatchingIcon(VillagerState currentState) {
        return behaviorTexture.get(currentState.getIcon()) != null;
    }


}
