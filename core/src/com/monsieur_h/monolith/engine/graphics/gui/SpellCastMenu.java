package com.monsieur_h.monolith.engine.graphics.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.utils.Align;
import com.monsieur_h.monolith.engine.generation.TileDef;
import com.monsieur_h.monolith.gameplay.MainScene;
import com.monsieur_h.monolith.gameplay.spell.CreateResourcesSpell;
import com.monsieur_h.monolith.gameplay.spell.TerraformSpell;

import java.util.HashMap;
import java.util.Map;

class SpellCastMenu extends Window { //TODO: Move to a side instead
    private static SpriteDrawable WATER_BUTTON_DRAWABLE;
    private final Skin skin;
    private final MainScene scene;
    private Label unFoldLabel;

    private Vector2 foldedPosition;
    private Vector2 unFoldedPosition;
    private boolean isFolded = true;

    SpellCastMenu(Skin skin, final MainScene scene) {
        super("SPELL_MENU", skin);
        this.skin = skin;
        this.scene = scene;
        Map<String, SpriteDrawable> iconsMap = loadImages();
        addButtons(iconsMap);
        addLabel();

        validate();
        setHeight(getPrefHeight());
        setWidth(getPrefWidth());
    }

    private void addButtons(Map<String, SpriteDrawable> iconsMap) {
        SpriteDrawable icon;
        Button button;

        addTerraformButton(iconsMap.get("water"), TileDef.TileType.WATER);
        addTerraformButton(iconsMap.get("mountain"), TileDef.TileType.HILL);

        addButtonWithListener(iconsMap.get("tree"), new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                new CreateResourcesSpell(scene.getInputMultiplexer());
                super.clicked(event, x, y);
            }
        });
    }

    private void addTerraformButton(SpriteDrawable icon, final TileDef.TileType tileType) {
        addButtonWithListener(icon, new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                new TerraformSpell(scene.getInputMultiplexer(), tileType);
                super.clicked(event, x, y);
            }
        });
    }

    private void addButtonWithListener(SpriteDrawable icon, ClickListener listener) {
        final Button button = new Button(icon);
        button.addListener(listener);
        button.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                onButtonClicked(button);
                fold();
                super.clicked(event, x, y);
            }
        });
        add(button);
    }

    private void onButtonClicked(Button finalButton) {
        finalButton.addAction(
                Actions.sequence(
                        Actions.color(Color.GREEN, .0f),
                        Actions.color(Color.WHITE, .25f)
                )
        );
        fold();
    }

    private void addLabel() {
        row();
        unFoldLabel = new Label("UNFOLD", skin);
        unFoldLabel.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                toggleFold();
                super.clicked(event, x, y);
            }
        });
        add(unFoldLabel).align(Align.center).colspan(10);
    }

    private Map<String, SpriteDrawable> loadImages() {
        Map<String, SpriteDrawable> map = new HashMap<String, SpriteDrawable>();
        map.put("water", new SpriteDrawable(new Sprite(new Texture("textures/icons/water.png"))));
        map.put("mountain", new SpriteDrawable(new Sprite(new Texture("textures/icons/mountains.png"))));
        map.put("tree", new SpriteDrawable(new Sprite(new Texture("textures/icons/tree.png"))));
        return map;
    }

    private void toggleFold() {
        clearActions();
        Vector2 targetPosition = isFolded ? unFoldedPosition : foldedPosition;
        addAction(Actions.moveTo(targetPosition.x, targetPosition.y, .5f, Interpolation.pow3));
        isFolded = !isFolded;
    }

    private void fold() {
        if (!isFolded) {
            toggleFold();
        }
    }

    private void unfold() {
        if (isFolded) {
            toggleFold();
        }
    }

    float getOverLapHeight() {
        return unFoldLabel.getPrefHeight();
    }

    void setUnFoldedPosition(Vector2 unFoldedPosition) {
        this.unFoldedPosition = unFoldedPosition;
        if (!isFolded) {
            setPosition(unFoldedPosition.x, unFoldedPosition.y);
        }
    }

    void setFoldedPosition(Vector2 foldedPosition) {
        this.foldedPosition = foldedPosition;
        if (isFolded) {
            setPosition(foldedPosition.x, foldedPosition.y);
        }
    }
}
