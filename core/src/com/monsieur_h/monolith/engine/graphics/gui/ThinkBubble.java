package com.monsieur_h.monolith.engine.graphics.gui;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class ThinkBubble extends Group {
    private static Color defaultIconColor = new Color(.2f, .2f, .2f, 1f);
    private Actor target;
    private Image bubbleImage;
    private Image iconImage;

    public ThinkBubble(Actor target) {
        this.target = target;
    }

    public void setBackground(Texture texture) {
        bubbleImage = new Image(texture);
        bubbleImage.setTouchable(Touchable.disabled);
        addActor(bubbleImage);
        setWidth(bubbleImage.getWidth());
        setHeight(bubbleImage.getHeight());
        setOrigin(getWidth() / 2f, 0f);
    }

    public void setIcon(Texture icon) {
        iconImage = new Image(icon);
        iconImage.setTouchable(Touchable.disabled);
        addActor(iconImage);

        setIconColor();

        resizeIcon();

        final float yOffset = bubbleImage.getHeight() * 10 / 100f;
        iconImage.setPosition(
                bubbleImage.getWidth() / 2f - (iconImage.getWidth() * iconImage.getScaleX() / 2f),
                bubbleImage.getHeight() / 2f - (iconImage.getHeight() * iconImage.getScaleY() / 2f) + yOffset
        );
    }

    private void setIconColor() {
        iconImage.setColor(defaultIconColor);
    }

    private void resizeIcon() {
        final float iconDiagonal = iconImage.getWidth() + iconImage.getHeight();
        float bubbleDiagonal = bubbleImage.getWidth() + bubbleImage.getHeight();
        bubbleDiagonal -= bubbleDiagonal * 60f / 100f;
        final float ratio = bubbleDiagonal / iconDiagonal;
        iconImage.setScale(ratio);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        setPosition(target.getX(), target.getY());
    }
}
