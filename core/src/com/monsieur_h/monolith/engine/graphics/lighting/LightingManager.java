package com.monsieur_h.monolith.engine.graphics.lighting;

import box2dLight.RayHandler;
import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.World;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.engine.GameCamera;

public abstract class LightingManager {
    public static final int MAX_LIGHT_DISTANCE = 500;
    public static final int RAY_COUNT = (Gdx.app.getType() != Application.ApplicationType.Desktop) ? 512 : 2048;

    protected final GameCamera camera;
    protected final World world;
    protected final RayHandler rayHandler;


    public LightingManager(World world, GameCamera camera) {
        this.world = world;
        this.camera = camera;
        rayHandler = new RayHandler(world);
    }

    public abstract void update();

    @SuppressWarnings("deprecation")
    //Suppresses the warning for rayHandler.setCombinedMatrix() since the new API wants the camera, and we don't want to render at camera scale
    public void draw() {
        Matrix4 combined = camera.combined;
        combined.scl(MonolithGame.METERS_TO_PIXEL_RATIO);

        rayHandler.setCombinedMatrix(combined);
        rayHandler.render();
        combined.scl(1 / MonolithGame.METERS_TO_PIXEL_RATIO);
    }

    public RayHandler getRayHandler() {
        return rayHandler;
    }

    public abstract void setEnabled(boolean active);
}
