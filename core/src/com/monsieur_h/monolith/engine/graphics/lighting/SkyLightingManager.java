package com.monsieur_h.monolith.engine.graphics.lighting;

import box2dLight.ConeLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.JsonValue;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.dto.TimeManager;
import com.monsieur_h.monolith.engine.ConfigReader;
import com.monsieur_h.monolith.engine.GameCamera;

public class SkyLightingManager extends LightingManager {
    public static final int DEFAULT_CONE_DEGREE = 35;
    public static final float MAX_SUNLIGHT_INTENSITY = .5f;
    private final Color ambientColor = new Color();
    private float skyBoxRadius;
    private ConeLight sunLight;
    private Vector2 centerOfRotation = new Vector2(0, 0);
    private float minAmbient;
    private float maxAmbient;

    public SkyLightingManager(World world, float lightDistance, GameCamera camera) {
        super(world, camera);
        skyBoxRadius = MonolithGame.pixelsToMeters((int) lightDistance);
        rayHandler.setBlur(true);
        initLights();
        initConfig();
    }

    private void initConfig() {
        JsonValue lightConfig = ConfigReader.getConfigRootNode(ConfigReader.Configurable.LIGHTING);
        JsonValue skyConfig = lightConfig.get(getClass().getSimpleName());
        maxAmbient = skyConfig.get("maxAmbient").asFloat();
        minAmbient = skyConfig.get("minAmbient").asFloat();
    }

    private void initLights() {
        sunLight = new ConeLight(
                rayHandler,
                RAY_COUNT,
                new Color(0, 0, 0, MAX_SUNLIGHT_INTENSITY),
                MAX_LIGHT_DISTANCE,
                centerOfRotation.x,
                centerOfRotation.y - skyBoxRadius,
                -180,
                DEFAULT_CONE_DEGREE
        );
    }

    @Override
    public void update() {
        final double timeOfTheDay = TimeManager.getTimeOfTheDay();
        updateSunLight(timeOfTheDay);

        updateAmbientLight(timeOfTheDay);

        rayHandler.update();
    }

    private void updateAmbientLight(double timeOfTheDay) {
        updateAmbientIntensity(timeOfTheDay);
        updateAmbientColor(timeOfTheDay);
        rayHandler.setAmbientLight(ambientColor.r, ambientColor.g, ambientColor.b, ambientColor.a);
    }

    private void updateAmbientColor(double timeOfTheDay) {
        final float blueRatio = 1 + (float) Math.sin(timeOfTheDay * (Math.PI) + Math.PI);
        float minColor = 0f;
        float maxColor = .25f;
        ambientColor.b = (maxColor - minColor) * blueRatio + minColor;
    }

    private void updateAmbientIntensity(double timeOfTheDay) {
        final float ratio = (float) Math.sin(timeOfTheDay * (Math.PI));
        ambientColor.a = minAmbient + ((maxAmbient - minAmbient) * ratio);
    }

    private void updateSunLight(double timeOfTheDay) {
        float angle = (float) (timeOfTheDay * 360);
        angle = (angle - 90) % 360;

        final Vector2 sunLightPosition = sunLight.getPosition();
        final Vector2 sunLightDirection = sunLightPosition.cpy().sub(centerOfRotation);
        final float currentSunAngle = sunLightDirection.angle();
        final float finalAngle = angle - currentSunAngle;
        sunLightDirection.rotate(finalAngle);
        sunLight.setPosition(centerOfRotation.cpy().add(sunLightDirection));
        sunLight.setDirection(sunLightDirection.cpy().scl(-1).angle());


        final float currentZoomRatio = camera.getCurrentZoomRatio();
        sunLight.setColor(0, 0, 0, currentZoomRatio * MAX_SUNLIGHT_INTENSITY);

        final float dst = (float) (Math.sin(timeOfTheDay * Math.PI) * MAX_LIGHT_DISTANCE);
        sunLight.setDistance(dst);
    }

    public void setEnabled(boolean enabled) {
        sunLight.setActive(enabled);
    }


}
