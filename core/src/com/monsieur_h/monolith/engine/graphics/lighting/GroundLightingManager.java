package com.monsieur_h.monolith.engine.graphics.lighting;

import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.JsonValue;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.dto.TimeManager;
import com.monsieur_h.monolith.engine.ConfigReader;
import com.monsieur_h.monolith.engine.GameCamera;
import com.monsieur_h.monolith.engine.physics.CollisionFilter;

public class GroundLightingManager extends LightingManager {

    private final PointLight pointerLight;
    private float maxLightIntensity;
    private float minLightIntensity;

    public GroundLightingManager(World world, GameCamera camera) {
        super(world, camera);
        pointerLight = new PointLight(
                rayHandler,
                RAY_COUNT,
                new Color(1f, .25f, .25f, .5f),
                15,
                0,
                0
        );
        pointerLight.setContactFilter(
                CollisionFilter.asBits(CollisionFilter.Tag.ALL),
                (short) 0x0000,
                CollisionFilter.asBits(CollisionFilter.Tag.ENTITY)
        );

        rayHandler.setBlur(true);

        initConfig();
    }

    private void initConfig() {
        JsonValue lightConfig = ConfigReader.getConfigRootNode(ConfigReader.Configurable.LIGHTING);
        JsonValue skyConfig = lightConfig.get(getClass().getSimpleName());
        maxLightIntensity = skyConfig.get("maxLightIntensity").asFloat();
        minLightIntensity = skyConfig.get("minLightIntensity").asFloat();
        float ambient = skyConfig.get("ambient").asFloat();
        rayHandler.setAmbientLight(ambient);
    }

    @Override
    public void update() {
        rayHandler.update();
        updateAmbientIntensity(TimeManager.getTimeOfTheDay());
    }

    private void updateAmbientIntensity(double timeOfTheDay) {
        final float ratio = (float) (1 - Math.sin(timeOfTheDay * (Math.PI)));
        Color ambientColor = pointerLight.getColor();
        ambientColor.a = minLightIntensity + ((maxLightIntensity - minLightIntensity) * ratio);
        pointerLight.setColor(ambientColor);
    }

    @Override
    public void setEnabled(boolean active) {
        pointerLight.setActive(active);
    }

    public void setPointerLightPosition(float x, float y) {
        Vector3 unproject = camera.unproject(new Vector3(x, y, 0));
        unproject.scl(1 / MonolithGame.METERS_TO_PIXEL_RATIO);
        pointerLight.setPosition(unproject.x, unproject.y);
    }

}
