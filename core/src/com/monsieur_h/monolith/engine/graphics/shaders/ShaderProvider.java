package com.monsieur_h.monolith.engine.graphics.shaders;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.GdxRuntimeException;

import java.util.HashMap;

public class ShaderProvider {
    public static final String GRAPHICS_SHADERS_DIR = "graphics/shaders/";
    private HashMap<Shader, ShaderProgram> storage = new HashMap<Shader, ShaderProgram>();

    public ShaderProvider() {
        loadShaderPrograms();
    }

    private void loadShaderPrograms() {
        for (Shader shader : Shader.values()) {
            final String shaderBaseName = shader.toString().toLowerCase();
            final ShaderProgram shaderProgram = new ShaderProgram(
                    Gdx.files.internal(GRAPHICS_SHADERS_DIR + shaderBaseName + "_vert.glsl"),
                    Gdx.files.internal(GRAPHICS_SHADERS_DIR + shaderBaseName + "_frag.glsl")
            );
            if (!shaderProgram.isCompiled()) {
                throw new GdxRuntimeException("Could not compile shader " + shader.toString() + "\n" + shaderProgram.getLog());
            }
            storage.put(shader, shaderProgram);
        }
    }

    public ShaderProgram getShader(Shader shader) {
        return storage.get(shader);
    }

    public enum Shader {
        BLUR
    }
}
