package com.monsieur_h.monolith.engine.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;
import java.util.HashMap;

public class KenneyAtlas {
    private final String atlasFilename;
    XmlReader.Element rootElement;
    private Texture texture;
    private HashMap<String, TextureRegion> storage = new HashMap<String, TextureRegion>();
    private String textureFileName;

    public KenneyAtlas(String atlasFilename) {
        this.atlasFilename = atlasFilename;
        loadXml();
        textureFileName = rootElement.getAttribute("imagePath");
        loadData();
    }

    public KenneyAtlas(String atlasFilename, String textureFilename) {
        this.atlasFilename = atlasFilename;
        this.textureFileName = textureFilename;
        loadXml();
        loadData();
    }

    private void loadData() {
        loadTexture();
        loadRegions();
    }

    private void loadXml() {
        XmlReader reader = new XmlReader();
        try {
            rootElement = reader.parse(Gdx.files.internal(atlasFilename));
        } catch (IOException e) {
            throw new GdxRuntimeException(e);
        }
    }

    private void loadRegions() {
        for (int i = 0; i < rootElement.getChildCount(); i++) {
            final XmlReader.Element child = rootElement.getChild(i);
            final String regionName = child.getAttribute("name");
            final int x = child.getInt("x");
            final int y = child.getInt("y");
            final int width = child.getInt("width");
            final int height = child.getInt("height");
            final TextureRegion region = new TextureRegion(texture, x, y, width, height);
            storage.put(regionName, region);
        }
    }

    private void loadTexture() {
        final int lastSlash = atlasFilename.lastIndexOf("/");
        String atlasFilename;

        if (lastSlash != -1) {
            final String folderPath = this.atlasFilename.substring(0, lastSlash);
            atlasFilename = folderPath + "/" + this.textureFileName;
        } else {
            atlasFilename = textureFileName;
        }
        texture = new Texture(Gdx.files.internal(atlasFilename), true);
    }

    public TextureRegion getRegion(String regionName) {
        return storage.get(regionName);
    }
}
