package com.monsieur_h.monolith.engine.graphics;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.engine.graphics.lighting.GroundLightingManager;
import com.monsieur_h.monolith.engine.input.GameWorldInputMultiplexer;
import com.monsieur_h.monolith.engine.input.InputHandler;
import com.monsieur_h.monolith.gameplay.MainScene;

public class PointerLightHandler implements InputHandler {


    private final boolean isDesktopMode;
    private GroundLightingManager groundLightingManager;

    public PointerLightHandler(GameWorldInputMultiplexer gameWorldInputMultiplexer, MainScene scene) {
        gameWorldInputMultiplexer.register(this);
        isDesktopMode = (Gdx.app.getType() == Application.ApplicationType.Desktop);
        groundLightingManager = scene.getGroundLightingManager();
        if (isDesktopMode) {
            groundLightingManager.setEnabled(true);
        }
    }

    private void updatePointerLightPosition(float x, float y) {
        groundLightingManager.setPointerLightPosition(x, y);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (isDesktopMode) {
            updatePointerLightPosition(screenX, screenY);
        }
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        updatePointerLightPosition(screenX, screenY);
        if (!isDesktopMode) {
            groundLightingManager.setEnabled(true);
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (!isDesktopMode) {
            groundLightingManager.setEnabled(false);
        }
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        updatePointerLightPosition(x + deltaX, y + deltaY);
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        updatePointerLightPosition(screenX, screenY);
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

}
