package com.monsieur_h.monolith.engine.graphics.sky;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.monsieur_h.monolith.MonolithGame;

import java.util.Random;

public class CloudFactory {
    private static TextureRegion[] cloudTextures;
    private Random randomGenerator = new Random();
    private World world;

    public CloudFactory(World world) {
        this.world = world;
        initTextures();
    }

    private void initTextures() {
        cloudTextures = new TextureRegion[4];
        cloudTextures[0] = new TextureRegion(new Texture("textures/sky/clouds_01.png"));
        cloudTextures[1] = new TextureRegion(new Texture("textures/sky/clouds_02.png"));
        cloudTextures[2] = new TextureRegion(new Texture("textures/sky/clouds_03.png"));
        cloudTextures[3] = new TextureRegion(new Texture("textures/sky/clouds_04.png"));
    }

    public Cloud createCloud(Vector2 startPosition) {
        final TextureRegion randomTexture = cloudTextures[randomGenerator.nextInt(cloudTextures.length)];
        Body cloudBody = createBody(randomTexture, startPosition);
        Cloud cloud = new Cloud(new TextureRegion(randomTexture), cloudBody);
        cloud.setOrigin(cloud.getWidth() / 2f, cloud.getHeight() / 2f);
        return cloud;
    }

    private Body createBody(TextureRegion randomTexture, Vector2 startPosition) {
        BodyDef def = new BodyDef();
        def.position.set(startPosition);
        def.type = BodyDef.BodyType.DynamicBody;
        def.linearVelocity.set(randomGenerator.nextFloat() + 1, 0);
        final Body body = world.createBody(def);

        FixtureDef fixtureDef = new FixtureDef();
        CircleShape circleShape = new CircleShape();

        //Default centered fixture
        final float defaultRadius = MonolithGame.pixelsToMeters(randomTexture.getRegionWidth()) / 16f;
        circleShape.setRadius(defaultRadius);
        fixtureDef.shape = circleShape;
        fixtureDef.isSensor = true;
        body.createFixture(fixtureDef);


        //Random additionnal fixtures
        final int additionalFixtureCount = randomGenerator.nextInt(5) + 1;
        for (int i = 0; i < additionalFixtureCount; i++) {
            final Vector2 position = new Vector2();
            position.setToRandomDirection().scl((defaultRadius * 2) * (randomGenerator.nextFloat() + 1));
            circleShape.setPosition(position);
            circleShape.setRadius(randomGenerator.nextFloat() * .5f + .5f);
            body.createFixture(fixtureDef);
        }
        return body;
    }

    public World getWorld() {
        return world;
    }

}
