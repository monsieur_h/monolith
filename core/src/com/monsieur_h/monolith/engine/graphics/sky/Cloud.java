package com.monsieur_h.monolith.engine.graphics.sky;


import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.engine.entities.GameWorldEntity;

public class Cloud extends GameWorldEntity {

    private boolean alive = true;

    public Cloud(TextureRegion texture, Body body) {
        super(EntityType.CLOUD);
        setupImage(texture);
        final Image image = getImage();
        image.setPosition(-image.getWidth() / 2f, -image.getHeight() / 2f);
        setupBody(body);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        updateGraphicalPosition();
    }

    private void updateGraphicalPosition() {
        final Vector2 bodyPosition = getBody().getPosition();
        setPosition(
                MonolithGame.metersToPixels(bodyPosition.x),
                MonolithGame.metersToPixels(bodyPosition.y)
        );
    }

    public void dispose() {
        if (alive) {
            remove();
            getBody().getWorld().destroyBody(getBody());
        }
        alive = false;

    }
}
