package com.monsieur_h.monolith.engine.graphics.sky;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.monsieur_h.monolith.dto.TimeManager;

public class Sky extends Stage {
    public static final String SKYBOX_FILENAME = "textures/sky/sky.png";
    private Image image;

    public Sky() {
        loadImage();
        placeImage();
        addActor(image);
    }

    private void placeImage() {
        image.setPosition(-image.getWidth() / 2f, -image.getHeight() / 2f);
        final Camera camera = getCamera();
        image.setX(image.getX() + camera.viewportWidth / 2f);
        image.setOrigin(image.getWidth() / 2f, image.getHeight() / 2f);

        scaleImage(camera);
    }

    private void scaleImage(Camera camera) {
        final float imgW = image.getWidth() / 2f;
        final float screenDiagonal = (float) Math.sqrt(camera.viewportWidth / 2f * camera.viewportWidth / 2 + camera.viewportHeight * camera.viewportHeight);

        final float ratio = screenDiagonal / imgW;
        image.setScale(ratio);
    }

    private void loadImage() {
        final Texture skyboxTexture = new Texture(Gdx.files.internal(SKYBOX_FILENAME), true);
        skyboxTexture.setFilter(Texture.TextureFilter.MipMapLinearLinear, Texture.TextureFilter.MipMapLinearNearest);
        image = new Image(new TextureRegion(skyboxTexture));
    }

    public Image getImage() {
        return image;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        final double timeOfTheDay = TimeManager.getTimeOfTheDay();
        image.setRotation((float) (timeOfTheDay * 360));
    }
}
