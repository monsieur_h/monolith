package com.monsieur_h.monolith.engine.graphics.sky;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.engine.GameCamera;

import java.util.Random;

public class CloudManager extends Group {
    private static int CLOUD_COUNT = 5;
    private final GameCamera camera;
    private final Vector2 centerPosition;
    private float worldRadius;
    private World world;
    private Random randomGenerator;
    private CloudFactory cloudFactory;
    private Body body;

    public CloudManager(GameCamera camera, float worldRadius) {
        initWorld();
        this.camera = camera;
        this.worldRadius = MonolithGame.pixelsToMeters((int) worldRadius);
        centerPosition = new Vector2(0, this.worldRadius);
        randomGenerator = new Random();
        cloudFactory = new CloudFactory(world);
    }

    public Body getBody() {
        return body;
    }

    private void initWorld() {
        world = new World(new Vector2(0, 0), true);
    }

    @Override
    public void act(float delta) {
        world.step(delta, 4, 2);
        super.act(delta);
        updateCloudCount();
        getColor().a = camera.getCurrentZoomRatio();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        final int blendSrcFunc = batch.getBlendSrcFunc();
        final int blendDstFunc = batch.getBlendDstFunc();
        batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
        super.draw(batch, parentAlpha);
        batch.setBlendFunction(blendSrcFunc, blendDstFunc);
    }

    private void updateCloudCount() {
        for (Actor cloudActor : getChildren()) {
            final Cloud cloud = (Cloud) cloudActor;
            final Vector2 cloudPosition = cloud.getBody().getPosition();
            if (cloudPosition.dst(centerPosition) > worldRadius) {

                cloud.addAction(Actions.sequence(
                        Actions.color(new Color(1, 1, 1, 0), .25f),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                cloud.dispose();
                            }
                        })
                ));
            }
        }

        while (getChildren().size < CLOUD_COUNT) {
            createCloud();
        }
    }

    private void createCloud() {
        Vector2 startPosition = new Vector2();
        startPosition.setToRandomDirection();
        final float distanceFromCenter = randomGenerator.nextFloat() * worldRadius;
        startPosition.setLength(distanceFromCenter);
        startPosition.add(centerPosition);

        final Cloud cloud = cloudFactory.createCloud(startPosition);
        addActor(cloud);
        cloud.setColor(new Color(1, 1, 1, 0f));
        cloud.addAction(Actions.color(new Color(1, 1, 1, .5f), .25f));

    }

    public World getWorld() {
        return world;
    }
}
