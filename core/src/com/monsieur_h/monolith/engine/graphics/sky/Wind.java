package com.monsieur_h.monolith.engine.graphics.sky;


import com.badlogic.gdx.math.Vector2;

public class Wind {
    public Vector2 direction;
    public float magnitude;

    public Wind(Vector2 direction, float magnitude) {
        this.direction = direction;
        this.magnitude = magnitude;
    }
}
