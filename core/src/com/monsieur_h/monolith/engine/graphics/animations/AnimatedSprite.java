package com.monsieur_h.monolith.engine.graphics.animations;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.monsieur_h.monolith.engine.graphics.KenneyAtlas;

import java.util.ArrayList;

public class AnimatedSprite {
    private static final float DEFAULT_TIME_PER_FRAME = .10f;
    private final int frameCount;
    private final float timePerFrame;
    private String[] regionNames;
    private int currentFrameIndex = 0;
    private float timeSinceLastStep = 0;
    private KenneyAtlas atlas;

    public AnimatedSprite(ArrayList<String> regionNames, float timePerFrame) {
        assert (regionNames.size() != 0);
        frameCount = regionNames.size();
        copyList(regionNames);

        this.timePerFrame = timePerFrame;
    }

    public AnimatedSprite(ArrayList<String> regionNames) {
        this(regionNames, DEFAULT_TIME_PER_FRAME);
    }

    private void copyList(ArrayList<String> regionNames) {
        this.regionNames = new String[frameCount];
        for (int i = 0; i < frameCount; i++) {
            this.regionNames[i] = regionNames.get(i);
        }
    }

    public void update(float delta) {
        timeSinceLastStep += delta;
        while (timeSinceLastStep >= timePerFrame) {
            step();
            timeSinceLastStep = Math.max(timeSinceLastStep - timePerFrame, 0);
        }
    }

    public void setAtlas(KenneyAtlas atlas) {
        this.atlas = atlas;
    }

    private void step() {
        currentFrameIndex = (currentFrameIndex + 1) % frameCount;
    }

    public TextureRegion getCurrentFrame() {
        assert (atlas != null);
        final String regionName = regionNames[currentFrameIndex];
        return atlas.getRegion(regionName);
    }
}
