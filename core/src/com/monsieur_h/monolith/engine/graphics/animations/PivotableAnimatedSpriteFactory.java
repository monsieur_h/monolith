package com.monsieur_h.monolith.engine.graphics.animations;

import com.monsieur_h.monolith.engine.graphics.KenneyAtlas;

import java.util.HashMap;

public class PivotableAnimatedSpriteFactory {
    public static final String TEXTURES_SPRITESHEETS_DIR = "textures/spritesheets/";
    public static final String DEFAULT_ATLAS = "citizen_atlas.xml";
    private HashMap<SpriteSheet, KenneyAtlas> availableSpriteSheets = new HashMap<SpriteSheet, KenneyAtlas>();
    private AnimatedSpriteFactory animatedSpriteFactory;

    public PivotableAnimatedSpriteFactory() {
        animatedSpriteFactory = new AnimatedSpriteFactory();
        loadSpriteSheets();
    }

    public PivotableAnimatedSprite create(SpriteSheet spriteSheet, Animation animation) {

        final KenneyAtlas atlas = availableSpriteSheets.get(spriteSheet);

        final PivotableAnimatedSprite.Direction[] orderedDirections = {
                PivotableAnimatedSprite.Direction.NORTH,
                PivotableAnimatedSprite.Direction.WEST,
                PivotableAnimatedSprite.Direction.SOUTH,
                PivotableAnimatedSprite.Direction.EAST
        };

        final PivotableAnimatedSprite pivotableAnimatedSprite = new PivotableAnimatedSprite(atlas);

        for (PivotableAnimatedSprite.Direction direction : orderedDirections) {
            final String animationName = animation.toString().toLowerCase();

            final AnimatedSprite animationSprite = animatedSpriteFactory.createByName(direction, animationName);
            pivotableAnimatedSprite.addAnimatedSprite(direction, animationSprite);
        }
        return pivotableAnimatedSprite;
    }

    private void loadSpriteSheets() {
        for (SpriteSheet spriteSheet : SpriteSheet.values()) {
            final String atlasPath = TEXTURES_SPRITESHEETS_DIR + DEFAULT_ATLAS;
            final String textureFilename = spriteSheet.toString().toLowerCase() + ".png";

            final KenneyAtlas kenneyAtlas = new KenneyAtlas(atlasPath, textureFilename);
            availableSpriteSheets.put(spriteSheet, kenneyAtlas);
        }
    }

    public enum SpriteSheet {
        BROWN_GUY,
        BROWN_GIRL,
//        GREEN_GIRL,
//        BLUE_GUY,
//        MONK,
//        RED_GIRL,
//        DARK_KNIGHT
    }

    public enum Animation {
        WALK,
        STAND
    }

}
