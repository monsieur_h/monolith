package com.monsieur_h.monolith.engine.graphics.animations;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.monsieur_h.monolith.engine.graphics.KenneyAtlas;

import java.util.HashMap;

public class PivotableAnimatedSprite {
    private HashMap<Direction, AnimatedSprite> directionalAnimations = new HashMap<Direction, AnimatedSprite>();
    private KenneyAtlas atlas;

    public PivotableAnimatedSprite(KenneyAtlas atlas) {
        this.atlas = atlas;
    }

    public void addAnimatedSprite(Direction direction, AnimatedSprite animatedSprite) {
        animatedSprite.setAtlas(atlas);
        directionalAnimations.put(direction, animatedSprite);
    }

    public TextureRegion getUpToDateTextureFromAngle(float angle) {
        final AnimatedSprite animatedSpriteFromAngle = getAnimatedSpriteFromAngle(angle);
        return animatedSpriteFromAngle.getCurrentFrame();
    }

    public void update(float delta) {
        for (AnimatedSprite animatedSprite : directionalAnimations.values()) {
            animatedSprite.update(delta);
        }
    }

    public AnimatedSprite getAnimatedSpriteFromAngle(float angle) {
        final int atlasSize = directionalAnimations.size();
        final int degrees = 360;
        final float angleRange = degrees / atlasSize; //Range in which a given sprite applies

        angle -= angleRange / 2f;
        angle = angle % degrees;
        if (angle <= 0) {
            angle += 359.999f;//.1f to avoid division by zero
        }
        final int index = (int) ((angle) / (degrees / atlasSize));
        Direction direction = Direction.values()[index];
        return directionalAnimations.get(direction);
    }


    public enum Direction {
        EAST,
        NORTH,
        WEST,
        SOUTH
    }
}
