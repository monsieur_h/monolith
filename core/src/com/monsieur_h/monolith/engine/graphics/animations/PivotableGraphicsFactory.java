package com.monsieur_h.monolith.engine.graphics.animations;


import com.monsieur_h.monolith.engine.graphics.KenneyAtlas;

import java.util.HashMap;

public class PivotableGraphicsFactory {
    private final KenneyAtlas atlas;
    private HashMap<Skin, PivotableGraphics> storage = new HashMap<Skin, PivotableGraphics>();

    public PivotableGraphicsFactory(String atlasPath) {
        atlas = new KenneyAtlas(atlasPath);
        initStorage();
    }

    private void initStorage() {
        final String[] directions = {"NW", "W", "SW", "S", "SE", "E", "NE", "N"};
        for (Skin skin : Skin.values()) {
            PivotableGraphics graphics = new PivotableGraphics(atlas);
            for (String direction : directions) {
                graphics.addRegion(skin.toString().toLowerCase() + "_" + direction + ".png");
            }
            storage.put(skin, graphics);
        }
    }

    public PivotableGraphics create(Skin skin) {
        return storage.get(skin);
    }

    public enum Skin {
        AMBULANCE,
        TAXI,
        POLICE,
        GARBAGE
    }
}
