package com.monsieur_h.monolith.engine.graphics.animations;


import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.monsieur_h.monolith.engine.graphics.KenneyAtlas;

import java.util.ArrayList;

public class PivotableGraphics {
    ArrayList<TextureRegion> graphics = new ArrayList<TextureRegion>();
    KenneyAtlas atlas;

    public PivotableGraphics(KenneyAtlas atlas) {
        this.atlas = atlas;
    }

    public void addRegion(String regionName) {
        graphics.add(atlas.getRegion(regionName));
    }

    public void addRegion(int index, String regionName) {
        graphics.add(index, atlas.getRegion(regionName));
    }

    public TextureRegion getTextureRegionFromAngle(float angle) {
        final int atlasSize = graphics.size();
        final int degrees = 360;
        final float angleRange = degrees / atlasSize; //Range in which a given sprite applies

        angle -= angleRange / 2f;
        angle = angle % degrees;
        if (angle <= 0) {
            angle += 359.999f;//.1f to avoid division by zero
        }
        final int index = (int) ((angle) / (degrees / atlasSize));
        return graphics.get(index);
    }
}
