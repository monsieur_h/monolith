package com.monsieur_h.monolith.engine.graphics.animations;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.XmlReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class AnimatedSpriteFactory {
    public static final String DEFAULT_SPRITESHEET = "textures/spritesheets/animation.xml";
    private XmlReader.Element rootElement;


    public AnimatedSpriteFactory() {
        loadXml();
    }

    private void loadXml() {
        XmlReader reader = new XmlReader();
        try {
            rootElement = reader.parse(Gdx.files.internal(DEFAULT_SPRITESHEET));
        } catch (IOException e) {
            throw new GdxRuntimeException(e);
        }
    }

    public HashMap<String, AnimatedSprite> createAll() {
        HashMap<String, AnimatedSprite> animations = new HashMap<String, AnimatedSprite>();
        for (XmlReader.Element animationNode : rootElement.getChildrenByName("Animation")) {
            final String animationName = animationNode.getAttribute("name");

            final ArrayList<String> regionNames = getRegionNames(animationNode);

            final AnimatedSprite animatedSprite = new AnimatedSprite(regionNames);
            animations.put(animationName, animatedSprite);
        }
        return animations;
    }


    public AnimatedSprite createByName(PivotableAnimatedSprite.Direction direction, String animationName) {
        final XmlReader.Element animationNode = findAnimationNode(animationName);
        final XmlReader.Element directionNode = findDirectionNode(animationNode, direction.toString());
        final ArrayList<String> regionNames = getRegionNames(directionNode);
        return new AnimatedSprite(regionNames);
    }

    private XmlReader.Element findDirectionNode(XmlReader.Element animationNode, String directionName) {
        for (XmlReader.Element directionNode : animationNode.getChildrenByName("Direction")) {
            if (directionName.equals(directionNode.get("name"))) {
                return directionNode;
            }
        }
        throw new GdxRuntimeException("Direction " + directionName + " not found for animation " + animationNode.get("name"));
    }

    private XmlReader.Element findAnimationNode(String animationName) {
        for (XmlReader.Element animationNode : rootElement.getChildrenByName("Animation")) {
            if (animationName.equals(animationNode.getAttribute("name"))) {
                return animationNode;
            }
        }
        throw new GdxRuntimeException("Animation not found : " + animationName);
    }

    private ArrayList<String> getRegionNames(XmlReader.Element animationNode) {
        ArrayList<String> regionNames = new ArrayList<String>();
        for (int j = 0; j < animationNode.getChildCount(); j++) {
            final String regionName = animationNode.getChild(j).get("regionName");
            regionNames.add(regionName);
        }
        return regionNames;
    }
}
