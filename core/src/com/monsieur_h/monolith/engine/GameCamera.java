package com.monsieur_h.monolith.engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;

public class GameCamera extends OrthographicCamera {
    private final float zoomFactor = 5;
    private final Group dummyActor = new Group();
    private final CameraShaker shaker = new CameraShaker();
    public float minZoom = .8f;
    public float maxZoom = 5f;
    private Actor trackedActor;
    private VillagerEntity trackedEntity;

    public GameCamera(float viewportWidth, float viewportHeight) {
        super(viewportWidth, viewportHeight);
    }

    public GameCamera() {
        super();
    }

    @Override
    public void update() {
        float deltaTime = Gdx.graphics.getDeltaTime();
        dummyActor.act(deltaTime);
        shaker.update(deltaTime);

        Actor actor;
        if (trackedActor != null) {
            actor = trackedActor;
            setZoomRatio(0f);
        } else {
            actor = dummyActor;
            setZoomRatio(dummyActor.getScaleX());
        }
        position.x = actor.getX();
        position.y = actor.getY();
        shaker.apply(position);
        super.update();
    }

    public void shake(float duration, float strength) {
        shaker.shake(duration, strength);
    }

    public void zoom(float amount) {
        zoom += amount / zoomFactor;
        zoom = MathUtils.clamp(zoom, minZoom, maxZoom);
        dummyActor.setScale(getCurrentZoomRatio());
    }

    public VillagerEntity getTrackedEntity() {
        return trackedEntity;
    }

    public void follow(Actor actor) {
        trackedActor = actor;
        trackedEntity = null;
    }

    public void follow(VillagerEntity entity) {
        trackedActor = entity.getActor();
        trackedEntity = entity;
    }

    public boolean isFollowing() {
        return trackedActor != null;
    }

    public void unFollow() {
        dummyActor.remove();
        dummyActor.setX(trackedActor.getX());
        dummyActor.setY(trackedActor.getY());
        trackedActor = null;
        trackedEntity = null;
    }

    @Override
    public void rotate(float angle) {
        super.rotate(angle);
        dummyActor.setRotation(angle);
    }

    @Override
    public void translate(float x, float y) {
        super.translate(x, y);
        dummyActor.moveBy(x, y);
    }

    @Override
    public void translate(Vector2 vec) {
        super.translate(vec);
        dummyActor.moveBy(vec.x, vec.y);
    }

    public void setX(float x) {
        position.x = x;
    }

    public void setY(float y) {
        position.y = y;
    }

    public void setZoom(float zoom) {
        this.zoom = MathUtils.clamp(zoom, minZoom, maxZoom);
    }

    private void setZoomRatio(float ratio) {
        zoom = ((maxZoom - minZoom) * ratio) + minZoom;
    }

    public float getCurrentZoomRatio() {
        return (zoom - minZoom) / (maxZoom - minZoom);
    }

    public void addAction(Action action) {
        dummyActor.addAction(action);
    }

    public void setPosition(Vector3 newPosition) {
        setPosition(newPosition.x, newPosition.y);
    }

    public void setPosition(Vector2 newPosition) {
        setPosition(newPosition.x, newPosition.y);
    }

    public void setPosition(float x, float y) {
        dummyActor.setPosition(x, y);
    }

    public Actor getTrackedActor() {
        return trackedActor;
    }

    public Vector2 unproject(Vector2 position) {
        Vector3 vector3 = new Vector3(position.x, position.y, 0f);
        Vector3 unproject = super.unproject(vector3);
        return new Vector2(unproject.x, unproject.y);
    }

    public Vector2 project(Vector2 position) {
        Vector3 vector3 = new Vector3(position.x, position.y, 0f);
        Vector3 project = super.project(vector3);
        return new Vector2(project.x, project.y);
    }
}
