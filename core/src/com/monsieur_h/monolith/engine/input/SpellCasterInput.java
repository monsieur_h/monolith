package com.monsieur_h.monolith.engine.input;

import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.gameplay.MainScene;
import com.monsieur_h.monolith.gameplay.spell.Spell;

import java.util.ArrayList;
import java.util.List;

/**
 * Created for project monolith by ahub on 10/15/16.
 * Overrides inputs and casts a spell on tap if no other input handles the situation
 */
public class SpellCasterInput implements InputHandler{

    private GameWorldInputMultiplexer gameWorldInputMultiplexer;
    public List<Spell> spellList = new ArrayList<Spell>();

    public SpellCasterInput(GameWorldInputMultiplexer gameWorldInputMultiplexer) {
        this.gameWorldInputMultiplexer = gameWorldInputMultiplexer;
        gameWorldInputMultiplexer.register(this);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        gameWorldInputMultiplexer.unregister(this);
        for (Spell spell : spellList) {
            spell.cast(new Vector2(x, y), gameWorldInputMultiplexer.getScene());
        }
        return true;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }
}
