package com.monsieur_h.monolith.engine.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.monsieur_h.monolith.gameplay.MainScene;


public class GUIInputHandler implements InputHandler {
    private final GameWorldInputMultiplexer gameWorldInputMultiplexer;
    private final MainScene scene;

    public GUIInputHandler(GameWorldInputMultiplexer gameWorldInputMultiplexer, MainScene scene) {
        this.gameWorldInputMultiplexer = gameWorldInputMultiplexer;
        gameWorldInputMultiplexer.register(this);
        this.scene = scene;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        Stage guiStage = scene.getGUI().getStage();
        if (guiStage.touchDown((int) x, (int) y, button, button)) {
            guiStage.touchUp((int) x, (int) y, button, button);
            return true;
        }

        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }
}
