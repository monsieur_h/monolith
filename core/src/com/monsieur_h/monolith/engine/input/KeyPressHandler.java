package com.monsieur_h.monolith.engine.input;


import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.gameplay.MainScene;

public class KeyPressHandler implements InputHandler {
    private MainScene mainScene;

    public KeyPressHandler(GameWorldInputMultiplexer multiplexer, MainScene mainScene) {
        multiplexer.register(this);
        this.mainScene = mainScene;
    }

    @Override
    public boolean keyUp(int keycode) {
        switch (keycode) {
            case Input.Keys.B:
                mainScene.debug = !mainScene.debug;
                return true;

            case Input.Keys.S:
                mainScene.setPostProcessActive(!mainScene.isPostProcessActive);
                return true;

            case Input.Keys.T:
                mainScene.isShaderActive = !mainScene.isShaderActive;
                return true;
        }
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }
}
