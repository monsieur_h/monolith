package com.monsieur_h.monolith.engine.input;

import com.badlogic.gdx.input.GestureDetector;

public class InputAndGestureDetector extends GestureDetector {
    public static final float LONG_PRESS_DURATION = 0.15f;
    public static final float TAP_COUNT_INTERVAL = 0.4f;
    public static final int HALF_TAP_SQUARE_SIZE = 20;
    public static final float MAX_FLING_DELAY = 0.15f;
    private GameWorldInputMultiplexer listener;

    public InputAndGestureDetector(GameWorldInputMultiplexer listener) {
        this(HALF_TAP_SQUARE_SIZE, TAP_COUNT_INTERVAL, LONG_PRESS_DURATION, MAX_FLING_DELAY, listener);
        this.listener = listener;
    }

    public InputAndGestureDetector(float halfTapSquareSize, float tapCountInterval, float longPressDuration, float maxFlingDelay, GameWorldInputMultiplexer listener) {
        super(halfTapSquareSize, tapCountInterval, longPressDuration, maxFlingDelay, listener);
        this.listener = listener;
    }

    @Override
    public boolean scrolled(int amount) {
        return listener.scrolled(amount);
    }

    @Override
    public boolean touchUp(int x, int y, int pointer, int button) {
        boolean eventHandled = super.touchUp(x, y, pointer, button);
        if (!eventHandled) {
            eventHandled = listener.touchUp(x, y, pointer, button);
        }
        return eventHandled;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return listener.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean touchDragged(int x, int y, int pointer) {
        boolean eventHandled = super.touchDragged(x, y, pointer);
        if (!eventHandled) {
            eventHandled = listener.touchDragged(x, y, pointer);
        }
        return eventHandled;
    }

    @Override
    public boolean keyUp(int keycode) {
        return listener.keyUp(keycode);
    }
}
