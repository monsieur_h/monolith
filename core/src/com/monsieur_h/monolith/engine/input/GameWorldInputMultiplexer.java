package com.monsieur_h.monolith.engine.input;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.engine.graphics.PointerLightHandler;
import com.monsieur_h.monolith.gameplay.MainScene;

import java.util.ArrayList;


public class GameWorldInputMultiplexer implements GestureDetector.GestureListener, InputProcessor {
    private final MainScene scene;
    private final ArrayList<InputHandler> handlers = new ArrayList<InputHandler>();

    public GameWorldInputMultiplexer(MainScene scene) {
        this.scene = scene;
        new CameraMover(this);
        new EntityGrabber(this);
        new KeyPressHandler(this, scene);
        new GUIInputHandler(this, scene);
        new PointerLightHandler(this, scene);
    }

    public void register(InputHandler handler) {
        handlers.add(handler);
    }

    public void unregister(InputHandler handler) {
        handlers.remove(handler);
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).touchDown((int) x, (int) y, pointer, button)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).tap(x, y, count, button)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).longPress(x, y)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).pan(x, y, deltaX, deltaY)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).panStop(x, y, pointer, button)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).zoom(initialDistance, distance)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).pinch(initialPointer1, initialPointer2, pointer1, pointer2)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).keyUp(keycode)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).touchDown(screenX, screenY, pointer, button)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).touchUp(screenX, screenY, pointer, button)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).touchDragged(screenX, screenY, pointer)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).mouseMoved(screenX, screenY)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        for (int i = handlers.size() - 1; i >= 0; i--) {
            if (handlers.get(i).scrolled(amount)) {
                return true;
            }
        }
        return false;
    }

    public MainScene getScene() {
        return scene;
    }
}
