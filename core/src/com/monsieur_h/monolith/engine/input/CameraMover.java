package com.monsieur_h.monolith.engine.input;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.monsieur_h.monolith.engine.GameCamera;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;
import com.monsieur_h.monolith.gameplay.GrabbableEntity;
import com.monsieur_h.monolith.gameplay.MainScene;


public class CameraMover implements InputHandler {
    private static final float ZOOM_UNIT_SNAP_DISTANCE_THRESHOLD = 100f;
    private final GameCamera camera;
    private final MainScene scene;
    private float previousZoomDistance = -1;
    private Vector3 panStartCameraPosition;

    public CameraMover(GameWorldInputMultiplexer multiplexer) {
        multiplexer.register(this);
        scene = multiplexer.getScene();


        camera = (GameCamera) multiplexer.getScene().getCamera();
    }

    private void moveCamera(float x, float y) {
        camera.setPosition(panStartCameraPosition.add(-x * camera.zoom, y * camera.zoom, 0).cpy());
    }


    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        if (panStartCameraPosition == null) {
            panStartCameraPosition = camera.position.cpy();
        }
        moveCamera(deltaX, deltaY);
        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        panStartCameraPosition = null;
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        if (count == 2) {
            final float currentZoomRatio = camera.getCurrentZoomRatio();
            float nextZoomRatio = (currentZoomRatio == 0) ? 1 : 0;
            final Vector2 zoomPosition = scene.screenToStageCoordinates(new Vector2(x, y));
            final float duration = .5f;
            camera.addAction(
                    Actions.parallel(
                            Actions.moveTo(zoomPosition.x, zoomPosition.y, duration, Interpolation.sine),
                            Actions.scaleTo(nextZoomRatio, nextZoomRatio, duration, Interpolation.sine)
                    )
            );
            return true;
        }
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        if (previousZoomDistance == -1) {
            previousZoomDistance = pointer1.dst(pointer2);
        }
        final float distance = pointer1.dst(pointer2);
        float factor = 1 - (distance / previousZoomDistance);

        previousZoomDistance = distance;
        updateCameraZoom(factor * 25f);
        return true;
    }

    private void updateCameraZoom(float amount) {
        if (camera.getCurrentZoomRatio() == 0 && amount < 0) {
            final GrabbableEntity entity = scene.getEntityFromScreenSpace(camera.position.x, camera.position.y, ZOOM_UNIT_SNAP_DISTANCE_THRESHOLD);
            if (entity != null) {
                followEntity(entity);
            }
        } else {
            if (camera.isFollowing()) {
                stopFollowingEntity();
            }
            camera.zoom(amount);
        }
    }

    private void stopFollowingEntity() {
        final VillagerEntity trackedEntity = camera.getTrackedEntity();
        if (!(trackedEntity == null)) {
            scene.getGUI().getVillagerUIManager().releaseVillagerWidgetDependency(trackedEntity.getVillagerAI());
        }
        camera.unFollow();
    }

    private void followEntity(GrabbableEntity entity) {
        if (camera.isFollowing()) {
            if (entity == camera.getTrackedActor()) {
                return;
            } else {
                stopFollowingEntity();
            }
        }
        VillagerEntity villager = (VillagerEntity) entity;
        camera.follow(villager);
        scene.getGUI().getVillagerUIManager().askForVillagerWidget(villager.getVillagerAI());
    }

    @Override
    public boolean scrolled(int amount) {
        final float tweakingFactor = 1f;
        updateCameraZoom(amount * tweakingFactor);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        previousZoomDistance = -1;
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }
}
