package com.monsieur_h.monolith.engine.input;

import com.badlogic.gdx.math.Vector2;

@SuppressWarnings({"SameReturnValue", "UnusedParameters"})
public
        // Some methods always want the event to be dispatched, so they return false all the time. This is normal behavior so the Warning is disabled
interface InputHandler {
    boolean mouseMoved(int screenX, int screenY);

    boolean longPress(float x, float y);

    boolean tap(float x, float y, int count, int button);

    boolean fling(float velocityX, float velocityY, int button);

    boolean pan(float x, float y, float deltaX, float deltaY);

    boolean panStop(float x, float y, int pointer, int button);

    boolean zoom(float initialDistance, float distance);

    boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2);

    boolean scrolled(int amount);

    boolean touchUp(int screenX, int screenY, int pointer, int button);

    boolean touchDragged(int screenX, int screenY, int pointer);

    boolean keyUp(int keycode);

    boolean touchDown(int screenX, int screenY, int pointer, int button);
}
