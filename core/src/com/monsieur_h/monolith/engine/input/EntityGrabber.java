package com.monsieur_h.monolith.engine.input;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;
import com.monsieur_h.monolith.gameplay.GrabbableEntity;
import com.monsieur_h.monolith.gameplay.MainScene;


public class EntityGrabber implements InputHandler {
    private final MouseJointDef mouseJointDefinition = new MouseJointDef();
    private final World world;
    private final MainScene scene;
    private Body dummy;
    private GrabbableEntity grabbedEntity;
    private MouseJoint mouseJoint;

    public EntityGrabber(GameWorldInputMultiplexer multiplexer) {
        multiplexer.register(this);
        scene = multiplexer.getScene();
        world = scene.getWorld();

        createDummy();

        mouseJointDefinition.bodyA = dummy;
        mouseJointDefinition.maxForce = 5000;
    }

    private void grab(int x, int y, GrabbableEntity entityAtCoordinates) {
        grabbedEntity = entityAtCoordinates;
        grabbedEntity.onGrab();
        createMouseJointOnGrabbedEntityCenter();
        mouseMoved(x, y);
    }

    private void unGrab() {
        grabbedEntity.onGrabReleased();
        destroyMouseJointOnGrabbedEntity();
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (mouseJoint != null) {
            updateMouseJointPosition(screenX, screenY);
        }
        return false;
    }

    private void updateMouseJointPosition(int screenX, int screenY) {
        final Vector3 unproject = scene.getCamera().unproject(new Vector3(screenX, screenY, 0));
        final Vector2 vector2 = new Vector2(unproject.x, unproject.y);
        vector2.scl(1 / MonolithGame.METERS_TO_PIXEL_RATIO);
        mouseJoint.setTarget(vector2);
    }

    @Override
    public boolean longPress(float x, float y) {
        if (grabbedEntity != null) {
            unGrab();
            return true;
        }

        final GrabbableEntity entityAtCoordinates = scene.getEntityFromScreenSpace(x, y);
        if (entityAtCoordinates != null) {
            grab((int) x, (int) y, entityAtCoordinates);
            return true;
        }

        return false;
    }

    private void createDummy() {
        dummy = world.createBody(new BodyDef());
    }

    private void createMouseJointOnGrabbedEntityCenter() {
        final Body entityBody = grabbedEntity.getBox2dComponent().getBody();
        if (entityBody == null) {
            return;
        }
        mouseJointDefinition.bodyB = entityBody;
        mouseJointDefinition.target.set(entityBody.getWorldCenter());
        mouseJoint = (MouseJoint) entityBody.getWorld().createJoint(mouseJointDefinition);
    }

    private void destroyMouseJointOnGrabbedEntity() {
        final Box2dSteeringEntity box2dComponent = grabbedEntity.getBox2dComponent();
        if (box2dComponent == null) {
            return;
        }
        box2dComponent.getBody().getWorld().destroyJoint(mouseJoint);
        grabbedEntity = null;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        if (grabbedEntity != null) {
            updateMouseJointPosition((int) x, (int) y);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        if (grabbedEntity != null) {
            unGrab();
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (grabbedEntity != null) {
            unGrab();
            return true;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (grabbedEntity != null) {
            mouseMoved(screenX, screenY);
            return true;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }
}
