package com.monsieur_h.monolith.engine.entities;


public interface VillagerCreationListener {
    void onVillagerCreated(VillagerEntity createdVillager);
}
