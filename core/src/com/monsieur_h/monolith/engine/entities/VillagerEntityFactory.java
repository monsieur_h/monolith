package com.monsieur_h.monolith.engine.entities;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.generation.WorldContainer;
import com.monsieur_h.monolith.engine.graphics.animations.PivotableAnimatedSprite;
import com.monsieur_h.monolith.engine.graphics.animations.PivotableAnimatedSpriteFactory;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;
import com.monsieur_h.monolith.engine.physics.CollisionFilter;
import com.monsieur_h.monolith.engine.physics.SurvivableAreaVerifier;

import java.util.ArrayList;


public class VillagerEntityFactory {
    public static final float DEFAULT_MAX_LINEAR_SPEED = 3f;
    public static final float DEFAULT_DAMPING = 2.5f;
    private final PivotableAnimatedSpriteFactory pivotableAnimatedSpriteFactory = new PivotableAnimatedSpriteFactory();
    private Filter defaultCollisionFilter;
    private Filter grabbedCollisionFilter;
    private WorldContainer worldContainer;
    private ArrayList<VillagerCreationListener> creationListener = new ArrayList<VillagerCreationListener>();
    private SurvivableAreaVerifier areaVerifier;

    public VillagerEntityFactory(WorldContainer worldContainer) {
        this.worldContainer = worldContainer;
        areaVerifier = new SurvivableAreaVerifier(worldContainer);
        initCollisionFilters();
    }

    private void initCollisionFilters() {
        defaultCollisionFilter = new Filter();
        defaultCollisionFilter.categoryBits = CollisionFilter.asBits(CollisionFilter.Tag.ENTITY);
        defaultCollisionFilter.maskBits = CollisionFilter.asBits(CollisionFilter.Tag.ALL);

        grabbedCollisionFilter = new Filter();
        grabbedCollisionFilter.categoryBits = CollisionFilter.asBits(CollisionFilter.Tag.NONE);
        grabbedCollisionFilter.maskBits = CollisionFilter.asBits(CollisionFilter.Tag.NONE);
    }

    public VillagerEntity createVillagerEntity(World world, PivotableAnimatedSpriteFactory.SpriteSheet spriteSheet, int posX, int posY) {
        final PivotableAnimatedSprite pivotableGraphics = pivotableAnimatedSpriteFactory.create(spriteSheet, PivotableAnimatedSpriteFactory.Animation.WALK);

        Box2dSteeringEntity box2dSteeringEntity = createBox2dSteeringEntity(world, posX, posY, pivotableGraphics.getUpToDateTextureFromAngle(0));

        VillagerEntity entity = new VillagerEntity(box2dSteeringEntity, new VillagerAI(worldContainer));

        entity.setPivotableGraphics(pivotableGraphics);

        final Image image = entity.getImage();
        image.setPosition(image.getX(), -image.getHeight() / 16);

        entity.setDefaultFilter(defaultCollisionFilter);
        entity.setGrabbedFilter(grabbedCollisionFilter);

        onVillagerCreated(entity);
        entity.register(areaVerifier);

        return entity;
    }

    public void register(VillagerCreationListener listener) {
        creationListener.add(listener);
    }

    private void onVillagerCreated(VillagerEntity createdVillager) {
        for (int i = creationListener.size() - 1; i >= 0; i--) {
            creationListener.get(i).onVillagerCreated(createdVillager);
        }
    }


    private Box2dSteeringEntity createBox2dSteeringEntity(World world, int posX, int posY, TextureRegion region) {
        int radiusInPixels = (int) ((region.getRegionWidth() / 2f));

        Body characterBody = createCircularBody(world, posX, posY, radiusInPixels);
        final Box2dSteeringEntity box2dSteeringEntity = new Box2dSteeringEntity(characterBody, false, MonolithGame.pixelsToMeters(radiusInPixels));
        box2dSteeringEntity.setMaxLinearSpeed(DEFAULT_MAX_LINEAR_SPEED);
        box2dSteeringEntity.setIndependentFacing(true);
        return box2dSteeringEntity;
    }

    private Body createCircularBody(World world, int posX, int posY, int radiusInPixels) {
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(MonolithGame.pixelsToMeters(radiusInPixels));

        Body characterBody = createBodyAtPosition(world, posX, posY);

        FixtureDef charFixtureDef = new FixtureDef();
        charFixtureDef.density = 1;
        charFixtureDef.shape = circleShape;
        charFixtureDef.filter.categoryBits = CollisionFilter.asBits(CollisionFilter.Tag.ENTITY);
        charFixtureDef.friction = 1;
//        charFixtureDef.
        characterBody.createFixture(charFixtureDef);

        circleShape.dispose();
        return characterBody;
    }

    private Body createBodyAtPosition(World world, int posX, int posY) {
        BodyDef characterBodyDef = new BodyDef();
        characterBodyDef.linearDamping = DEFAULT_DAMPING;
        characterBodyDef.angularDamping = DEFAULT_DAMPING;
        characterBodyDef.position.set(MonolithGame.pixelsToMeters(posX), MonolithGame.pixelsToMeters(posY));
        characterBodyDef.type = BodyDef.BodyType.DynamicBody;
        return world.createBody(characterBodyDef);
    }

    public void markAsSensor(Box2dSteeringEntity character) {
        Array<Fixture> fixtures = character.getBody().getFixtureList();
        for (int i = 0; i < fixtures.size; i++) {
            fixtures.get(i).setSensor(true);
        }
    }
}
