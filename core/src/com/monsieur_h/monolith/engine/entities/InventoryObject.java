package com.monsieur_h.monolith.engine.entities;

public interface InventoryObject {
    ResourceEntity.ResourceType getType();

    int getAmount();

    void setAmount(int amount);
}
