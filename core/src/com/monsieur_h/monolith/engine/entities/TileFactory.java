package com.monsieur_h.monolith.engine.entities;


import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.engine.generation.TileDef;
import com.monsieur_h.monolith.engine.generation.TileGraphicsFactory;
import com.monsieur_h.monolith.engine.physics.CollisionFilter;

public class TileFactory {
    public static final float TILE_HEIGHT_RATIO = 1.5f;
    public static float tileW;
    private static float tileH;
    private TileGraphicsFactory tileGraphicsFactory = new TileGraphicsFactory();

    public TileFactory() {
        initTileSize();
    }

    public static Vector2 tilePosToPixels(int x, int y) {
        if (tileW == 0 || tileH == 0) {
            throw new GdxRuntimeException("ERROR: TileFactory was not initialized");
        }
        Vector2 screenPos = new Vector2();
        screenPos.x = (y - x) * tileW / 2f;
        screenPos.y = (y + x) * tileH / 2f / TILE_HEIGHT_RATIO;
        return screenPos;
    }

    /**
     * Returns the center of the tile instead of its origin
     */
    public static Vector2 tilePosToPixelsCenter(int x, int y) {
        Vector2 pos = tilePosToPixels(x, y);
        pos.add(tileW / 2, tileH / TILE_HEIGHT_RATIO);
        return pos;
    }

    public static float[] getTileVerticesInPixelUnits() {
        final float tileHIsometric = tileH / TILE_HEIGHT_RATIO;
        return new float[]{
                tileW / 2, 0, //mid bottom
                tileW, (tileHIsometric / 2), //mid right
                tileW / 2, (tileHIsometric), //mid top
                0, (tileHIsometric / 2) //mid left
        };
    }

    public static float[] getTileVerticesInPhysicUnits() {
        float[] tileVerticesInPix = getTileVerticesInPixelUnits();
        for (int i = 0; i < tileVerticesInPix.length; i++) {
            tileVerticesInPix[i] = MonolithGame.pixelsToMeters((int) tileVerticesInPix[i]);
        }
        return tileVerticesInPix;
    }

    private void initTileSize() {
        Image img = new Image(tileGraphicsFactory.fromType(TileDef.TileType.GRASS));//Grass tile for size reference
        tileW = img.getWidth();
        tileH = img.getHeight();
    }

    public Tile createTile(TileDef tileDef, World world, int x, int y) {
        final TextureRegion region = tileGraphicsFactory.fromTile(tileDef);
        Vector2 screenPos = tilePosToPixels(x, y);
        Tile tile = new Tile(tileDef);
        tile.setupImage(region);
        tile.getImage().setPosition(screenPos.x, screenPos.y);

        if (!tileDef.isWalkable()) {
            final Body tileCollider = createTileCollider(world, x, y);
            tile.setupBody(tileCollider);
        }

        if (tileDef.type == TileDef.TileType.HOUSE) {
            return new HouseTile(tile);
        }

        return tile;
    }

    public Body createTileCollider(World world, int x, int y) {

        PolygonShape colliderShape = new PolygonShape();
        final float tileHIsometric = tileH / TILE_HEIGHT_RATIO;
        float[] vertices = getTileVerticesInPhysicUnits();
        colliderShape.set(vertices);

        BodyDef defaultBodyDef = new BodyDef();
        defaultBodyDef.type = BodyDef.BodyType.StaticBody;

        FixtureDef defaultFixtureDef = new FixtureDef();
        defaultFixtureDef.shape = colliderShape;
        defaultFixtureDef.filter.categoryBits = CollisionFilter.asBits(CollisionFilter.Tag.NON_WALKABLE_TILE);

        Vector2 tmpPosition = tilePosToPixels(x, y);

        //Compensate for tile height (since the collider is not at the bottom of the image)
        tmpPosition.y += tileHIsometric / 2f;

        defaultBodyDef.position.set(MonolithGame.pixelsToMeters((int) tmpPosition.x), MonolithGame.pixelsToMeters((int) tmpPosition.y));
        Body tileBody = world.createBody(defaultBodyDef);
        tileBody.createFixture(defaultFixtureDef);
        return tileBody;
    }
}
