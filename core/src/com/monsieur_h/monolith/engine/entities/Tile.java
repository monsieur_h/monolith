package com.monsieur_h.monolith.engine.entities;


import com.monsieur_h.monolith.engine.generation.TileDef;

public class Tile extends GameWorldEntity {
    private TileDef def;

    public Tile(TileDef def) {
        super(EntityType.TILE);
        this.def = def;
    }

    public TileDef getDef() {
        return def;
    }
}
