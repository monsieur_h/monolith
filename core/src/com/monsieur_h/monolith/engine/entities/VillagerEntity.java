package com.monsieur_h.monolith.engine.entities;


import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.ai.states.VillagerStateIcons;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;
import com.monsieur_h.monolith.gameplay.GrabbableEntity;

public class VillagerEntity extends PivotableSteeringGameWorldEntity implements GrabbableEntity {
    private VillagerAI villagerAI;
    private Filter defaultFilter;
    private Filter grabbedFilter;


    public VillagerEntity(Box2dSteeringEntity box2dComponent, VillagerAI villagerAI) {
        super(box2dComponent);
        this.villagerAI = villagerAI;
        villagerAI.setParentEntity(this);
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        getVillagerAI().setName(name);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        villagerAI.update(delta);
    }

    public VillagerStateIcons getCurrentStateType() {
        return villagerAI.getCurrentVillagerStateIcon();
    }

    public VillagerAI getVillagerAI() {
        return villagerAI;
    }

    @Override
    public void onGrab() {
        super.onGrab();
        villagerAI.onGrab();
        setCollisionFilter(grabbedFilter);
    }

    @Override
    public void kill() {
        super.kill();
        villagerAI.kill();
        float death_anim_duration = .5f;
        getImage().addAction(
                Actions.parallel(
                        Actions.moveBy(0, -getImage().getHeight(), death_anim_duration),
                        Actions.fadeOut(death_anim_duration / 2f)
                )
        );
    }

    @Override
    public void onGrabReleased() {
        super.onGrabReleased();
        villagerAI.onGrabReleased();
        setCollisionFilter(defaultFilter);
    }

    public void setGrabbedFilter(Filter grabbedFilter) {
        this.grabbedFilter = grabbedFilter;
    }

    public void setDefaultFilter(Filter defaultFilter) {
        this.defaultFilter = defaultFilter;
    }
}
