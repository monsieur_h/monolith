package com.monsieur_h.monolith.engine.entities;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.monsieur_h.monolith.engine.ai.HouseInventory;
import com.monsieur_h.monolith.engine.ai.Inventory;
import com.monsieur_h.monolith.engine.generation.TileDef;
import com.monsieur_h.monolith.engine.physics.CollisionFilter;

public class HouseTile extends Tile {
    private HouseInventory inventory = new HouseInventory();

    public HouseTile(TileDef def) {
        super(def);
    }

    public HouseTile(Tile tile) {
        super(tile.getDef());
        this.setupBody(tile.getBody());
        this.image = tile.image;
    }

    @Override
    public void setupBody(Body body) {
        super.setupBody(body);
        for (Fixture fixture : body.getFixtureList()) {
            Filter filter = new Filter();
            filter.categoryBits = CollisionFilter.asBits(CollisionFilter.Tag.ENTITY);
            fixture.setFilterData(filter);
        }
    }

    public HouseInventory getInventory() {
        return inventory;
    }
}
