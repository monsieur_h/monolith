package com.monsieur_h.monolith.engine.entities;


import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.monsieur_h.monolith.engine.graphics.animations.PivotableAnimatedSprite;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;

public class PivotableSteeringGameWorldEntity extends SteeringGameWorldEntity {
    private PivotableAnimatedSprite pivotableGraphics;

    public PivotableSteeringGameWorldEntity(Box2dSteeringEntity box2dComponent) {
        super(box2dComponent);
    }

    public void setPivotableGraphics(PivotableAnimatedSprite pivotableGraphics) {
        this.pivotableGraphics = pivotableGraphics;
        setupImage(pivotableGraphics.getUpToDateTextureFromAngle(0));
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        pivotableGraphics.update(delta);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    @Override
    protected void updateRotation() {
        final float rotation = box2dComponent.getBody().getAngle() * MathUtils.radiansToDegrees;
        TextureRegion textureRegionFromAngle = pivotableGraphics.getUpToDateTextureFromAngle(rotation);
        image.setDrawable(new TextureRegionDrawable(textureRegionFromAngle));
        image.setScaling(Scaling.none);
    }

    @Override
    protected void setupBox2dComponent(Box2dSteeringEntity box2dComponent) {
        super.setupBox2dComponent(box2dComponent);
        box2dComponent.setIndependentFacing(false);
    }
}
