package com.monsieur_h.monolith.engine.entities;

public class ResourceChunk implements InventoryObject {
    public final ResourceEntity.ResourceType resourceType;
    public int amount;

    public ResourceChunk(ResourceEntity.ResourceType resourceType, int amount) {
        this.resourceType = resourceType;
        this.amount = amount;
    }

    @Override
    public ResourceEntity.ResourceType getType() {
        return resourceType;
    }

    @Override
    public int getAmount() {
        return amount;
    }

    @Override
    public void setAmount(int amount) {
        this.amount = amount;
    }
}
