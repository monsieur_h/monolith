package com.monsieur_h.monolith.engine.entities;


import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.engine.generation.ResourceGraphicsFactory;
import com.monsieur_h.monolith.engine.generation.TileDef;
import com.monsieur_h.monolith.engine.physics.CollisionFilter;

public class ResourceFactory {
    private ResourceGraphicsFactory resourceGraphicsFactory = new ResourceGraphicsFactory();

    public ResourceEntity createEntity(TileDef tileDef, World world, int x, int y) {//todo fix coordinates
        final TextureRegion region = resourceGraphicsFactory.fromTile(tileDef);
        final ResourceEntity resourceEntity = new ResourceEntity(ResourceEntity.ResourceType.FOOD);
        resourceEntity.setupImage(region);
        Vector2 pixelPos = TileFactory.tilePosToPixelsCenter(x, y);
        pixelPos.add(new Vector2().setToRandomDirection().scl(32));
        final Image image = resourceEntity.getImage();
        image.setPosition(pixelPos.x - image.getWidth() / 2f, pixelPos.y - image.getWidth() / 2f);
        final Body resourcePhysicalBody = createResourcePhysicalBody(resourceEntity, world, pixelPos);
        resourceEntity.setupBody(resourcePhysicalBody);
        return resourceEntity;
    }

    private Body createResourcePhysicalBody(ResourceEntity entity, World world, Vector2 pixelPos) {
        CircleShape circleShape = new CircleShape();
        BodyDef bodyDef = new BodyDef();
        FixtureDef fixtureDef = new FixtureDef();
        final Image image = entity.getImage();
        final float width = MonolithGame.pixelsToMeters((int) (image.getWidth() / 2f));
        circleShape.setRadius(width);
        fixtureDef.shape = circleShape;
        fixtureDef.filter.categoryBits = CollisionFilter.asBits(CollisionFilter.Tag.ENTITY);

        bodyDef.position.set(
                MonolithGame.pixelsToMeters((int) (pixelPos.x)),
                MonolithGame.pixelsToMeters((int) (pixelPos.y))
        );
        bodyDef.type = BodyDef.BodyType.StaticBody;
        final Body body = world.createBody(bodyDef);
        body.createFixture(fixtureDef);
        return body;
    }
}
