package com.monsieur_h.monolith.engine.entities;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;

public class SteeringGameWorldEntity extends GameWorldEntity {
    protected Box2dSteeringEntity box2dComponent;

    public SteeringGameWorldEntity(Box2dSteeringEntity box2dComponent) {
        super(EntityType.VILLAGER);
        setupBox2dComponent(box2dComponent);
    }

    protected void setupBox2dComponent(Box2dSteeringEntity box2dComponent) {
        this.box2dComponent = box2dComponent;
        setupBody(box2dComponent.getBody());
    }

    public Box2dSteeringEntity getBox2dComponent() {
        return box2dComponent;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        box2dComponent.update(delta);
        updateGraphicalPosition();
        updateRotation();
    }

    private void updateGraphicalPosition() {
        final Vector2 bodyPosition = box2dComponent.getBody().getPosition();
        setPosition(
                getOriginX() + MonolithGame.metersToPixels(bodyPosition.x),
                getOriginY() + MonolithGame.metersToPixels(bodyPosition.y)
        );
    }

    protected void updateRotation() {
        float angleInDegrees = box2dComponent.getBody().getAngle() * MathUtils.radiansToDegrees;
        setRotation(angleInDegrees);
    }
}
