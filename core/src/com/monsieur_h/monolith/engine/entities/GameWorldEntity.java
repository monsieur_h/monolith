package com.monsieur_h.monolith.engine.entities;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;
import com.monsieur_h.monolith.engine.physics.GrabListener;
import com.monsieur_h.monolith.engine.physics.SurvivableAreaVerifier;
import com.monsieur_h.monolith.gameplay.GrabbableEntity;

import java.util.ArrayList;

public class GameWorldEntity extends Group implements GrabbableEntity {
    protected final EntityType entityType;
    protected Image image;
    protected Body body;
    private boolean isAlive = true;
    private ArrayList<GrabListener> grabListeners = new ArrayList<GrabListener>();

    public GameWorldEntity(EntityType entityType) {
        this.entityType = entityType;
    }

    public void setupImage(TextureRegion region) {
        image = new Image(region);
        image.setPosition(-region.getRegionWidth() / 2f, -region.getRegionHeight() / 2f);
        image.setTouchable(Touchable.disabled);
        image.setVisible(true);
        addActor(image);
        resizeEntity();
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setupBody(Body body) {
        this.body = body;
    }

    public Body getBody() {
        return body;
    }

    private void resizeEntity() {
        setWidth(image.getWidth());
        setHeight(image.getHeight());
    }

    public Image getImage() {
        return image;
    }

    @Override
    public Actor hit(float x, float y, boolean touchable) {
        x = Math.abs(x);
        y = Math.abs(y);
        return (x <= getWidth() && y <= getHeight()) ? this : null;
    }

    @Override
    public void onGrab() {
        for (GrabListener listener : grabListeners) {
            listener.onGrab(this);
        }
    }

    @Override
    public Actor getActor() {
        return this;
    }

    @Override
    public void onGrabReleased() {
        for (GrabListener listener : grabListeners) {
            listener.onGrabReleased(this);
        }
    }

    @Override
    public Box2dSteeringEntity getBox2dComponent() {
        return null;
    }

    public void kill() {
        isAlive = false;
    }

    public boolean isAlive() {
        return isAlive;
    }

    protected void setCollisionFilter(Filter filter) {
        if (body == null) {
            return;
        }
        for (Fixture fixture : body.getFixtureList()) {
            fixture.setFilterData(filter);
        }
    }

    public void register(SurvivableAreaVerifier areaVerifier) {
        grabListeners.add(areaVerifier);
    }

    public enum EntityType {
        RESOURCE,
        TILE,
        CLOUD,
        VILLAGER
    }
}
