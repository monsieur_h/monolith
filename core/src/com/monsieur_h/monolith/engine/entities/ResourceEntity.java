package com.monsieur_h.monolith.engine.entities;

import com.badlogic.gdx.physics.box2d.Body;

public class ResourceEntity extends GameWorldEntity {
    private final int defaultGatherAmount = 1;
    private int amount = 1;
    private ResourceType resourceType;
    private boolean removable = false;

    public ResourceEntity(ResourceType resourceType) {
        super(EntityType.RESOURCE);
        this.resourceType = resourceType;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public ResourceChunk gatherResource() {
        amount -= defaultGatherAmount;
        if (amount <= 0) {
            kill();
        }
        return new ResourceChunk(resourceType, defaultGatherAmount);
    }

    @Override
    public void kill() {
        super.kill();
        getImage().remove();
        final Body body = getBody();
        body.getWorld().destroyBody(body);
        removable = true;
    }

    public boolean isRemovable() {
        return removable;
    }

    public enum ResourceType {
        WOOD,
        FOOD
    }
}
