package com.monsieur_h.monolith.engine;

/**
 * Created for project monolith by ahub on 10/17/16.
 */
public class Util {
    public static boolean sameSign(float a, float b) {
        return ((a > 0 && b > 0) || (a < 0 && b < 0));
    }
}
