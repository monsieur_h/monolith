package com.monsieur_h.monolith.engine.generation;


import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.monsieur_h.monolith.engine.entities.ResourceEntity;
import com.monsieur_h.monolith.engine.entities.Tile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class WorldContainer {
    private final com.badlogic.gdx.physics.box2d.World world;
    private final ArrayList<Tile> tiles = new ArrayList<Tile>();
    private final ArrayList<ResourceEntity> resources = new ArrayList<ResourceEntity>();
    private final ArrayList<Body> mapBorders = new ArrayList<Body>();
    private float radius = 0;


    WorldContainer(com.badlogic.gdx.physics.box2d.World world) {
        this.world = world;
    }

    public void addTile(Tile tile) {
        tiles.add(tile);
    }

    public void addResource(ResourceEntity resourceEntity) {
        resources.add(resourceEntity);
    }

    public void addBorder(Body border) {
        mapBorders.add(border);
    }

    public void update() {
        for (int i = resources.size() - 1; i >= 0; i--) {
            if (resources.get(i).isRemovable()) {
                resources.remove(i);
            }
        }
    }

    public ArrayList<Tile> getTiles() {
        return tiles;
    }

    public ArrayList<ResourceEntity> getResources() {
        return resources;
    }

    public ArrayList<Body> getMapBorders() {
        return mapBorders;
    }

    private void computeRadius() {
        if (tiles.isEmpty()) {
            throw new GdxRuntimeException("Cannot compute radius with no tiles");
        }
        float minX = Float.POSITIVE_INFINITY;
        float minY = Float.POSITIVE_INFINITY;
        float maxX = Float.NEGATIVE_INFINITY;
        float maxY = Float.NEGATIVE_INFINITY;
        for (Tile tile : tiles) {
            minX = Math.min(tile.getImage().getX(), minX);
            minY = Math.min(tile.getImage().getY(), minY);

            maxX = Math.max(tile.getImage().getRight(), maxX);
            maxY = Math.max(tile.getImage().getTop(), maxY);
        }
        radius = Math.max((maxX - minX), (maxY - minY)) / 2f;
    }

    public float getRadius() {
        if (radius == 0) {
            computeRadius();
        }
        return radius;
    }

    /**
     * Return tiles close to a position that collide with Tiles AABBs, in GROUND layer coordinates
     * @param position position to look from
     * @return List of tiles ordered by proximity
     */
    public List<Tile> getTilesAt(final Vector2 position) {
        List<Tile> tiles = new ArrayList<Tile>();
        Image img;
        for (Tile t : getTiles()) {
            img = t.getImage();
            if (position.x >= img.getX() && position.x <= img.getX() + img.getWidth()
                    && position.y >= img.getY() && position.y <= img.getY() + img.getHeight()) {
                tiles.add(t);
            }
        }
        Collections.sort(tiles, new Comparator<Tile>() {
            @Override
            public int compare(Tile t1, Tile t2) {
                return (int) (getCenter(t1).dst(position) - getCenter(t2).dst(position));
            }

            Vector2 getCenter(Tile t) {
                Image i = t.getImage();
                return new Vector2(i.getX() + (i.getWidth() / 2), i.getY() + (i.getHeight() / 2));
            }
        });
        return tiles;
    }

    public ResourceEntity getClosestResourceByType(ResourceEntity.ResourceType type, int box2dX, int box2dY) {
        ResourceEntity closest = null;
        float closestDistance = Float.POSITIVE_INFINITY;
        final Vector2 comparisonPoint = new Vector2(box2dX, box2dY);
        for (ResourceEntity resourceEntity : resources) {
            if (resourceEntity.getResourceType() != type) {
                continue;
            }

            if (resourceEntity.isRemovable()) {
                continue;
            }

            final float currentDistance = comparisonPoint.dst(resourceEntity.getBody().getPosition());
            if (currentDistance < closestDistance) {
                closest = resourceEntity;
                closestDistance = currentDistance;
            }
        }
        return closest;
    }

    public ArrayList<Tile> getTileByType(TileDef.TileType type) {
        ArrayList<Tile> foundTiles = new ArrayList<Tile>();
        for (Tile tile : tiles) {
            if (tile.getDef().type == type) {
                foundTiles.add(tile);
            }
        }
        return foundTiles;
    }

    public com.badlogic.gdx.physics.box2d.World getWorld() {
        return world;
    }

    public void deleteTile(Tile targetTile) {
        targetTile.getImage().remove();
        tiles.remove(targetTile);
        Body body = targetTile.getBody();
        if (body != null) {
            body.getWorld().destroyBody(body);
        }
    }
}
