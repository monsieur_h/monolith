package com.monsieur_h.monolith.engine.generation;


import com.badlogic.gdx.graphics.Color;

public class TileDef {
    private static Color tmpColor;
    public TileType type = TileType.GRASS;
    public TileContext context = new TileContext();
    public int x;//In map coordinates
    public int y;

    public TileDef(TileType type) {
        this.type = type;
        context.adjacentTilesType[4] = type;
    }

    static TileType fromRGB(int rgbValue) {
        tmpColor = new Color(rgbValue);
        if (tmpColor.r == 1 && tmpColor.g == 1 && tmpColor.b == 1) {
            return TileType.HOUSE;
        } else if (tmpColor.b == 1) {
            return TileType.WATER;
        } else if (tmpColor.g == 1) {
            return TileType.HILL;
        } else {
            return TileType.GRASS;
        }
    }

    static boolean hasResourcesFromRGB(int rgbValue) {
        tmpColor = new Color(rgbValue);
        return tmpColor.r == 1 && tmpColor.b != 1;
    }

    public boolean isWalkable() {
        return (type != TileType.HILL
                && type != TileType.WATER
                && type != TileType.HOUSE);
    }

    public enum TileType {
        WATER,
        GRASS,
        HILL,
        HOUSE
    }
}
