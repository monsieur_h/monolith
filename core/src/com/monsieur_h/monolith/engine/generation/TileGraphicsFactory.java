package com.monsieur_h.monolith.engine.generation;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.monsieur_h.monolith.engine.graphics.KenneyAtlas;

import java.util.ArrayList;

public class TileGraphicsFactory {
    private static final String HOUSE_TEXTURE_PATH = "textures/landscape/house.png";
    private final String ATLAS_PATH = "textures/landscape/landscapeTiles_sheet.xml";
    private final KenneyAtlas atlas;
    private final TextureRegion houseRegion;
    private ArrayList<TextureRegionContext> storage = new ArrayList<TextureRegionContext>();


    public TileGraphicsFactory() {
        atlas = new KenneyAtlas(ATLAS_PATH);
        houseRegion = new TextureRegion(new Texture(HOUSE_TEXTURE_PATH));
        populateStorage();
    }

    private void populateStorage() {
        final TileContext house = new TileContext();
        house.setAll(null);
        house.adjacentTilesType[4] = TileDef.TileType.HOUSE;
        storage.add(new TextureRegionContext(house, houseRegion));

        final TileContext waterMiddle = new TileContext();
        waterMiddle.adjacentTilesType[1] = TileDef.TileType.WATER;
        waterMiddle.adjacentTilesType[3] = TileDef.TileType.WATER;
        waterMiddle.adjacentTilesType[4] = TileDef.TileType.WATER;
        waterMiddle.adjacentTilesType[5] = TileDef.TileType.WATER;
        waterMiddle.adjacentTilesType[7] = TileDef.TileType.WATER;
        storage.add(new TextureRegionContext(waterMiddle, atlas.getRegion("water")));


        final TileContext waterWest = new TileContext();
        waterWest.setAll(TileDef.TileType.WATER);
        waterWest.adjacentTilesType[3] = TileDef.TileType.GRASS;
        waterWest.adjacentTilesType[0] = null;
        waterWest.adjacentTilesType[6] = null;
        storage.add(new TextureRegionContext(waterWest, atlas.getRegion("shore_east")));

        final TileContext waterEast = new TileContext();
        waterEast.setAll(TileDef.TileType.WATER);
        waterEast.adjacentTilesType[5] = TileDef.TileType.GRASS;
        waterEast.adjacentTilesType[2] = null;
        waterEast.adjacentTilesType[8] = null;
        storage.add(new TextureRegionContext(waterEast, atlas.getRegion("shore_west")));

        final TileContext waterNorth = new TileContext();
        waterNorth.setAll(TileDef.TileType.WATER);
        waterNorth.adjacentTilesType[7] = TileDef.TileType.GRASS;
        waterNorth.adjacentTilesType[6] = null;
        waterNorth.adjacentTilesType[8] = null;
        storage.add(new TextureRegionContext(waterNorth, atlas.getRegion("shore_south")));

        final TileContext waterSouth = new TileContext();
        waterSouth.setAll(TileDef.TileType.WATER);
        waterSouth.adjacentTilesType[1] = TileDef.TileType.GRASS;
        waterSouth.adjacentTilesType[0] = null;
        waterSouth.adjacentTilesType[2] = null;
        storage.add(new TextureRegionContext(waterSouth, atlas.getRegion("shore_north")));

        final TileContext shoreNE = new TileContext();
        shoreNE.setAll(TileDef.TileType.WATER);
        shoreNE.adjacentTilesType[2] = TileDef.TileType.GRASS;
        storage.add(new TextureRegionContext(shoreNE, atlas.getRegion("shoreNE")));

        final TileContext shoreSW = new TileContext();
        shoreSW.setAll(TileDef.TileType.WATER);
        shoreSW.adjacentTilesType[6] = TileDef.TileType.GRASS;
        storage.add(new TextureRegionContext(shoreSW, atlas.getRegion("shoreSW")));

        final TileContext shoreSE = new TileContext();
        shoreSE.setAll(TileDef.TileType.WATER);
        shoreSE.adjacentTilesType[8] = TileDef.TileType.GRASS;
        storage.add(new TextureRegionContext(shoreSE, atlas.getRegion("shoreSE")));

        final TileContext shoreNW = new TileContext();
        shoreNW.setAll(TileDef.TileType.WATER);
        shoreNW.adjacentTilesType[0] = TileDef.TileType.GRASS;
        storage.add(new TextureRegionContext(shoreNW, atlas.getRegion("shoreNW")));

        final TileContext cornerNW = new TileContext();
        cornerNW.setAll(null);
        cornerNW.adjacentTilesType[0] = TileDef.TileType.GRASS;
        cornerNW.adjacentTilesType[1] = TileDef.TileType.GRASS;
        cornerNW.adjacentTilesType[3] = TileDef.TileType.GRASS;
        cornerNW.adjacentTilesType[4] = TileDef.TileType.WATER;
        storage.add(new TextureRegionContext(cornerNW, atlas.getRegion("cornerShoreNW")));

        final TileContext cornerSE = new TileContext();
        cornerSE.setAll(null);
        cornerSE.adjacentTilesType[7] = TileDef.TileType.GRASS;
        cornerSE.adjacentTilesType[5] = TileDef.TileType.GRASS;
        cornerSE.adjacentTilesType[8] = TileDef.TileType.GRASS;
        cornerSE.adjacentTilesType[4] = TileDef.TileType.WATER;
        storage.add(new TextureRegionContext(cornerSE, atlas.getRegion("cornerShoreSE")));

        final TileContext cornerSW = new TileContext();
        cornerSW.setAll(null);
        cornerSW.adjacentTilesType[4] = TileDef.TileType.WATER;
        cornerSW.adjacentTilesType[3] = TileDef.TileType.GRASS;
        cornerSW.adjacentTilesType[7] = TileDef.TileType.GRASS;
        cornerSW.adjacentTilesType[6] = TileDef.TileType.GRASS;
        storage.add(new TextureRegionContext(cornerSW, atlas.getRegion("cornerShoreSW")));

        final TileContext cornerNE = new TileContext();
        cornerNE.setAll(null);
        cornerNE.adjacentTilesType[1] = TileDef.TileType.GRASS;
        cornerNE.adjacentTilesType[4] = TileDef.TileType.WATER;
        cornerNE.adjacentTilesType[2] = TileDef.TileType.GRASS;
        cornerNE.adjacentTilesType[5] = TileDef.TileType.GRASS;
        storage.add(new TextureRegionContext(cornerNE, atlas.getRegion("cornerShoreNE")));

    }

    private TextureRegion get(TileContext context) {
        int bestScore = -1;
        TileContext currentContext;
        TextureRegion bestMatch = null;

        for (TextureRegionContext currentTextureRegionContext : storage) {
            currentContext = currentTextureRegionContext.context;
            final int currentScore = currentContext.compareScore(context);

            if (currentScore > bestScore) {
                bestScore = currentScore;
                bestMatch = currentTextureRegionContext.region;
            }
        }
        return bestMatch;
    }

    public TextureRegion fromType(TileDef.TileType tileType) {
        return atlas.getRegion(tileType.toString().toLowerCase());
    }

    public TextureRegion fromTile(TileDef tileDef) {
        final TextureRegion region = get(tileDef.context);
        if (region != null) {
            return region;
        }
        return fromType(tileDef.type);
    }

    private class TextureRegionContext {
        final TextureRegion region;
        final TileContext context;

        TextureRegionContext(TileContext context, TextureRegion region) {
            this.region = region;
            this.context = context;
        }
    }

}
