package com.monsieur_h.monolith.engine.generation;


import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.monsieur_h.monolith.engine.graphics.KenneyAtlas;

import java.util.HashMap;

public class ResourceGraphicsFactory {
    private final String ATLAS_PATH = "textures/landscape/trees.xml";
    private final KenneyAtlas atlas;
    private final HashMap<TileDef.TileType, TextureRegion> storage;

    public ResourceGraphicsFactory() {
        atlas = new KenneyAtlas(ATLAS_PATH);
        storage = new HashMap<TileDef.TileType, TextureRegion>();

        storage.put(TileDef.TileType.GRASS, atlas.getRegion("treeGreen_high"));
    }

    public TextureRegion fromTile(TileDef tileDef) {
        return storage.get(tileDef.type);
    }
}
