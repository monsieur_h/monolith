package com.monsieur_h.monolith.engine.generation;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.physics.box2d.World;
import com.monsieur_h.monolith.engine.entities.ResourceEntity;
import com.monsieur_h.monolith.engine.entities.ResourceFactory;
import com.monsieur_h.monolith.engine.entities.Tile;
import com.monsieur_h.monolith.engine.entities.TileFactory;

public class WorldFactory {


    @SuppressWarnings("FieldCanBeLocal")
    private final String PIXMAP_NAME = "pixmap.bmp";
    private Pixmap pixmap;
    private TileDef[][] tileDefs;
    private final World world;
    private final WorldContainer worldContainer;
    private final TileFactory tileFactory = new TileFactory();
    private final ResourceFactory resourceFactory = new ResourceFactory();

    public WorldFactory(World b2world) {
        this.world = b2world;
        worldContainer = new WorldContainer(b2world);

        loadPixmap();
        readTilesFromPixmap();
        setTilesContext();
        createTilesFromTileDef();
        createMapBorderColliders();
    }

    private void loadPixmap() {
        pixmap = new Pixmap(Gdx.files.internal(PIXMAP_NAME));
    }

    private void readTilesFromPixmap() {
        final int worldW = pixmap.getWidth();
        final int worldH = pixmap.getHeight();

        tileDefs = new TileDef[worldW][worldH];
        for (int y = 0; y < worldH; y++) {
            for (int x = 0; x < worldW; x++) {
                final int pixel = pixmap.getPixel(x, y);
                tileDefs[x][y] = new TileDef(TileDef.fromRGB(pixel));
            }
        }
    }

    private void setTilesContext() {
        final int worldW = pixmap.getWidth();
        final int worldH = pixmap.getHeight();

        for (int y = 0; y < worldH; y++) {
            for (int x = 0; x < worldW; x++) {
                setAdjacentTilesContext(y, x);
                setResourceContext(y, x);
                setMapCoordinateContext(x, y);
            }
        }
    }

    private void setMapCoordinateContext(int x, int y) {
        final TileDef tileDef = tileDefs[x][y];
        tileDef.x = x;
        tileDef.y = y;
    }

    private void setResourceContext(int y, int x) {
        final TileDef tileDef = tileDefs[x][y];
        final int pixel = pixmap.getPixel(x, y);
        tileDef.context.hasResource = TileDef.hasResourcesFromRGB(pixel);
    }


    private void createTilesFromTileDef() {
        for (int y = 0; y < tileDefs.length; y++) {
            for (int x = 0; x < tileDefs[0].length; x++) {

                final TileDef tileDef = tileDefs[x][y];

                final Tile tile = tileFactory.createTile(tileDef, world, x, y);
                worldContainer.addTile(tile);

                if (tileDef.context.hasResource) {
                    final ResourceEntity resource = resourceFactory.createEntity(tileDef, world, x, y);
                    worldContainer.addResource(resource);
                }
            }
        }
    }

    private void setAdjacentTilesContext(int y, int x) {
        final TileDef tileDef = tileDefs[x][y];
        tileDef.context.adjacentTilesType[0] = getAdjacentTileType(x, y, -1, -1);
        tileDef.context.adjacentTilesType[1] = getAdjacentTileType(x, y, 0, -1);
        tileDef.context.adjacentTilesType[2] = getAdjacentTileType(x, y, +1, -1);
        tileDef.context.adjacentTilesType[3] = getAdjacentTileType(x, y, -1, 0);
        tileDef.context.adjacentTilesType[4] = getAdjacentTileType(x, y, 0, 0);
        tileDef.context.adjacentTilesType[5] = getAdjacentTileType(x, y, +1, 0);
        tileDef.context.adjacentTilesType[6] = getAdjacentTileType(x, y, -1, +1);
        tileDef.context.adjacentTilesType[7] = getAdjacentTileType(x, y, 0, +1);
        tileDef.context.adjacentTilesType[8] = getAdjacentTileType(x, y, +1, +1);
    }


    private TileDef.TileType getAdjacentTileType(int x, int y, int xOffset, int yOffset) {
        final int neighborX = x + xOffset;
        final int neighborY = y + yOffset;
        if (neighborX < 0
                || neighborX >= tileDefs.length
                || neighborY < 0
                || neighborY >= tileDefs[0].length) {
            return null;
        }
        return tileDefs[neighborX][neighborY].type;
    }

    public WorldContainer getWorldContainer() {
        return worldContainer;
    }

    private void createMapBorderColliders() {
        for (int x = 0; x < tileDefs.length; x++) {
            tileFactory.createTileCollider(world, x, -1);
            tileFactory.createTileCollider(world, x, tileDefs.length);
        }

        for (int y = 0; y < tileDefs[0].length; y++) {
            tileFactory.createTileCollider(world, -1, y);
            tileFactory.createTileCollider(world, tileDefs[0].length, y);
        }
    }
}
