package com.monsieur_h.monolith.engine.generation;


public class TileContext {
    /**
     * Adjacent types in clockwise order starting topleft
     * 0 | 1 | 2
     * 3 | 4 | 5
     * 6 | 7 | 8
     *
     * ------------->Translated
     * 8 / 7 / 6
     * 5 / 4 / 3
     * 2 / 1 / 0
     */
    public TileDef.TileType[] adjacentTilesType = new TileDef.TileType[9];
    public boolean hasResource = false;

    /**
     * Compares to TileDef contexts and score them. Returns adjacentTilesType.length if identical
     */
    public int compareScore(TileContext other) {
        int score = 0;

        for (int i = 0; i < adjacentTilesType.length; i++) {
            if (adjacentTilesType[i] == null// Null is a wildcard
                    || other.adjacentTilesType[i] == null) {
                continue;
            }

            if (adjacentTilesType[i] == other.adjacentTilesType[i]) {
                score++;
            } else {
                return Integer.MIN_VALUE;
            }
        }
        return score;
    }

    public void setAll(TileDef.TileType tileType) {
        for (int i = 0; i < adjacentTilesType.length; i++) {
            adjacentTilesType[i] = tileType;
        }
    }
}
