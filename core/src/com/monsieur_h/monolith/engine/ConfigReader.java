package com.monsieur_h.monolith.engine;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.HashMap;

public class ConfigReader {
    public static HashMap<Configurable, JsonValue> configFiles = new HashMap<Configurable, JsonValue>();
    public static JsonReader reader = new JsonReader();

    public static void init() {
        for (Configurable configurable : Configurable.values()) {
            readConfigFile(configurable);
        }
    }

    public static void readConfigFile(Configurable configurable) {
        JsonValue rootElement = reader.parse(Gdx.files.internal("config/" + configurable.toString().toLowerCase() + ".json"));
        configFiles.put(Configurable.LIGHTING, rootElement);
    }

    public static JsonValue getConfigRootNode(Configurable configurable) {
        return configFiles.get(configurable);
    }

    public enum Configurable {
        LIGHTING
    }

}
