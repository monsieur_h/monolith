package com.monsieur_h.monolith.engine.text;

import com.badlogic.gdx.utils.I18NBundle;
import com.badlogic.gdx.utils.StringBuilder;
import com.monsieur_h.monolith.dto.L10nProvider;
import com.monsieur_h.monolith.engine.ai.RelationshipMapper;
import com.monsieur_h.monolith.engine.ai.SocialAttributes;
import com.monsieur_h.monolith.engine.ai.VillagerAI;

import java.util.ArrayList;
import java.util.List;

/**
 * Created for project monolith by ahub on 10/19/16.
 */
public class VillagerDescriptor {
    private VillagerAI villagerAI;

    public VillagerDescriptor(VillagerAI villagerAI) {

        this.villagerAI = villagerAI;
    }

    public String getDescription() {
        I18NBundle bundle = L10nProvider.getBundle(L10nProvider.BundleType.DESCRIPTION);

        final String familyDesc = getFamilyDescription(bundle);
        final String socialDesc = getSocialDescription(bundle);

        return String.format("%s\n%s", familyDesc, socialDesc);
    }

    private String getSocialDescription(I18NBundle bundle) {
        SocialAttributes socialAttributes = villagerAI.getSocialAttributes();
        List<String> attributesList = new ArrayList<String>();
        for (SocialAttributes.Value value : socialAttributes.getAll()) {
            attributesList.add(bundle.get(value.toString()));
        }

        StringBuilder builder = new StringBuilder();
        builder.append(attributesList.get(0));
        for (int i = 1; i < attributesList.size() - 1; i++) {
            builder.append(", ");
            builder.append(attributesList.get(i));
        }
        builder.append(" and ");
        builder.append(attributesList.get(attributesList.size() - 1));

        return bundle.format("DESC", builder.toString());
    }

    private String getFamilyDescription(I18NBundle bundle) {
        //Bob, single, childless
        //Alice, married to X, mother of 2
        final RelationshipMapper relations = villagerAI.getRelationshipMapper();
        final String name = villagerAI.getName();
        final String maritalStatus = getMaritalStatus(bundle, relations);

        final String parentHood = getParentHoodDescription(relations, bundle);

        return String.format("%s, %s, %s.", name, maritalStatus, parentHood);
    }

    private String getMaritalStatus(I18NBundle bundle, RelationshipMapper relations) {
        if (relations.getCompanion() == null) {
            return bundle.get("SINGLE");
        } else {
            return bundle.format("MARRIED", relations.getCompanion().getName());
        }
    }

    private String getParentHoodDescription(RelationshipMapper relations, I18NBundle bundle) {
        if (relations.getChildrenCount() <= 0) {
            return bundle.format("CHILDLESS");
        } else {
            return bundle.format(villagerAI.getSex() == VillagerAI.Sex.MALE ? "FATHER" : "MOTHER", relations.getChildrenCount());
        }
    }
}
