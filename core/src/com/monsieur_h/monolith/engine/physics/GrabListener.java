package com.monsieur_h.monolith.engine.physics;

import com.monsieur_h.monolith.gameplay.GrabbableEntity;

public interface GrabListener {
    void onGrab(GrabbableEntity grabbed);

    void onGrabReleased(GrabbableEntity grabbed);
}
