package com.monsieur_h.monolith.engine.physics;


import com.badlogic.gdx.math.Vector2;

public class AABB {
    public Vector2 min;
    public Vector2 max;

    public AABB(Vector2 min, Vector2 max) {
        this.min = min;
        this.max = max;
    }

    public AABB(float x, float y, float width, float height) {
        this.min = new Vector2(x, y);
        this.max = min.cpy().add(width, height);
    }

    public boolean contains(Vector2 point) {
        return (point.x >= min.x) && (point.x <= max.x) && (point.y >= min.y) && (point.y <= max.y);
    }

    public boolean contains(float x, float y) {
        return contains(new Vector2(x, y));
    }

    public boolean overlap(AABB other) {
        return contains(other.min)
                || contains(other.max)
                || contains(other.min.x, other.max.y)
                || contains(other.max.x, other.min.y);
    }

    public boolean contains(AABB other) {
        return contains(other.min)
                && contains(other.max)
                && contains(other.min.x, other.max.y)
                && contains(other.max.x, other.min.y);
    }

    public float getWidth() {
        return (max.x - min.x);
    }

    public float getHeight() {
        return (max.y - min.y);
    }
}
