package com.monsieur_h.monolith.engine.physics;

public class CollisionFilter {
    public static short asBits(Tag tag) {
        return (short) tag.bits;
    }

    public enum Tag {
        NONE(0),
        NON_WALKABLE_TILE(1),
        GHOST(2),
        ENTITY(4),
        ALL(0xFFFF);

        private final int bits;

        Tag(int bits) {
            this.bits = bits;
        }
    }
}
