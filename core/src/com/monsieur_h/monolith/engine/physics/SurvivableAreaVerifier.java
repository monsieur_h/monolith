package com.monsieur_h.monolith.engine.physics;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.monsieur_h.monolith.MonolithGame;
import com.monsieur_h.monolith.engine.entities.Tile;
import com.monsieur_h.monolith.engine.generation.WorldContainer;
import com.monsieur_h.monolith.gameplay.GrabbableEntity;

import java.util.ArrayList;

public class SurvivableAreaVerifier implements GrabListener {

    private WorldContainer worldContainer;

    public SurvivableAreaVerifier(WorldContainer worldContainer) {

        this.worldContainer = worldContainer;
    }

    @Override
    public void onGrab(GrabbableEntity grabbed) {

    }

    @Override
    public void onGrabReleased(GrabbableEntity grabbed) {
        boolean isOnSafeArea = isAreaSafe(grabbed);
        if (!isOnSafeArea) {
            grabbed.kill();
        }
    }

    private boolean isAreaSafe(GrabbableEntity grabbed) {
        // TODO: Check with more precision than the images
        Actor entityActor = grabbed.getActor();
        Vector2 pos = new Vector2(entityActor.getX(), entityActor.getY());

        if (!isOnWorld(pos.x, pos.y)) {
            return false;
        }

        AABB actorBox = new AABB(
                entityActor.getX(),
                entityActor.getY(),
                entityActor.getWidth(),
                entityActor.getHeight()
        );

        ArrayList<Tile> tiles = worldContainer.getTiles();
        for (Tile tile : tiles) {
            AABB tileBox = asAABB(tile);
            if (tileBox.overlap(actorBox)) {
                if (!(tile.getDef().isWalkable()) && (physicsCheck(pos.x, pos.y, tile))) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isOnWorld(float x, float y) {
        AABB worldAABB = getWorldAABB();
        if (!worldAABB.contains(x, y)) {
            return false;
        }

        float w = worldAABB.getWidth();
        float h = worldAABB.getHeight();
        float minx = worldAABB.min.x;
        float miny = worldAABB.min.y;
        float[] vertices = {
                minx + (w / 2), miny + 0, //mid bottom
                minx + w, miny + (h / 2), //mid right
                minx + (w / 2), miny + h, //mid top
                minx + 0, miny + (h / 2) //mid left
        };
        Polygon worldHitBox = new Polygon(vertices);
        return worldHitBox.contains(x, y);
    }

    private AABB getWorldAABB() {
        ArrayList<Tile> tiles = worldContainer.getTiles();
        AABB sampleAABB = asAABB(tiles.remove(0));
        Vector2 min = sampleAABB.min.cpy();
        Vector2 max = sampleAABB.max.cpy();
        for (Tile tile : tiles) {
            AABB tileAABB = asAABB(tile);
            min.x = Math.min(min.x, tileAABB.min.x);
            min.y = Math.min(min.y, tileAABB.min.y);
            max.x = Math.max(max.x, tileAABB.max.x);
            max.y = Math.max(max.y, tileAABB.max.y);
        }
        return new AABB(min, max);
    }

    private boolean physicsCheck(float x, float y, Tile tile) {
        assert tile.getBody() != null;
        Vector2 point = new Vector2(
                MonolithGame.pixelsToMeters((int) x),
                MonolithGame.pixelsToMeters((int) y)
        );
        for (Fixture fixture : tile.getBody().getFixtureList()) {
            if (fixture.testPoint(point)) {
                return true;
            }
        }
        return false;
    }

    private AABB asAABB(Tile tile) {
        Actor tileActor = tile.getImage();
        return new AABB(tileActor.getX(), tileActor.getY(), tileActor.getWidth(), tileActor.getHeight());
    }
}
