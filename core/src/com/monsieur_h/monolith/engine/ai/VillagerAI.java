package com.monsieur_h.monolith.engine.ai;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.fsm.State;
import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.utils.Timer;
import com.monsieur_h.monolith.dto.BehaviorLog;
import com.monsieur_h.monolith.dto.BehaviorLogFactory;
import com.monsieur_h.monolith.dto.RingBuffer;
import com.monsieur_h.monolith.dto.TimeManager;
import com.monsieur_h.monolith.engine.ai.messages.MessageType;
import com.monsieur_h.monolith.engine.ai.states.*;
import com.monsieur_h.monolith.engine.entities.HouseTile;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;
import com.monsieur_h.monolith.engine.generation.WorldContainer;
import com.monsieur_h.monolith.engine.text.VillagerDescriptor;

import java.util.ArrayList;

public class VillagerAI {
    public static final float RECONSIDER_DECISION_EVERY = 20f;
    public static final float DEFAULT_HIT_DAMAGE = .05f;
    public static final float DEFAULT_SAFETY_MODIFIER = .1f;
    private final Timer.Task task;
    private final BehaviorLogFactory behaviorLogFactory;
    private Sex sex = Sex.FEMALE;
    private String name = "noname";
    private float birthdate = 0;//In days

    private BiologicalStatus status = new BiologicalStatus();
    private SocialAttributes socialAttributes;
    private ArrayList<VillagerStateChangeListener> stateListeners = new ArrayList<VillagerStateChangeListener>();
    private ArrayList<VillagerPlanChangeListener> planListeners = new ArrayList<VillagerPlanChangeListener>();
    private VillagerEntity parentEntity;
    private VillagerStateMachine stateMachine;
    private VillagerStatePlanner planner;
    private WorldContainer worldContainer;
    private float lastDelta;
    private HouseTile house;
    private RelationshipMapper relationshipMapper = new RelationshipMapper();
    private RingBuffer<BehaviorLog> lastPlans = new RingBuffer<BehaviorLog>(5);
    private RingBuffer<BehaviorLog> lastActions = new RingBuffer<BehaviorLog>(10);
    private BiologicalStatus.NeedType lastChoice;
    private Inventory inventory = new Inventory();
    private boolean alive = true;

    public VillagerAI(WorldContainer worldContainer) {
        this.worldContainer = worldContainer;
        planner = new VillagerStatePlanner(this);
        stateMachine = new VillagerStateMachine(this);
        task = new Timer.Task() {
            @Override
            public void run() {
                makeChoice();
            }
        };
        behaviorLogFactory = new BehaviorLogFactory();
        birthdate = TimeManager.getWorldAgeInDays();
        socialAttributes = SocialAttributeFactory.create();
        MessageManager.getInstance().addListener(this.stateMachine, MessageType.RESOURCE_MODIFIED.getValue());
    }

    public float getAge() {
        return TimeManager.getWorldAgeInDays() - birthdate;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public WorldContainer getWorldContainer() {
        return worldContainer;
    }

    public void update(float delta) {
        if (!isAlive()) {
            return;
        }
        lastDelta = delta;
        status.update(delta);
        if (status.isDeadStatus()) {
            onDie();
        }
        relationshipMapper.update();
        stateMachine.update();
        if (stateMachine.getCurrentState() == null) {
            makeChoice();
        }
    }

    public void onStateChanged(State<VillagerAI> previousState, State<VillagerAI> currentState) {
        for (VillagerStateChangeListener listener : stateListeners) {
            listener.onStateChanged(this, previousState, currentState);
        }
    }

    public void onGrab() {
        interruptBehavior();
        for (VillagerStateChangeListener listener : stateListeners) {
            listener.onGrab(this);
        }
        String s = new VillagerDescriptor(this).getDescription();
        Gdx.app.log("DESC", s);

    }

    public void onGrabReleased() {
        for (VillagerStateChangeListener listener : stateListeners) {
            listener.onReleased(this);
        }

        if (!isAlive()) {
            return;
        }
        scheduleChoice();
        stateMachine.pushState(new StunnedState());
        stateMachine.popState();
    }

    public void registerChangeListener(VillagerStateChangeListener listener) {
        stateListeners.add(listener);
    }

    public void registerPlanListener(VillagerPlanChangeListener listener) {
        planListeners.add(listener);
    }

    public VillagerEntity getParentEntity() {
        return parentEntity;
    }

    public void setParentEntity(VillagerEntity parentEntity) {
        this.parentEntity = parentEntity;
    }

    public VillagerStateIcons getCurrentVillagerStateIcon() {
        VillagerState state = (VillagerState) stateMachine.getCurrentState();
        return state.getIcon();
    }

    public VillagerState getCurrentState() {
        return (VillagerState) stateMachine.getCurrentState();
    }

    public void makeChoice() {
        ArrayList<VillagerState> plan = buildPlan();
        applyPlan(plan);

        task.cancel();
        Timer.schedule(task, RECONSIDER_DECISION_EVERY);

        onPlanChanged();
    }

    private ArrayList<VillagerState> buildPlan() {
        final ArrayList<BiologicalStatus.NeedType> prioritizedNeeds = status.getPrioritizedNeeds();

        ArrayList<VillagerState> plan = new ArrayList<VillagerState>();
        for (BiologicalStatus.NeedType type : prioritizedNeeds) {
            if (planner.buildPlanForType(type, plan)) {
                lastChoice = type;
                break;
            }
        }

        if (plan.isEmpty()) { // No solution or plan for anything ? Wander, looking for a solution
            lastChoice = prioritizedNeeds.get(0);
            planner.buildDefaultPlan(plan);
        }
        return plan;
    }

    private void applyPlan(ArrayList<VillagerState> plan) {
        stateMachine.clearStack();
        for (VillagerState state : plan) {
            stateMachine.pushState(state);
        }
        stateMachine.popState();//Start the topmost state
    }

    private void onPlanChanged() {
        for (VillagerPlanChangeListener listener : planListeners) {
            listener.onPlanChanged(this);
        }
        lastPlans.add(behaviorLogFactory.create(this, BehaviorLog.LogLOD.PLAN));
    }

    public void onStateComplete() {
        if (stateMachine.stateStack.size() > 0) {
            stateMachine.popState();
        } else {
            makeChoice();
        }
    }

    public float getLastDelta() {
        return lastDelta;
    }

    public void updateNeed(BiologicalStatus.NeedType needType, int amount) {
        status.updateNeed(needType, amount);
    }

    public void scheduleChoice() {
        if (!isAlive()) {
            return;
        }
        task.cancel();
        Timer.schedule(task, RECONSIDER_DECISION_EVERY);
    }

    public void interruptBehavior() {
        if (!isAlive()) {
            return;
        }
        task.cancel();
        if (stateMachine.getCurrentState() != null) {
            stateMachine.getCurrentState().exit(this);
        }
        stateMachine.clearStack();
    }

    public float getNeedSatisfaction(BiologicalStatus.NeedType needType) {
        return status.getSatisfaction(needType);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HouseTile getHouse() {
        return house;
    }

    public void setHouse(HouseTile house) {
        this.house = house;
    }

    public BiologicalStatus.NeedType getLastChoice() {
        return lastChoice;
    }

    public void onStateEnter() {
        lastActions.add(behaviorLogFactory.create(this, BehaviorLog.LogLOD.STATE));
    }

    public RingBuffer<BehaviorLog> getLastPlans() {
        return lastPlans;
    }

    public RingBuffer<BehaviorLog> getLastActions() {
        return lastActions;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public VillagerStateMachine getStateMachine() {
        return stateMachine;
    }

    public void onStateFailed(VillagerState state) {
        interruptBehavior();
        makeChoice();
    }

    public RelationshipMapper getRelationshipMapper() {
        return relationshipMapper;
    }

    public void onAttackStarted(VillagerAI attacker) {
        final State<VillagerAI> currentState = stateMachine.getCurrentState();
        if (currentState instanceof AttackState || currentState instanceof ApproachEnnemyState) {
            return;
        }

        interruptBehavior();
        relationshipMapper.deteriorateRelationship(attacker);
        final RelationshipMeter.RelationType relationToAttacker = relationshipMapper.getRelationShip(attacker).getAsType();
        if (relationToAttacker == RelationshipMeter.RelationType.HATE) {//Hate him : fight back
            stateMachine.pushState(new AttackState(attacker));
        } else {//Don't hate him ? Flee
            stateMachine.pushState(new FleeAttackState(attacker));
            stateMachine.popState();
        }
        onPlanChanged();
    }

    public void onHit() {
        final float currentHealth = status.getSatisfaction(BiologicalStatus.NeedType.HEALTH);
        final float nextHealth = currentHealth - DEFAULT_HIT_DAMAGE;
        status.setSatisfaction(BiologicalStatus.NeedType.HEALTH, nextHealth);
    }

    public void kill() {
        status.setSatisfaction(BiologicalStatus.NeedType.HEALTH, 0);
    }

    private void onDie() {
        alive = false;
        task.cancel();
        stateMachine.clearStack();
        stateMachine.pushState(new DeadState());
        stateMachine.popState();
        onPlanChanged();
    }

    public boolean isAlive() {
        return alive;
    }

    public void onUnsafe() {
        float satisfaction = status.getSatisfaction(BiologicalStatus.NeedType.SAFETY);
        status.setSatisfaction(BiologicalStatus.NeedType.SAFETY, satisfaction - DEFAULT_SAFETY_MODIFIER);
    }

    public SocialAttributes getSocialAttributes() {
        return socialAttributes;
    }

    public enum Sex {
        MALE,
        FEMALE
    }
}
