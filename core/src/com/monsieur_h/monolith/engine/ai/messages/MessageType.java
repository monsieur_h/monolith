package com.monsieur_h.monolith.engine.ai.messages;

public enum MessageType {
    RESOURCE_MODIFIED(1);

    private final int intValue;

    MessageType(int value) {
        intValue = value;
    }

    public static MessageType fromInt(int intValue) {
        for (MessageType messageType : MessageType.values()) {
            if (messageType.getValue() == intValue) {
                return messageType;
            }
        }
        return null;
    }

    public int getValue() {
        return intValue;
    }
}
