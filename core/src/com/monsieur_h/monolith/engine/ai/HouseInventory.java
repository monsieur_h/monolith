package com.monsieur_h.monolith.engine.ai;

import com.monsieur_h.monolith.engine.entities.InventoryObject;
import com.monsieur_h.monolith.engine.entities.ResourceChunk;
import com.monsieur_h.monolith.engine.entities.ResourceEntity;

public class HouseInventory extends Inventory {
    public HouseInventory() {
        super(50);//Number of different objects to be stored
    }

    @Override
    public boolean add(InventoryObject inventoryObject) {
        final InventoryObject similarObject = findByType(inventoryObject.getType());
        if (similarObject == null) {
            return addNewObject(inventoryObject);
        } else {
            final int currentAmount = similarObject.getAmount();
            similarObject.setAmount(currentAmount + inventoryObject.getAmount());
        }
        return true;
    }

    public InventoryObject takeByType(ResourceEntity.ResourceType type, int amount) {
        final InventoryObject resource = findByType(type);
        if (resource == null || resource.getAmount() <= 0) {
            return null;
        }
        final int currentQuantity = resource.getAmount();
        final int newQuantity = Math.max(currentQuantity - amount, 0);
        resource.setAmount(newQuantity);
        return new ResourceChunk(type, (currentQuantity - newQuantity));
    }

    public float getAmountByType(ResourceEntity.ResourceType type) {
        InventoryObject resource = findByType(type);
        if (resource == null) {
            return 0;
        }
        return resource.getAmount();
    }

    public boolean hasResource(ResourceEntity.ResourceType type) {
        return getAmountByType(type) > 0;
    }

    private boolean addNewObject(InventoryObject inventoryObject) {
        if (isFull()) {
            return false;
        }
        storage.add(inventoryObject);
        return true;
    }

    private InventoryObject findByType(ResourceEntity.ResourceType type) {
        for (InventoryObject object : storage) {
            if (object.getType() == type) {
                return object;
            }
        }
        return null;
    }
}
