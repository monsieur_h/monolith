package com.monsieur_h.monolith.engine.ai;


import com.badlogic.gdx.math.MathUtils;

public class BiologicalNeed {
    public static final int MAX_SATISFACTION = 1;
    public static final int MIN_SATISFACTION = 0;
    private float currentSatisfaction = 1f;
    private float timeToDecay = 300f;//In second
    private boolean deadly = false;
    private BiologicalStatus.NeedType type;


    public BiologicalNeed(BiologicalStatus.NeedType type, boolean deadly, float timeToDecay) {
        this.timeToDecay = timeToDecay;
        this.deadly = deadly;
        this.type = type;
    }

    public BiologicalNeed(BiologicalStatus.NeedType type, boolean deadly) {
        this.type = type;
        this.deadly = deadly;
    }

    public BiologicalNeed(BiologicalStatus.NeedType type) {
        this.type = type;
    }

    public float getPriority() {
        return (float) Math.exp((MAX_SATISFACTION - currentSatisfaction) * 5f);
    }

    public void update(float delta) {
        setCurrentSatisfaction(currentSatisfaction - (1 / timeToDecay) * delta);
    }

    public void fulfil(float amount) {
        setCurrentSatisfaction(currentSatisfaction + amount);
    }

    public void setCurrentSatisfaction(float currentSatisfaction) {
        this.currentSatisfaction = MathUtils.clamp(currentSatisfaction, MIN_SATISFACTION, MAX_SATISFACTION);
    }

    public float getSatisfaction() {
        return currentSatisfaction;
    }

    public BiologicalStatus.NeedType getType() {
        return type;
    }

    public boolean isDeadly() {
        return deadly;
    }
}
