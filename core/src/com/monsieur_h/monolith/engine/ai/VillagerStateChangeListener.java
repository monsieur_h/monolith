package com.monsieur_h.monolith.engine.ai;


import com.badlogic.gdx.ai.fsm.State;

public interface VillagerStateChangeListener {
    void onStateChanged(VillagerAI villagerAI, State<VillagerAI> previousState, State<VillagerAI> currentState);

    void onGrab(VillagerAI villagerAI);

    void onReleased(VillagerAI villagerAI);
}
