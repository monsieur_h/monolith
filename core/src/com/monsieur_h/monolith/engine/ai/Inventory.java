package com.monsieur_h.monolith.engine.ai;

import com.monsieur_h.monolith.engine.entities.InventoryObject;

import java.util.ArrayList;

public class Inventory {

    public static final int DEFAULT_CAPACITY = 1;
    protected ArrayList<InventoryObject> storage = new ArrayList<InventoryObject>(DEFAULT_CAPACITY);
    private int capacity;

    public Inventory(int capacity) {
        this.capacity = capacity;
    }

    public Inventory() {
        this(DEFAULT_CAPACITY);
    }

    public boolean add(InventoryObject inventoryObject) {
        if (isFull()) {
            return false;
        }
        return storage.add(inventoryObject);
    }

    public InventoryObject pop() {
        return storage.remove(0);
    }

    public InventoryObject peek() {
        return storage.get(0);
    }

    public boolean isFull() {
        return storage.size() == capacity;
    }
}
