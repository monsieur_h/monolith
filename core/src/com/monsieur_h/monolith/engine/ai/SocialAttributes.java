package com.monsieur_h.monolith.engine.ai;

import java.util.ArrayList;
import java.util.List;

import static com.monsieur_h.monolith.engine.ai.SocialAttributes.Value.*;

/**
 * Created for project monolith by ahub on 10/19/16.
 */
public class SocialAttributes {

    private final List<Value> valueList;

    public SocialAttributes() {
        valueList = new ArrayList<Value>();
    }

    public boolean has(Value value) {
        return valueList.indexOf(value) != -1;
    }

    public void add(Value value) {
        valueList.add(value);
    }

    public List<Value> getAll() {
        return valueList;
    }

    public enum Value {
        WARM,
        COLD,
        INTELLIGENT,
        STUPID,
        PEACEFUL,
        AGGRESSIVE,
        QUICK,
        SLOW,
        HANDSOME,
        AWKWARD,
        CLUMSY,
        SKILLFUL
    }

    public enum Axis {
        WARMTH(WARM, COLD),
        INTELLIGENCE(INTELLIGENT, STUPID),
        PACE(QUICK, SLOW),
        LOOK(HANDSOME, AWKWARD),
        SKILL(SKILLFUL, CLUMSY),
        AGGRESSIVITY(PEACEFUL, AGGRESSIVE);

        private final ArrayList<Value> attributeValues;

        Axis(Value option1, Value option2) {
            attributeValues = new ArrayList<Value>();
            attributeValues.add(option1);
            attributeValues.add(option2);
        }

        public Value get(int i) {
            assert i >= 0 && i < attributeValues.size();
            return attributeValues.get(i);
        }
    }
}

