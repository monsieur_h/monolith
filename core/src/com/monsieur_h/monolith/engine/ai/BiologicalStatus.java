package com.monsieur_h.monolith.engine.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class BiologicalStatus {
    private ArrayList<BiologicalNeed> needList = new ArrayList<BiologicalNeed>();

    public BiologicalStatus() {
        needList.add(new BiologicalNeed(NeedType.SOCIAL, false, 500));
        needList.add(new BiologicalNeed(NeedType.HUNGER, true, 120));
        needList.add(new BiologicalNeed(NeedType.HEALTH, true, 360));
        needList.add(new BiologicalNeed(NeedType.SAFETY, false));//default 300
    }

    public NeedType getHighestPriorityNeed() {
        sortByPriority();
        return needList.get(needList.size() - 1).getType();
    }

    public ArrayList<NeedType> getPrioritizedNeeds() {
        sortByPriority();
        ArrayList<NeedType> prioritizedTypes = new ArrayList<NeedType>();
        for (BiologicalNeed need : needList) {
            prioritizedTypes.add(need.getType());
        }
        return prioritizedTypes;
    }

    private void sortByPriority() {
        Collections.sort(needList, new Comparator<BiologicalNeed>() {
            @Override
            public int compare(BiologicalNeed o1, BiologicalNeed o2) {
                return (int) ((o2.getPriority() - o1.getPriority()) * 100);
            }
        });
    }

    public void updateNeed(NeedType needType, float amount) {
        final BiologicalNeed need = getNeed(needType);
        assert need != null;
        need.fulfil(amount);
    }

    public void update(float delta) {
        for (BiologicalNeed need : needList) {
            need.update(delta);
        }
    }

    public boolean isDeadStatus() {
        for (BiologicalNeed need : needList) {
            if (need.isDeadly() && need.getSatisfaction() == BiologicalNeed.MIN_SATISFACTION) {
                return true;
            }
        }
        return false;
    }

    private BiologicalNeed getNeed(NeedType searchedType) {
        for (BiologicalNeed need : needList) {
            if (need.getType() == searchedType) {
                return need;
            }
        }
        return null;
    }

    public float getSatisfaction(NeedType needType) {
        return getNeed(needType).getSatisfaction();
    }

    public void setSatisfaction(NeedType type, float satisfaction) {
        getNeed(type).setCurrentSatisfaction(satisfaction);
    }

    public enum NeedType {
        HUNGER,
        SAFETY,
        SOCIAL,
        HEALTH
    }
}
