package com.monsieur_h.monolith.engine.ai;


import com.badlogic.gdx.ai.fsm.DefaultStateMachine;
import com.badlogic.gdx.ai.fsm.State;
import com.monsieur_h.monolith.engine.ai.states.IdleState;
import com.monsieur_h.monolith.engine.ai.states.VillagerState;

import java.util.Stack;

public class VillagerStateMachine extends DefaultStateMachine<VillagerAI> {
    Stack<VillagerState> stateStack = new Stack<VillagerState>();

    public VillagerStateMachine(VillagerAI owner) {
        super(owner);
    }

    public VillagerStateMachine(VillagerAI owner, State<VillagerAI> initialState) {
        super(owner, initialState);
    }

    public VillagerStateMachine(VillagerAI owner, State<VillagerAI> initialState, State<VillagerAI> globalState) {
        super(owner, initialState, globalState);
    }

    public void pushState(VillagerState state) {
        stateStack.push(state);
    }

    public void popState() {
        if (stateStack.size() > 0) {
            changeState(stateStack.pop());
        }
    }

    @Override
    public void setInitialState(State<VillagerAI> state) {
        super.setInitialState(state);
    }

    @Override
    public void changeState(State<VillagerAI> newState) {
        super.changeState(newState);
        owner.onStateChanged(previousState, currentState);
    }

    public void clearStack() {
        currentState = new IdleState();
        stateStack.clear();
    }

    public VillagerAI getVillagerAI() {
        return owner;
    }
}
