package com.monsieur_h.monolith.engine.ai.states;

public class StunnedState extends TimedState {

    public static final float DEFAULT_STUN_DURATION = .5f;

    public StunnedState() {
        super(VillagerStateIcons.STUN, DEFAULT_STUN_DURATION);
    }

    @Override
    protected boolean isStateProcessable() {
        return true;
    }
}