package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.msg.Telegram;
import com.monsieur_h.monolith.engine.ai.BiologicalStatus;
import com.monsieur_h.monolith.engine.ai.VillagerAI;

/**
 * Created for project monolith by ahub on 10/18/16.
 */
public class PrayState extends TimedState {

    protected PrayState(float duration) {
        super(VillagerStateIcons.PRAY, duration);
        this.duration = duration;
    }

    @Override
    protected void onSuccess(VillagerAI villager) {
        villager.updateNeed(BiologicalStatus.NeedType.SAFETY, getDurationBasedReward());
        villager.updateNeed(BiologicalStatus.NeedType.SOCIAL, getDurationBasedReward());
        super.onSuccess(villager);
    }

    private int getDurationBasedReward() {
        return (int) ((duration / 10) * Math.random());
    }

    @Override
    protected boolean isStateProcessable() {
        return true;//todo : make it near a praying place ? or put that in a new state -> praying at monument or something
    }

    @Override
    public boolean onMessage(VillagerAI entity, Telegram telegram) {
        return false;
    }
}
