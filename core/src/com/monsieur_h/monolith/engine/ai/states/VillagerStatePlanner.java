package com.monsieur_h.monolith.engine.ai.states;

import com.monsieur_h.monolith.engine.ai.BiologicalStatus;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.entities.ResourceEntity;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;
import com.monsieur_h.monolith.engine.generation.WorldContainer;

import java.util.ArrayList;

public class VillagerStatePlanner {
    private VillagerAI villagerAI;

    public VillagerStatePlanner(VillagerAI villagerAI) {
        this.villagerAI = villagerAI;
    }

    public boolean buildPlanForType(BiologicalStatus.NeedType needType, ArrayList<VillagerState> list) {
        switch (needType) { //TODO : handle lazyness -> go wander sometimes
            case HUNGER:
                return handleHunger(list);

            case SAFETY:
                return handleSafety(list);

            case SOCIAL:
                return handleSocial(list);

            default:
                return false;
        }
    }

    public boolean buildDefaultPlan(ArrayList<VillagerState> planList) {
        return handleDefault(planList);
    }

    private boolean handleSocial(ArrayList<VillagerState> list) {//FIXME: Only handles hate & pray at the moment
        final VillagerAI worstContact = villagerAI.getRelationshipMapper().getBestEnemy();
        if (worstContact != null) {
            float hate = villagerAI.getRelationshipMapper().getRelationShip(worstContact).getRelationValue();
            if (hate > .5f) {//todo make it a variable hate threshold per villager
                return buildAttackWorstContactPlan(list, worstContact);
            }
        }
        return buildPrayPlan(list);
    }

    private boolean buildPrayPlan(ArrayList<VillagerState> list) {
        float currentSafety = villagerAI.getNeedSatisfaction(BiologicalStatus.NeedType.SAFETY);
        list.add(new PrayState(1 - currentSafety * 10));
        list.add(new ThinkState());
        return true;
    }

    private boolean buildAttackWorstContactPlan(ArrayList<VillagerState> list, VillagerAI worstContact) {
        list.add(new AttackState(worstContact));
        list.add(new ApproachEnnemyState(worstContact));
        list.add(new ThinkState());
        return true;
    }

    private boolean handleHunger(ArrayList<VillagerState> list) {

        boolean resourceInHouse = villagerAI.getHouse().getInventory().hasResource(ResourceEntity.ResourceType.FOOD);

        if (resourceInHouse) {
            return buildGetFoodInHousePlan(list);
        } else {
            return buildGetFoodInWildPlan(list);
        }


    }

    private boolean buildGetFoodInHousePlan(ArrayList<VillagerState> list) {
        list.add(new EatState());
        list.add(new GetFromHouseState(ResourceEntity.ResourceType.FOOD));
        list.add(new GoHomeState());
        list.add(new ThinkState());
        return true;
    }

    private boolean buildGetFoodInWildPlan(ArrayList<VillagerState> list) {
        final WorldContainer worldContainer = villagerAI.getWorldContainer();
        final VillagerEntity villagerEntity = villagerAI.getParentEntity();
        final ResourceEntity closestResource = worldContainer.getClosestResourceByType(
                ResourceEntity.ResourceType.FOOD,
                (int) villagerEntity.getBox2dComponent().getPosition().x,
                (int) villagerEntity.getBox2dComponent().getPosition().y
        );

        if (closestResource == null) {
            return false;
        }
        //Reminder : add in reverse order the stack in the statemachine is prioritized
        list.add(new EatState());
        list.add(new GatherResourceState(closestResource));
        list.add(new ApproachResourceState(closestResource));
        list.add(new ThinkState());
        return true;
    }

    private boolean handleSafety(ArrayList<VillagerState> list) {
        final WorldContainer worldContainer = villagerAI.getWorldContainer();
        final VillagerEntity villagerEntity = villagerAI.getParentEntity();
        final ResourceEntity closestResource = worldContainer.getClosestResourceByType(
                ResourceEntity.ResourceType.FOOD,
                (int) villagerEntity.getBox2dComponent().getPosition().x,
                (int) villagerEntity.getBox2dComponent().getPosition().y
        );

        if (closestResource == null) {
            return false;
        }
        list.add(new StoreResourceState());
        list.add(new GoHomeState());
        list.add(new GatherResourceState(closestResource));
        list.add(new ApproachResourceState(closestResource));
        list.add(new ThinkState());
        return true;
    }

    private boolean handleDefault(ArrayList<VillagerState> list) {
        list.add(new WanderState());
        list.add(new ThinkState());
        return true;
    }
}
