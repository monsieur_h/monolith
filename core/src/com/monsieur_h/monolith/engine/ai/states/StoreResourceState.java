package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.msg.Telegram;
import com.monsieur_h.monolith.engine.ai.BiologicalStatus;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.entities.InventoryObject;

public class StoreResourceState extends TimedState {

    public static final float STORAGE_DURATION = 3f;

    protected StoreResourceState() {
        super(VillagerStateIcons.STORING, STORAGE_DURATION);
    }

    @Override
    protected void onSuccess(VillagerAI villager) {
        final InventoryObject item = villagerAI.getInventory().pop();
        villagerAI.getHouse().getInventory().add(item);
        villagerAI.updateNeed(BiologicalStatus.NeedType.SAFETY, 1);
        super.onSuccess(villager);
    }

    @Override
    protected boolean isStateProcessable() {
        return villagerAI.getInventory().peek() != null;
    }

    @Override
    public boolean onMessage(VillagerAI entity, Telegram telegram) {
        return false;
    }
}
