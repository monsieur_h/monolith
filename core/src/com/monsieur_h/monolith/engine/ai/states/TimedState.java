package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.math.MathUtils;
import com.monsieur_h.monolith.engine.ai.VillagerAI;

public abstract class TimedState extends VillagerState {
    protected float duration;
    private float elapsed = 0;

    protected TimedState(VillagerStateIcons type, float duration) {
        super(type);
        this.duration = duration;
    }

    @Override
    public void enter(VillagerAI entity) {
        super.enter(entity);
        elapsed = 0;
    }

    @Override
    public void update(VillagerAI entity) {
        super.update(entity);
        elapsed += entity.getLastDelta();
        if (getCompletion() == 1) {
            onSuccess(entity);
        }
    }

    @Override
    public boolean onMessage(VillagerAI entity, Telegram telegram) {
        return false;
    }

    public float getCompletion() {
        return MathUtils.clamp(elapsed / duration, 0, 1);
    }
}
