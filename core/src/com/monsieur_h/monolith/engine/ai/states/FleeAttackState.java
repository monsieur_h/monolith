package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.steer.behaviors.Evade;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.SteeringBehaviorProvider;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;

public class FleeAttackState extends TimedState {

    private VillagerAI attacker;

    public FleeAttackState(VillagerAI attacker) {
        super(VillagerStateIcons.STUN, AttackState.ATTACK_DURATION * 1.05f);
        this.attacker = attacker;
    }

    @Override
    public void enter(VillagerAI entity) {
        super.enter(entity);
        final Box2dSteeringEntity ownBox2dComponent = entity.getParentEntity().getBox2dComponent();
        final Box2dSteeringEntity attackedBox2dComponent = attacker.getParentEntity().getBox2dComponent();
        final Evade<Vector2> fleeBehavior = SteeringBehaviorProvider.createFleeBehavior(ownBox2dComponent, attackedBox2dComponent);
        ownBox2dComponent.setSteeringBehavior(fleeBehavior);
    }

    @Override
    protected boolean isStateProcessable() {
        return true;
    }

    @Override
    public boolean onMessage(VillagerAI entity, Telegram telegram) {
        return false;
    }
}
