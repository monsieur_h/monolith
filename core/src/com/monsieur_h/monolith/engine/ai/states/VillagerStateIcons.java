package com.monsieur_h.monolith.engine.ai.states;


public enum VillagerStateIcons {
    IDLE,
    APPROACHING,
    GATHERING,
    EATING,
    WANDERING,
    STORING,
    STUN,
    THINKING,
    BEATING,
    DEAD,
    PRAY,
    NO_ICON
}
