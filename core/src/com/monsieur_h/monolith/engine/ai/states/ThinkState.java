package com.monsieur_h.monolith.engine.ai.states;

import com.monsieur_h.monolith.engine.ai.VillagerAI;

public class ThinkState extends TimedState {

    public static final float DEFAULT_THINK_DURATION = 5f;

    protected ThinkState() {
        super(VillagerStateIcons.THINKING, DEFAULT_THINK_DURATION);
    }

    @Override
    public void update(VillagerAI entity) {
        super.update(entity);
    }

    @Override
    protected boolean isStateProcessable() {
        return true;
    }
}
