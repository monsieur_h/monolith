package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.steer.behaviors.Arrive;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.SteeringBehaviorProvider;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;

public class GoHomeState extends VillagerState {
    private static final float APPROACH_RADIUS = 1f;
    private VillagerAI villagerAI;
    private Box2dSteeringEntity houseEntity;

    protected GoHomeState() {
        super(VillagerStateIcons.APPROACHING);
    }

    @Override
    public void enter(VillagerAI entity) {
        super.enter(entity);
        if (entity.getHouse() == null) {
            exit(entity);
        }
        this.villagerAI = entity;
        houseEntity = new Box2dSteeringEntity(entity.getHouse().getBody(), false, APPROACH_RADIUS);
        final VillagerEntity villager = villagerAI.getParentEntity();
        final Arrive<Vector2> goNearBehavior = SteeringBehaviorProvider.createGoToBehavior(villager, houseEntity);

        villager.getBox2dComponent().setSteeringBehavior(goNearBehavior);
    }

    @Override
    public void update(VillagerAI entity) {
        final Vector2 position = villagerAI.getParentEntity().getBox2dComponent().getPosition();
        final float dst = houseEntity.getPosition().dst(position);
        if (dst < APPROACH_RADIUS) {
            onSuccess(entity);
        }
    }

    @Override
    protected boolean isStateProcessable() {
        return houseEntity.getBody() != null;
    }

    @Override
    public boolean onMessage(VillagerAI entity, Telegram telegram) {
        return false;
    }
}
