package com.monsieur_h.monolith.engine.ai.states;

import com.monsieur_h.monolith.engine.ai.BiologicalStatus;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.entities.InventoryObject;
import com.monsieur_h.monolith.engine.entities.ResourceEntity;

public class EatState extends TimedState {

    public static final float DEFAULT_EAT_DURATION = 5f;

    protected EatState() {
        super(VillagerStateIcons.EATING, DEFAULT_EAT_DURATION);
    }

    @Override
    public void onSuccess(VillagerAI villager) {
        final InventoryObject pop = villager.getInventory().pop();
        if (pop.getType() == ResourceEntity.ResourceType.FOOD) {
            villager.updateNeed(BiologicalStatus.NeedType.HUNGER, pop.getAmount());
        }
        super.onSuccess(villager);
    }

    @Override
    protected boolean isStateProcessable() {
        return villagerAI.getInventory().peek().getType() == ResourceEntity.ResourceType.FOOD;
    }
}
