package com.monsieur_h.monolith.engine.ai.states;

import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.entities.InventoryObject;
import com.monsieur_h.monolith.engine.entities.ResourceEntity;


public class GetFromHouseState extends TimedState {
    public static final float STORAGE_DURATION = 3f;
    private ResourceEntity.ResourceType resourceType;

    public GetFromHouseState(ResourceEntity.ResourceType resourceType) {
        super(VillagerStateIcons.STORING, STORAGE_DURATION);
        this.resourceType = resourceType;
    }

    @Override
    protected void onSuccess(VillagerAI villager) {
        if (!isStateProcessable()) {
            onFail();
            return;
        }
        InventoryObject inventoryObject = villagerAI.getHouse().getInventory().takeByType(resourceType, 1);
        villagerAI.getInventory().add(inventoryObject);
        super.onSuccess(villager);
    }

    @Override
    protected void onFail() {
        villagerAI.onUnsafe();
        super.onFail();
    }

    @Override
    protected boolean isStateProcessable() {
        return villagerAI.getHouse().getInventory().hasResource(resourceType);
    }
}
