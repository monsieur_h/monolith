package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.steer.behaviors.Pursue;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.SteeringBehaviorProvider;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;

public class ApproachEnnemyState extends VillagerState {
    private VillagerEntity target;

    public ApproachEnnemyState(VillagerAI target) {
        super(VillagerStateIcons.BEATING);
        this.target = target.getParentEntity();
    }

    @Override
    public void enter(VillagerAI entity) {
        super.enter(entity);
        final Pursue<Vector2> pursueBehavior = SteeringBehaviorProvider.createPursueBehavior(
                entity.getParentEntity().getBox2dComponent(),
                target.getBox2dComponent()
        );
        entity.getParentEntity().getBox2dComponent().setSteeringBehavior(pursueBehavior);
    }

    @Override
    public void update(VillagerAI entity) {
        super.update(entity);
        if (isTargetAtRange()) {
            onSuccess(entity);
        }
    }

    private boolean isTargetAtRange() {
        final Vector2 ownPos = villagerAI.getParentEntity().getBody().getPosition();
        final Vector2 targetPos = target.getBody().getPosition();
        final float bodyWidth = villagerAI.getParentEntity().getBody().getFixtureList().get(0).getShape().getRadius();
        final float distance = ownPos.dst(targetPos);
        return (distance <= bodyWidth * 3);
    }

    @Override
    protected boolean isStateProcessable() {
        return true;
    }

    @Override
    public boolean onMessage(VillagerAI entity, Telegram telegram) {
        return false;
    }
}
