package com.monsieur_h.monolith.engine.ai.states;


import com.badlogic.gdx.ai.fsm.State;
import com.monsieur_h.monolith.engine.ai.VillagerAI;

public abstract class VillagerState implements State<VillagerAI> {
    private final VillagerStateIcons icon;
    protected VillagerAI villagerAI;

    protected VillagerState(VillagerStateIcons icon) {
        this.icon = icon;
    }

    @Override
    public void enter(VillagerAI entity) {
        entity.getParentEntity().getBox2dComponent().setSteeringBehavior(null);
        entity.onStateEnter();
        villagerAI = entity;
    }

    @Override
    public void exit(VillagerAI entity) {
        villagerAI = null;
    }

    protected void onSuccess(VillagerAI villager) {
        villager.onStateComplete();
    }

    @Override
    public void update(VillagerAI entity) {
        if (!isStateProcessable()) {
            onFail();
        }
    }

    protected void onFail() {
        villagerAI.onStateFailed(this);
    }

    /**
     * Checks if the state has the mandatory conditions to continue its execution
     *
     * @return true if able to continue
     */
    protected abstract boolean isStateProcessable();

    public VillagerStateIcons getIcon() {
        return icon;
    }

}
