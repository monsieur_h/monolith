package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.msg.MessageManager;
import com.badlogic.gdx.ai.msg.Telegram;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.ai.messages.MessageType;
import com.monsieur_h.monolith.engine.entities.ResourceEntity;

public class GatherResourceState extends TimedState {

    public static final float DEFAULT_GATHER_DURATION = 2f;

    public static final float GATHER_RADIUS = 1f;
    private ResourceEntity closestResource;

    protected GatherResourceState(ResourceEntity closestResource) {
        super(VillagerStateIcons.GATHERING, DEFAULT_GATHER_DURATION);
        this.closestResource = closestResource;
    }

    @Override
    public void exit(VillagerAI entity) {
        super.exit(entity);
    }

    @Override
    protected void onSuccess(VillagerAI villager) {
        super.onSuccess(villager);
        villager.getInventory().add(closestResource.gatherResource());
        MessageManager.getInstance().dispatchMessage(villager.getStateMachine(), MessageType.RESOURCE_MODIFIED.getValue(), closestResource);
    }

    @Override
    public boolean onMessage(VillagerAI entity, Telegram telegram) {
        if (telegram.message == MessageType.RESOURCE_MODIFIED.getValue()
                && telegram.sender != entity.getStateMachine()) {
            ResourceEntity resourceEntity = (ResourceEntity) telegram.extraInfo;
            if (resourceEntity == closestResource) {
                onFail();
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    protected boolean isStateProcessable() {
        if (closestResource.isRemovable()) {
            return false;
        } else {
            return closestResource.getBody().getPosition().dst(villagerAI.getParentEntity().getBody().getPosition()) < GATHER_RADIUS;
        }
    }
}
