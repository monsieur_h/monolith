package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.msg.Telegram;
import com.monsieur_h.monolith.engine.ai.VillagerAI;

public class DeadState extends VillagerState {
    public DeadState() {
        super(VillagerStateIcons.DEAD);
    }

    @Override
    protected boolean isStateProcessable() {
        return true;
    }

    @Override
    public boolean onMessage(VillagerAI entity, Telegram telegram) {
        return false;
    }
}
