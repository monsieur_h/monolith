package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.msg.Telegram;
import com.monsieur_h.monolith.engine.ai.VillagerAI;

public class IdleState extends VillagerState {
    public IdleState() {
        super(VillagerStateIcons.NO_ICON);
    }

    @Override
    protected boolean isStateProcessable() {
        return true;
    }

    @Override
    public boolean onMessage(VillagerAI entity, Telegram telegram) {
        return false;
    }
}
