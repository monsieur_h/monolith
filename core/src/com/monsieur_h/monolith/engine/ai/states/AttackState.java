package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.steer.behaviors.Seek;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.SteeringBehaviorProvider;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;

public class AttackState extends TimedState {
    public static final float ATTACK_DURATION = 1.75f;
    private final VillagerEntity target;
    private VillagerAI targetAI;

    public AttackState(VillagerAI target) {
        super(VillagerStateIcons.BEATING, ATTACK_DURATION);
        this.targetAI = target;
        this.target = target.getParentEntity();
    }

    @Override
    public void enter(VillagerAI entity) {
        super.enter(entity);
        Gdx.app.log("FIGHTS", "" + entity.getName() + " is attacking " + targetAI.getName());
        targetAI.onAttackStarted(entity);
        final VillagerEntity parentEntity = entity.getParentEntity();
        final Seek<Vector2> seekBehavior = SteeringBehaviorProvider.createSeekBehavior(
                parentEntity,
                target.getBox2dComponent()
        );
        parentEntity.getBox2dComponent().setSteeringBehavior(seekBehavior);
    }

    @Override
    protected boolean isStateProcessable() {
        return isTargetAtRange();
    }

    @Override
    protected void onSuccess(VillagerAI villager) {
        villagerAI.getRelationshipMapper().increaseRelationship(targetAI);
        targetAI.getRelationshipMapper().deteriorateRelationship(villagerAI);
        targetAI.onHit();
        super.onSuccess(villager);
    }

    @Override
    protected void onFail() {
        super.onFail();
    }

    private boolean isTargetAtRange() {
        final Vector2 ownPos = villagerAI.getParentEntity().getBody().getPosition();
        final Vector2 targetPos = target.getBody().getPosition();
        final float bodyWidth = villagerAI.getParentEntity().getBody().getFixtureList().get(0).getShape().getRadius();
        final float distance = ownPos.dst(targetPos);
        return (distance <= bodyWidth * 5);
    }

    @Override
    public boolean onMessage(VillagerAI entity, Telegram telegram) {
        return false;
    }
}
