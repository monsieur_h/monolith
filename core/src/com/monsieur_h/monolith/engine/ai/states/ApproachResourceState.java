package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.msg.Telegram;
import com.badlogic.gdx.ai.steer.behaviors.Arrive;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.SteeringBehaviorProvider;
import com.monsieur_h.monolith.engine.ai.VillagerAI;
import com.monsieur_h.monolith.engine.ai.VillagerStateMachine;
import com.monsieur_h.monolith.engine.ai.messages.MessageType;
import com.monsieur_h.monolith.engine.entities.ResourceEntity;
import com.monsieur_h.monolith.engine.entities.VillagerEntity;
import com.monsieur_h.monolith.engine.physics.Box2dSteeringEntity;


public class ApproachResourceState extends VillagerState {
    public static final float APPROACH_RADIUS = 1f;
    private ResourceEntity resourceEntity;
    private VillagerAI villagerAI;
    private Box2dSteeringEntity resourceSteeringEntity;

    public ApproachResourceState(ResourceEntity resourceEntity) {
        super(VillagerStateIcons.APPROACHING);
        this.resourceEntity = resourceEntity;
    }

    @Override
    public void enter(VillagerAI villagerAI) {
        super.enter(villagerAI);
        this.villagerAI = villagerAI;
        final VillagerEntity villager = villagerAI.getParentEntity();
        resourceSteeringEntity = new Box2dSteeringEntity(resourceEntity.getBody(), false, APPROACH_RADIUS);
        final Arrive<Vector2> goNearBehavior = SteeringBehaviorProvider.createGoToBehavior(villager, resourceSteeringEntity);
        villager.getBox2dComponent().setSteeringBehavior(goNearBehavior);
    }

    @Override
    public void update(VillagerAI entity) {
        super.update(entity);
        final Vector2 position = villagerAI.getParentEntity().getBox2dComponent().getPosition();
        final float dst = resourceSteeringEntity.getPosition().dst(position);
        if (dst < APPROACH_RADIUS) {
            onSuccess(entity);
        }
    }

    @Override
    protected boolean isStateProcessable() {
        return !resourceEntity.isRemovable();
    }

    @Override
    public void exit(VillagerAI entity) {
        entity.getParentEntity().getBox2dComponent().setSteeringBehavior(null);
    }

    @Override
    public boolean onMessage(VillagerAI currentVillagerAI, Telegram telegram) {
        if (telegram.message == MessageType.RESOURCE_MODIFIED.getValue()) {
            ResourceEntity resourceEntity = (ResourceEntity) telegram.extraInfo;
            if (resourceEntity == this.resourceEntity) {
                final VillagerStateMachine sender = (VillagerStateMachine) telegram.sender;
                currentVillagerAI.getRelationshipMapper().deteriorateRelationship(sender.getVillagerAI());
                onFail();
                return true;
            }
            return false;
        }
        return false;
    }
}
