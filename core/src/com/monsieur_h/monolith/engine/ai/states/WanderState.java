package com.monsieur_h.monolith.engine.ai.states;

import com.badlogic.gdx.ai.steer.behaviors.Wander;
import com.badlogic.gdx.math.Vector2;
import com.monsieur_h.monolith.SteeringBehaviorProvider;
import com.monsieur_h.monolith.engine.ai.VillagerAI;

public class WanderState extends TimedState {
    private static final float DEFAULT_WANDER_DURATION = 5f;

    public WanderState() {
        super(VillagerStateIcons.WANDERING, DEFAULT_WANDER_DURATION);
    }

    @Override
    protected boolean isStateProcessable() {
        return true;
    }

    @Override
    public void enter(VillagerAI entity) {
        super.enter(entity);
        final Wander<Vector2> wanderBehavior = SteeringBehaviorProvider.createWanderBehavior(entity.getParentEntity());
        entity.getParentEntity().getBox2dComponent().setSteeringBehavior(wanderBehavior);
    }
}
