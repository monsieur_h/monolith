package com.monsieur_h.monolith.engine.ai;

import com.badlogic.gdx.math.MathUtils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created for project monolith by ahub on 10/19/16.
 */
public class SocialAttributeFactory {

    public static final int DEFAULT_ATTRIBUTES_COUNT = 3;

    public static SocialAttributes create() {
        return create(DEFAULT_ATTRIBUTES_COUNT);
    }

    public static SocialAttributes create(int attributesCount) {
        Set<SocialAttributes.Axis> axises = new HashSet<SocialAttributes.Axis>();

        while (axises.size() < attributesCount) {
            axises.add(getRandomAxis());
        }
        SocialAttributes socialAttributes = new SocialAttributes();

        for (SocialAttributes.Axis axis : axises) {
            socialAttributes.add(axis.get(MathUtils.random(1)));
        }
        return socialAttributes;
    }

    private static SocialAttributes.Axis getRandomAxis() {
        int length = SocialAttributes.Axis.values().length;
        return SocialAttributes.Axis.values()[MathUtils.random(length - 1)];
    }
}
