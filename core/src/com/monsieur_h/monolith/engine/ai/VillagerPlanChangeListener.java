package com.monsieur_h.monolith.engine.ai;

public interface VillagerPlanChangeListener {
    void onPlanChanged(VillagerAI owner);
}
