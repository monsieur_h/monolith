package com.monsieur_h.monolith.engine.ai;

import com.badlogic.gdx.math.MathUtils;

public class RelationshipMeter {
    public static final int MIN_VALUE = 0;
    public static final int MAX_VALUE = 1;
    private static RelationType[] orderedTypes = {
            RelationType.HATE,
            RelationType.DISLIKE,
            RelationType.INDIFFERENT,
            RelationType.LIKE,
            RelationType.LOVE
    };
    private float relationValue;//Goes from 0(HATE) to 1(LOVE)

    public RelationshipMeter() {
        relationValue = 0;
    }

    public void update() {
        relationValue = MathUtils.clamp(relationValue, MIN_VALUE, MAX_VALUE);
    }

    public RelationType getAsType() {
        update();
        final float ratio = (relationValue - MIN_VALUE) / (MAX_VALUE - MIN_VALUE);
        final int index = MathUtils.round(ratio * (orderedTypes.length - 1));
        return orderedTypes[index];
    }

    public float getRelationValue() {
        return relationValue;
    }

    public void setRelationValue(float relationValue) {
        this.relationValue = relationValue;
    }

    public enum RelationType {
        LOVE,
        HATE,
        LIKE,
        DISLIKE,
        INDIFFERENT,
        UNKNOWN
    }
}
