package com.monsieur_h.monolith.engine.ai;


import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class RelationshipMapper {
    public static final float HATE_WORLD_BIAIS = .5f;
    private static float DEFAULT_RELATIONSHIP_MODIFIER = .1f;
    private final float MIN_FAMILY_RELATION = 0;
    private VillagerAI father;
    private VillagerAI mother;
    private VillagerAI companion;
    private ArrayList<VillagerAI> children = new ArrayList<VillagerAI>();
    private HashMap<VillagerAI, RelationshipMeter> relationships = new HashMap<VillagerAI, RelationshipMeter>();

    public void update() {
        for (RelationshipMeter meter : relationships.values()) {
            meter.update();
        }
        clampFamilyValues();
    }

    private void clampFamilyValues() {
        clampRelation(father, MIN_FAMILY_RELATION, RelationshipMeter.MAX_VALUE);
        clampRelation(mother, MIN_FAMILY_RELATION, RelationshipMeter.MAX_VALUE);
        for (VillagerAI childAI : children) {
            clampRelation(childAI, MIN_FAMILY_RELATION, RelationshipMeter.MAX_VALUE);
        }
    }

    private void clampRelation(VillagerAI villagerAI, float minValue, float maxValue) {
        RelationshipMeter meter = relationships.get(villagerAI);
        if (meter == null) {
            return;
        }
        final float clampedValue = MathUtils.clamp(meter.getRelationValue(), minValue, maxValue);
        meter.setRelationValue(clampedValue);
    }

    public void deteriorateRelationship(VillagerAI other) {
        if (!isKnown(other)) {
            meet(other);
        }
        final RelationshipMeter meter = relationships.get(other);
        final float relationValue = meter.getRelationValue() - DEFAULT_RELATIONSHIP_MODIFIER;
        meter.setRelationValue(relationValue);
        meter.update();
    }

    private void meet(VillagerAI other) {
        relationships.put(other, new RelationshipMeter());
    }

    public void increaseRelationship(VillagerAI other) {
        if (!isKnown(other)) {
            meet(other);

        }
        final RelationshipMeter meter = relationships.get(other);
        final float relationValue = meter.getRelationValue() + DEFAULT_RELATIONSHIP_MODIFIER * HATE_WORLD_BIAIS;
        meter.setRelationValue(relationValue);
        meter.update();
    }

    public RelationshipMeter getRelationShip(VillagerAI other) {
        if (isKnown(other)) {
            return relationships.get(other);
        } else {
            return new RelationshipMeter();
        }
    }

    public boolean isKnown(VillagerAI other) {
        return !(relationships.get(other) == null);
    }

    public void addChild(VillagerAI childAI) {
        children.add(childAI);
    }

    public VillagerAI getFather() {
        return father;
    }

    public void setFather(VillagerAI father) {
        this.father = father;
    }

    public VillagerAI getMother() {
        return mother;
    }

    public void setMother(VillagerAI mother) {
        this.mother = mother;
    }

    public VillagerAI getBestEnemy() {
        RelationshipMeter worstMeter = new RelationshipMeter();
        worstMeter.setRelationValue(RelationshipMeter.MAX_VALUE);
        VillagerAI worstEnemy = null;

        for (VillagerAI currentVillager : relationships.keySet()) {
            final RelationshipMeter currentMeter = relationships.get(currentVillager);
            if (currentMeter.getAsType() != RelationshipMeter.RelationType.HATE) {
                continue;
            }

            if (currentMeter.getRelationValue() < worstMeter.getRelationValue()) {
                worstMeter = currentMeter;
                worstEnemy = currentVillager;
            }
        }
        return worstEnemy;
    }

    public VillagerAI getCompanion() {
        return companion;
    }

    public int getChildrenCount() {
        return children.size();
    }

    public void marry(VillagerAI other) {
        companion = other;
    }
}
